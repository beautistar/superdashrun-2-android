﻿using UnityEngine;
using System.Collections;

public class CreditFrameProperties : MonoBehaviour {
	public string ProfilePictureNo;
	public string Name;
	public tk2dTextMesh NameText;
	public tk2dSprite ProfilePicture;
	// Use this for initialization
	void Start () {
		//ProfilePicture.SetSprite (ProfilePictureNo);
		NameText.text = Name;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
