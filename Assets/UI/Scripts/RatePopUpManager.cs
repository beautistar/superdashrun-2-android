﻿using UnityEngine;
using System.Collections;

public class RatePopUpManager : MonoBehaviour {
	public static RatePopUpManager Instance;
	public GameObject RatePopup;
	public int RemindingDays;
	bool IsAndroid = false;
	bool IsAmazon = false;
	string PackageName = "com.zidapps.superdashrun2";
	string iOSAppID = "903863351";
	string AndroidURL = /*"https://play.google.com/store/apps/details?id=com.DevelopmentInnovationLLC.OutrunEpic";*/   "amzn://apps/android?p=com.zidapps.superdashrun2";
	string IOSURL ="";

	void Awake() {
		PackageName = UIGameManager.Instance.PackageName;

		if (UIGameManager.Instance.IsAndroid) {
			IsAndroid = true;	
			
		} else if (UIGameManager.Instance.IsAmazon) {
			IsAmazon = true;
		}

		IOSURL = "https://itunes.apple.com/us/app/Super-Dash-Run/id"+iOSAppID+"?mt=8";

		if (IsAndroid) {
			AndroidURL	= "https://play.google.com/store/apps/details?id="+PackageName;
		}
		if (IsAmazon) {
			AndroidURL	= "amzn://apps/android?p="+PackageName;
		}

		Instance = this;
	}

	public void Init() {
		if (UIGameManager.Instance.IsAndroid) {
			IsAndroid = true;	
			
		} else if (UIGameManager.Instance.IsAmazon) {
			IsAmazon = true;
		}

		StartRatePopUpNew ();
	}

	void StartRatePopUpNew() {
	//	Debug.Log ("StartRatePopUpNew "+ (PlayerPrefs.GetInt ("RemindDays", 0) % 3) + " " + PlayerPrefs.GetInt ("ShowRemindDays", 0));
		if ((PlayerPrefs.GetInt ("RemindDays", 0) % RemindingDays) == 0 && PlayerPrefs.GetInt ("ShowRemindDays", 0) == 0) {
			UIGameManager.Instance.ShowWall ("",120);
			RatePopup.SetActive (true);	
			PopUpManager.Instance.IsPopOn = true;
			
		} else {
			RatePopup.SetActive (false);
		}
		//Are you enjoying TakeOff Running? Rate it now
	}

	public void OnRateitClick() {
		#if UNITY_IPHONE

		Application.OpenURL (IOSURL);
		#endif
		
		#if UNITY_ANDROID
		Application.OpenURL (AndroidURL);
		#endif

		PlayerPrefs.SetInt ("ShowRemindDays", 1);
	}

	public void OnRemindMelater() {
		UIGameManager.Instance.HideWall ();
		RatePopup.SetActive (false);
	}
}
