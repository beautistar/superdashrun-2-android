﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using Facebook.Unity;

public class FBIntegrate : MonoBehaviour {
	public  static FBIntegrate Instance;
	public bool ShowLogin = false;
	public int Count ;
	public  List<object>                 scores          = new List<object>();
	public  List<object>                 ScoresIDs         =  new List<object>();
	public  List<object>                 ScoresNames         = new List<object>();
	public  List<object>                 ScoresScore         = new List<object>(); 
	public Dictionary<string,string> FriendsDetails = new Dictionary<string, string>();
	public List<Texture> ProfilePics = new List<Texture>();
	public int Myno = 0;
	public int MyScore = 0;
	// Use this for initialization
	public static event Action OnInitComplete;
	public static event Action OnFBLoggedIn ;
	public static event Action<string> OnFBLoginFailed ;
	public static event Action OnGetAppScore ;
	public static event Action OnLikePageCall  ;
	public static event Action OnPostStatusCall ;
	public static event Action OnPostStatusFailedCall ;
	public static event Action OnProfilePicsCall ;
	public static event Action OnScorePostCall ;
	public static event Action<string> OnScorePostFailedCall ;
	public static event Action OnTimeExceed ;
	public static event Action MyScoreSucced ;
	public static event Action<string> MyScoreFailed ;

	private List<string> permissions = new List<string> ();

	bool IsGettingData = false;
	float Timer  = 0;
	bool TimeUp = false;

	bool FirstTime = false;

//	string permissions;

	void Awake(){
		if(Instance == null || Instance != this)
			Instance = this;

		if (!FB.IsInitialized) {
			FB.Init (() => {
				if (FB.IsInitialized)
					FB.ActivateApp ();
				else
					Debug.LogError ("Couldn't initialize");
			},
				isGameShown => {
					if (!isGameShown)
						Time.timeScale = 0;
					else
						Time.timeScale = 1;
				});
		} else
			FB.ActivateApp ();

		permissions.Add ("public_profile");
//		permissions.Add ("email");
		permissions.Add ("user_friends");
//		permissions.Add ("publish_actions");
//		permissions.Add ("user_games_activity");
		permissions.Add ("user_photos");
	}

	void OnEnable(){
	
	}

	public void InIt(){
		//FbDebug.Log("Awake");
		enabled = false;
		FB.Init(SetInit, OnHideUnity);
	}

	void Start() {
	}
	
	private void SetInit(){
		OnInitComplete ();
		//FbDebug.Log("SetInit");
		enabled = true; // "enabled" is a property inherited from MonoBehaviour
		if (FB.IsLoggedIn) {
//			FbDebug.Log("Already logged in");
			FirstTime = true;
			//OnLoggedIn ();
		}
	}

	void Update() {
		if (IsGettingData && Timer > 23) {
			//Debug.Log ("Timer "+Timer);
			Timer = 0;
			TimeUp = true;
			IsGettingData = false;
		}
		else if(IsGettingData) {
			Timer += Time.fixedDeltaTime;
		}
	}
	
	private void OnHideUnity(bool isGameShown) {
		//FbDebug.Log("OnHideUnity");
		if (!isGameShown)	{
			// pause the game - we will need to hide
			Time.timeScale = 0;
		}
		else {
			// start the game back up - we're getting focus again
			Time.timeScale = 1;
		}
	}

	public void SetFBScore(string Score){
		Instance.Login ();
	}

	void OnGUI(){			
		if (!FB.IsLoggedIn && ShowLogin ) {                                                                                                                
			GUI.Label((new Rect(Screen.height/2 , Screen.width/2, 200, 100)), "Login to Facebook");             
			if (GUI.Button((new Rect(0 , 0, 200, 100)), "Login to Facebook")) {                                                                                                            
//				FB.LogInWithPublishPermissions (new List<string>(){"publish_actions"}, LoginCallback);  	
				FB.LogInWithReadPermissions(permissions, LoginCallback);
			}  
		}  
	}

	public void Login(){

		if (!FB.IsLoggedIn) { // LOGIN
//			FB.LogInWithPublishPermissions (new List<string>(){"publish_actions"}, LoginCallback);  
			FB.LogInWithReadPermissions(permissions, LoginCallback);
		}
		else {
			OnFBLoggedIn ();
			//GetAppScore ();
		}
	}

	void LoginCallback(ILoginResult result) {
		//FbDebug.Log("LoginCallback "   + result);
		if (result.Error != null) {                                                                                                                          
//			FbDebug.Error (result.Error); 
			if(!FirstTime)
				OnFBLoginFailed(result.Error);
			return;                                                                                                                
		} 
		//Debug.Log ("4");
		if (!FB.IsLoggedIn) {
			if(!FirstTime)
				OnFBLoginFailed(result.Error);
			return;	
		}

		//Debug.Log ("1");

		if(PlayerPrefs.GetInt ("FirstTimeFacebook",0) == 0) {
			DataManager.instance.adjustTotalCoins (EarnFreebiesStaticData.Instance.getOffersCoins (5));
			DataManager.instance.setTotalCoinsLifetime (EarnFreebiesStaticData.Instance.getOffersCoins (5));
			PlayerPrefs.SetInt ("FirstTimeFacebook", 1);
			//Debug.Log ("2");
			Updatemanager.instance.OnUpdateCoins();
			//Debug.Log ("3");
		}

		if(!FirstTime)
			OnFBLoggedIn ();
		
		GetAppScore ();
		getProfileImage ();
	}

	public void InviteFriends()
	{
		//https://fb.me/145551686098723

		FB.AppRequest(
			message: "I'm playing Super Dash Run – I invite you to beat my score!",
			title: "Super Dash Run"//,
//			callback: FBRequestCallback
		);
	}

//	public void FBRequestCallback (IAppRequestResult result)
//	{
//		// Error checking
//		Debug.Log("AppRequestCallback");
//		if (result.Error != null)
//		{
//			Debug.LogError(result.Error);
//			return;
//		}
//		Debug.Log(result.RawResult);
//
//		// Check response for success - show user a success popup if so
//		object obj;
//		if (result.ResultDictionary.TryGetValue ("cancelled", out obj))
//		{
//			Debug.Log("Request cancelled");
//		}
//		else if (result.ResultDictionary.TryGetValue ("request", out obj))
//		{
//			UM_ExampleStatusBar.text = "Request sent";
//			//PopupScript.SetPopup("Request Sent", 3f);
//			Debug.Log("Request sent");
//		}
//	}

	public void OnVisitTwitter()
	{
		Application.OpenURL("https://twitter.com/superdashrun");
	}

	public void OnVisitFB()
	{
		Application.OpenURL("https://facebook.com/superdashrun");
	}

	void PostStatus(IAppRequestResult response) {
		var result = Facebook.MiniJSON.Json.Deserialize(response.RawResult) as Dictionary<string, object>;

		if(result.ContainsKey ("cancelled")){
			bool CancelledResult = (bool) result ["cancelled"];
			if (response.Error != null || CancelledResult) {                                                                                                                          
				//FbDebug.Error ( "PostStatusOntimelineCallback: "+response.Error);
				OnPostStatusFailedCall();
				return;                                                                                                                
			} 
		}

		OnPostStatusCall ();
	}

	public void LoginLeaderBoard(){
		
		if (!FB.IsLoggedIn) { // LOGIN
//			FB.LogInWithPublishPermissions (new List<string>(){"publish_actions"}, LoginCallback); 
			FB.LogInWithReadPermissions(permissions, LoginCallback);
		}
		else {
			OnFBLoggedIn ();
			//GetAppScore ();
		}
	}

	void LoginCallbackLeaderBoard(ILoginResult result) {
		//FbDebug.Log("LoginCallback "   + result);
		if (result.Error != null) {                                                                                                                          
//			FbDebug.Error (result.Error); 
			OnFBLoginFailed(result.Error);
			return;                                                                                                                
		} 
		
		if (!FB.IsLoggedIn) {
			OnFBLoginFailed(result.Error);
			return;	
		}
		
		OnFBLoggedIn ();
	}

	public void GetmyScore() {
		FB.API("me/score", HttpMethod.GET, MyScoreCallback);
	}

	public void MyScoreCallback(IGraphResult result) {
		Debug.Log (result.RawResult);
		if (result.Error == null) {
			List<object> Tempscores = Util.DeserializeScores (result.RawResult, "data");
			if(Tempscores == null || Tempscores.Count == 0) {
				MyScore = 0;
				MyScoreSucced ();
				return;
			}
			int TempCount = Tempscores.Count;
			Debug.Log (TempCount);
			//{"data":[{"user":{"name":"Abhi Verma","id":"100000902450660"},"score":10000,"application":{"name":"SpellTower","id":"614975348573527"}}]}

			var Con = Tempscores [0] as Dictionary<string, object>;
			//Debug.Log ("con" + Con);
			foreach (string key in Con.Keys) {
					switch (key) {				
					case "score": 				
							System.Int32.TryParse (Con [key].ToString (), out MyScore);
							break;					
					}		 
			}
			//Debug.Log (MyScore);
			MyScoreSucced ();		
				
		} else {
			MyScoreFailed (result.Error);
		}
	}
	
	public void GetAppScore(){
//		FB.API(FB.AppId+"/scores?friends.limit(10)", HttpMethod.GET, AppScoreCallback);
		FB.API ("/app/scores?fields=score,user.limit(30)", HttpMethod.GET, AppScoreCallback);
//		FB.API("/me?fields=id,first_name,friends.limit(10).fields(first_name,id)", HttpMethod.GET, GetMyDetailsCallBack);	// To get the list of friends of a player
	}

	public void AppScoreCallback(IGraphResult result) {       
		//FbDebug.Log ("ScoreCallback");  
		//FbDebug.Log (result.Text);
		
		ScoresIDs = new List<object> ();
		ScoresNames = new List<object> ();
		ScoresScore = new List<object> ();
		
		scores = Util.DeserializeScores (result.RawResult, "data");
		Count = scores.Count;
		//{"data":[{"user":{"name":"Abhi Verma","id":"100000902450660"},"score":10000,"application":{"name":"SpellTower","id":"614975348573527"}}]}
		for (int i=0; i<scores.Count; i++) {

			var Con = scores [i] as Dictionary<string, object>;
			object Id = "";
			object name = "";
			object scr = "";
			foreach (string key in Con.Keys) {
				
				switch (key) {				
				case "user": 				
					var temp = (Con [key] as Dictionary<string,object>);				
					name = temp ["name"];				
					Id = temp ["id"];	
					ScoresIDs.Add (Id.ToString ());

					if(Id.Equals (AccessToken.CurrentAccessToken.UserId)) {
						Myno = i;
					}

					ScoresNames.Add (name.ToString ());
					//FriendsDetails.Add (Id.ToString (), name.ToString ());				
					break;			
				case "score": 				
					scr = Con [key].ToString ();
					ScoresScore.Add (scr);	
					break;			
				case "application":
					
					break;			
				}		
			}    
		}

		if(System.Int32.Parse(FBIntegrate.Instance.ScoresScore[Myno].ToString ()) >= PlayerPrefs.GetInt("HighScore"))
			PlayerPrefs.SetInt("HighScore", System.Int32.Parse(FBIntegrate.Instance.ScoresScore[Myno].ToString ()));

		if(!FirstTime)
			OnGetAppScore ();  // Event fires after getting score from Facebook

		getProfileImage ();
	}

	public void PostAppScore(string Score){						// Post Score to the Facebook App
		//FbDebug.Log("Logged in. ID: " + FB.UserId);
		//FB.API("/me?fields=id,first_name,friends.limit(10).fields(first_name,id)", Facebook.HttpMethod.GET, GetDetailsCallBack);	// To get the list of friends of a player
		//FB.API("/me/scores", Facebook.HttpMethod.GET, MyScoreCallback);
		FB.API("/me/scores", HttpMethod.POST, SaveScoreCallback,EncryptScore(Score)); // To Push a score in player account
	}
	
	Dictionary<string,string> EncryptScore(string Score){					// Encrypt data so that score and wave can be send in a same string
		Dictionary<string,string> scoreData = new Dictionary<string, string> ();
		scoreData.Add ("score", Score);
		//Debug.Log (Score);
		return scoreData;
	}

	void SaveScoreCallback(IGraphResult result) {     
		//FbDebug.Log("SaveScoreCallback");
		//FbDebug.Log(result.Text);
		if (result.Error != null) {                                                                                                                          
//			FbDebug.Error(result.Error);
			OnScorePostFailedCall(result.Error);
			return;                                                                                                                
		} 
		OnScorePostCall ();
	}

	public void IsLikedPage(string PageID) {
		Debug.Log ("PageID "+PageID);
		FB.API("me/likes/"+PageID,HttpMethod.GET, LikePageCallback);
	}
	
	void LikePageCallback(IGraphResult result) {

		Debug.Log (result.RawResult);
		List<object> o = Util.DeserializeScores (result.RawResult, "data");
		//Debug.Log ("Object o " + o.Count);
		if (o.Count == 0)
			return;
		if (result.Error != null) {                                                                                                                          
//			FbDebug.Error (result.Error); 
			return;                                                                                                                
		} 
		
		OnLikePageCall ();
	}

	public void OpenUrl(string AppID) {
		//Debug.Log ("OpenUrl");
		//Application.ExternalEval("'http://m.facebook.com/"+AppID+"','width=500,height=300')");
		//Application.OpenURL("http://m.facebook.com/"+currentAppID);
		//Application.OpenURL("http://m.facebook.com/"+AppID);  //http://m.facebook.com/profile.php?id="   "fb://profile/613508015358935" 
		//Application.OpenURL("fb://profile/"+AppID);
		//try {
			//StartCoroutine ("OpenFacebookPage",AppID);
		//}
		//catch(UnityException e) {
		  Application.OpenURL ("https://m.facebook.com/" + AppID);
		//}
	}

	IEnumerator OpenFacebookPage(string AppID){
		Application.OpenURL("fb://profile/"+AppID);
		yield return new WaitForSeconds(1);
		if(leftApp){
			leftApp = false;
		}
		else{
			Application.OpenURL("https://m.facebook.com/"+AppID);
		}
	}

	bool leftApp = false;
	
	void OnApplicationPause(bool pauseStatus) {
		leftApp = pauseStatus;
	}

	public void PostStatusOntimeline() {
		FB.FeedShare (
		toId : "",
			link: new Uri("https://facebook.com/superdashrun"),
			linkName: "Super Dash Run!",
			linkCaption: "I thought up a witty tagline about Super Dash Run!",
			linkDescription:  "I'm playing Super Dash Run – I invite you to beat my score!",
		//picture: "https://example.com/myapp/assets/1/larch.jpg",
		callback: PostStatusOntimelineCallback);
	}

	public void PostStatusOntimeline(string LinkCaption, string LinkDescription) {
		FB.FeedShare (
			toId : "",
			link: new Uri("https://facebook.com/superdashrun"),
			linkName: "Super Dash Run!",
			linkCaption: LinkCaption,
			linkDescription:  LinkDescription,
			//picture: "https://example.com/myapp/assets/1/larch.jpg",
			callback: PostStatusOntimelineCallback);
	}

	void PostStatusOntimelineCallback(IShareResult response) {
		var result = Facebook.MiniJSON.Json.Deserialize(response.RawResult) as Dictionary<string, object>;

		if(result.ContainsKey ("cancelled")){
		     bool CancelledResult = (bool) result ["cancelled"];
		if (response.Error != null || CancelledResult) {                                                                                                                          
			//FbDebug.Error ( "PostStatusOntimelineCallback: "+response.Error);
			OnPostStatusFailedCall();
			return;                                                                                                                
		 } 
		}
			
		OnPostStatusCall ();
	}

	 public void getProfileImage() {
		//Debug.Log ("getProfileImage");
		ProfilePics = new List<Texture> ();
		IsGettingData = true;
		Timer = 0;
		TimeUp = false;
		StartCoroutine ("getProfileImageFromNet");
	}

	public Texture2D Mytexture ;

	public IEnumerator getProfileImageFromNet() {
		//Debug.Log ("getProfileImageFromNet");
		//FB.API("/me/picture", Facebook.HttpMethod.GET, MyPictureCallback);
		for (int i=0; i<ScoresIDs.Count; i++) {
		
			//Debug.Log (FB.IsLoggedIn);
			//Debug.Log (Timer);

			if (FB.IsLoggedIn) {
				WWW url = new WWW ("http" + "://graph.facebook.com/" + ScoresIDs[i].ToString () + "/picture");
				//UIGameManager.Instance.ShowWall ("Getting Data...");

				if(TimeUp) {
					OnTimeExceed();
					TimeUp = false;
					url.Dispose ();
					yield break;
				}
				yield return url;

				Texture2D textFb2 = new Texture2D (128, 128, TextureFormat.ARGB32, false); //TextureFormat must be DXT5
				url.LoadImageIntoTexture (textFb2);
				//profilePic.renderer.material.mainTexture = textFb2;
				if(ScoresIDs[i].Equals (AccessToken.CurrentAccessToken.UserId))
					Mytexture = textFb2;
				ProfilePics.Add (textFb2);
			}
		}
		if(!FirstTime)
			OnProfilePicsCall ();

		FirstTime = false;
	}

	
	void MyPictureCallback(IGraphResult result) {                                                                                                                              
		//FbDebug.Log("MyPictureCallback");                                                                                          
		//		FbDebug.Log(result.Text);
		//ProfilePic = result.Texture;
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
//			FbDebug.Error(result.Error);                                                                                           
			// Let's just try again                                                                                                
			FB.API("/me/picture", HttpMethod.GET, MyPictureCallback);                                
			return;                                                                                                                
		}                                                                                                                          
		
	} 

	void GetMyDetailsCallBack(IGraphResult result){
		//FbDebug.Log("GetDetailsCallBack");
		//FbDebug.Log (result.Text);
		
		if (result.Error != null) {                                                                                                                          
//			FbDebug.Error(result.Error);                                                                                                                       
			return;                                                                                                                
		} 
	}

}
