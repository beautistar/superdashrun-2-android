﻿using UnityEngine;
using System.Collections.Generic;

public class ProfileAchieveManager : MonoBehaviour {
	public static ProfileAchieveManager Instance;
	public GameObject ProfileAchieve;
	List<ProfileAchieveBtnProperties> profileAchievebtn = new List<ProfileAchieveBtnProperties>();
	MissionManager missionManager;
	public int InitialY = -1;
	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	public void setAchievements() {
		int k = 0;
		missionManager = MissionManager.instance;
		int CurrentLevel = missionManager.GetAssignmentLevel ();

		if (profileAchievebtn.Count > 0) {
			for (int i=0; i<profileAchievebtn.Count; i++)
				Destroy (profileAchievebtn [i].gameObject);
		}

		profileAchievebtn.Clear ();

		InitialY = -1;	
		for (int i=0; i<=CurrentLevel ; i++) {
			k = 0;

			for (int j=0; j<3; j++) {

				if(i == CurrentLevel)
				{
					if(missionManager.AssignmentStatus (j) == 1) {
					GameObject go = (GameObject)Instantiate (ProfileAchieve);
					go.transform.parent = this.transform;
					go.transform.localPosition = new Vector3 (0, InitialY, 0);
					profileAchievebtn.Add (go.GetComponent<ProfileAchieveBtnProperties> ());
					profileAchievebtn [profileAchievebtn.Count - 1].SetText (DataManager.instance.getMissionDescription ((MissionType)i + j + k));
					InitialY -= 1;
					k += 20;
					}
				}
				else {
					GameObject go = (GameObject)Instantiate (ProfileAchieve);
					go.transform.parent = this.transform;
					go.transform.localPosition = new Vector3 (0, InitialY, 0);
					profileAchievebtn.Add (go.GetComponent<ProfileAchieveBtnProperties> ());
					profileAchievebtn [profileAchievebtn.Count - 1].SetText (DataManager.instance.getMissionDescription ((MissionType)i + j + k));
					InitialY -= 1;
					k += 20;
				}
			}
				
		}

		if (profileAchievebtn.Count == 0) {
				GameObject go = (GameObject)Instantiate (ProfileAchieve);
				go.transform.parent = this.transform;
				go.transform.localPosition = new Vector3 (0, InitialY, 0);
				profileAchievebtn.Add (go.GetComponent<ProfileAchieveBtnProperties> ());
				profileAchievebtn [0].SetText ("No Achievement yet");
				InitialY -= 1;
			}
		}
}
