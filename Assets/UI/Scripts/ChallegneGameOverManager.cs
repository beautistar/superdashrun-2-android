using UnityEngine;
using System.Collections;
using System;

public class ChallegneGameOverManager : MonoBehaviour {

	public static ChallegneGameOverManager Instance;
	public tk2dTextMesh DailyChallengeText;
	public tk2dTextMesh DailyHeaderText;
	public tk2dTextMesh DailyTimerText;
	public tk2dTextMesh DailyFootertext;
	public tk2dSprite DailyIcon;
	public tk2dTextMesh DailyIconText;
	DailyChallengeStaticData dailyChallengeStaticData;

	public GameObject DailyChallenge;
	public GameObject WeeklyChallenge;
	public GameObject GameOverScreen;
	public ChallengeBtn DailychallengeBtn;
	public ChallengeBtn WeeklychallengeBtn;
	public tk2dTextMesh WeeklyChallengeText;
	WeeklyChallengeStaticData weeklyChallengeStaticData;

	bool StartTimer = false;

	void Awake () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		if (!StartTimer)
			return;
		
		CountTimer ();
	}
	void OnEnable() {
		ShowGameOverScreen ();
	}

	public void ShowWeeklyChallenge(int Order) {
		GameOverScreen.SetActive (false);
		WeeklyChallenge.SetActive (true);
		DailyChallenge.SetActive (false);
		SetWeeklyChallengeDetails (Order);
	}

	public void OnClickWeeklyNext() {
		if (ChallengerManager.Instance.IsDailyChallengeCompleted) {
			ShowDailyChallenge();	
			return;
		}
		ShowGameOverScreen ();
	}

	public void ShowDailyChallenge() {
		GameOverScreen.SetActive (false);
		WeeklyChallenge.SetActive (false);
		DailyChallenge.SetActive (true);
		SetDailyChallengeDetails ();
	}
	
	public void OnClickDailyNext() {
		ShowGameOverScreen ();
	}

	public void ShowGameOverScreen() {
		GameOverScreen.SetActive (true);
		WeeklyChallenge.SetActive (false);
		DailyChallenge.SetActive (false);
	}

	public void SetWeeklyChallengeDetails(int Order) {
		weeklyChallengeStaticData = WeeklyChallengeStaticData.Instance;
		WeeklyChallengeText.text = 	WeeklyChallengeStaticData.Instance.GetWeeklyChallName (Order).Split (' ')[0]+" " +  WeeklyChallengeStaticData.Instance.GetWeeklyChallValue (Order).ToString () +" " + WeeklyChallengeStaticData.Instance.GetWeeklyChallName (Order).Split (' ')[1];;
		WeeklychallengeBtn.SetDetailsWeekly (Order);
	}

	public void SetDailyChallengeDetails() {
		dailyChallengeStaticData = DailyChallengeStaticData.Instance;
		DailyChallengeText.text = dailyChallengeStaticData.GetDailyChallengeName(dailyChallengeStaticData.GetDailyCurrentChallengeType());
		DailyIcon.SetSprite (dailyChallengeStaticData.GetSymbolName(dailyChallengeStaticData.GetDailyCurrentChallengeOrder()  ));
		if (DailyChallengeStaticData.Instance.GetDailyCurrentChallengeOrder () < DailyChallengeStaticData.Instance.TotalBtns)
			DailyIconText.text = DailyChallengeStaticData.Instance.GetCoins(DailyChallengeStaticData.Instance.GetDailyCurrentChallengeOrder()).ToString ();
		else
			DailyIconText.text = "";
		DailychallengeBtn.SetDetails (dailyChallengeStaticData.GetDailyCurrentChallengeOrder() - 1 , true);
		DailyHeaderText.text = "Next Challenge In";
		DailyFootertext.text = "Come Back And Win";
		StartTimer = true;
	}

	void SetNewDailyChallenge() {
		DailyHeaderText.text = dailyChallengeStaticData.GetDailyChallengeName(dailyChallengeStaticData.GetDailyCurrentChallengeType());
		DailyTimerText.text = "";
		DailyFootertext.text = "";
		StartTimer = true;
		DailyIcon.SetSprite (dailyChallengeStaticData.GetSymbolName(dailyChallengeStaticData.GetDailyCurrentChallengeOrder()));
		if (DailyChallengeStaticData.Instance.GetDailyCurrentChallengeOrder () < DailyChallengeStaticData.Instance.TotalBtns)
			DailyIconText.text = DailyChallengeStaticData.Instance.GetCoins(DailyChallengeStaticData.Instance.GetDailyCurrentChallengeOrder()).ToString ();
		else
			DailyIconText.text = "";

	}

	void CountTimer() {
		DateTime now = DateTime.Now.Date;
		DateTime TodayDate = new DateTime(now.Year, now.Month, now.Day  , DateTime.Now.TimeOfDay.Hours, DateTime.Now.TimeOfDay.Minutes, DateTime.Now.TimeOfDay.Seconds);
		DateTime NextDate;
		NextDate = now.AddDays (1).AddHours (00).AddMinutes (00).AddSeconds (00);
		//NextDate.TimeOfDay = new TimeSpan (00, 00, 00);

		/*if (DateTime.DaysInMonth (now.Year, now.Month) >= (now.Day + 1)) {
			NextDate = new DateTime(now.Year, now.Month, now.Day , 00 ,00 , 00); 
		} else {
			NextDate = new DateTime(now.Year, now.Month, now.Day + 1 , 00 ,00 , 00); 
		}*/
		//Debug.Log (TodayDate);
		//Debug.Log (NextDate);
		
		TimeSpan timeStamp = NextDate.Subtract (TodayDate);
		DailyTimerText.text =  string.Format("{0:00} : {1:00} : {2:00}",timeStamp.Hours,timeStamp.Minutes, timeStamp.Seconds);
		if (timeStamp.Days > 0) {
			OnTimeUp();
		}
		//Debug.Log (timeStamp.Hours + " " + timeStamp.Minutes + " " + timeStamp.Seconds);
	}
	
	void OnTimeUp() {
		StartTimer = false;
		ChallengerManager.Instance.IntializeDailyChallenge ();
		SetNewDailyChallenge ();
	}
}
