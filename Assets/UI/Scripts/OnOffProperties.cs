﻿using UnityEngine;
using System.Collections;

public class OnOffProperties : MonoBehaviour {
	public SettingBtnProperties settingBtn;
	public Btn2Properties Btn2Instance;
	public GameObject Off;
	public GameObject On;
	public ButtonSlideScript OnScript;
	public ButtonSlideScript OffScript;

	public bool Active = false;
	public int OrderInList;

	// Use this for initialization
	void Start () {
	
	}

	public void OnClick(){
		SetActiveBtn ();
	}

	public void OnOn(){

		Off.SetActive (true);
		On.SetActive (false);
		Active = true;
		Btn2Instance.SlideScript = OffScript;
	}

	public void OnOff(){
		Off.SetActive (false);
		On.SetActive (true);
		Active = false;
		Btn2Instance.SlideScript = OnScript;
	}

	 void SetActiveBtn() {
		if(Active) {
			OnOff ();
		}
		else {
			OnOn ();
		}
		settingBtn.OnItemClick ();
	}
}
