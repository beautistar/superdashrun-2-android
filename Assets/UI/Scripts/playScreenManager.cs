using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class playScreenManager : MonoBehaviour {
	public static playScreenManager Instance;
	public tk2dSprite fB;
	public tk2dSprite Twitter;
	public GameObject SuperDash;
	public GameObject Run;
	public GameObject line;
	public GameObject punchLine;
	public GameObject StartFromScreen;
	public GameObject AcheivementBtn;
	public GameObject LeaderBoardBtn;
	public GameObject GoogleplusBtn;

	bool IsSecondTime = false;
	bool IsCalledFromThis = false;

	// Use this for initialization
	void Awake () {

		Instance = this;
		if (UIGameManager.Instance.IsAmazon) {
			AcheivementBtn.SetActive(false);
			LeaderBoardBtn.SetActive(false);
			GoogleplusBtn.SetActive(false);	
		}
	}

	void OnEnable() {
		FBIntegrate.OnFBLoggedIn += HandleOnFBLoggedIn;
		FBIntegrate.OnFBLoginFailed += HandleOnFBLoginFailed;
		FBIntegrate.OnGetAppScore += HandleOnGetAppScore;
		FBIntegrate.OnProfilePicsCall += HandleOnProfilePicsCall;
		FBIntegrate.OnTimeExceed += HandleOnTimeExceed;
		IsCalledFromThis = false;
		if (IsSecondTime)
			Init ();
	}

	void OnDisable() {
		FBIntegrate.OnFBLoggedIn -= HandleOnFBLoggedIn;
		FBIntegrate.OnFBLoginFailed -= HandleOnFBLoginFailed;
		FBIntegrate.OnGetAppScore -= HandleOnGetAppScore;
		FBIntegrate.OnProfilePicsCall -= HandleOnProfilePicsCall;
		FBIntegrate.OnTimeExceed -= HandleOnTimeExceed;
	}

	void Start(){
		Init ();
		StartFromScreen.SetActive (false);
	}

	void Init() {

		if (FB.IsLoggedIn) {
			fB.SetSprite ("fbicon");	
		} else {
			fB.SetSprite ("fblogin");
		}

		if (TwitterManage.IsLogined) {
			Twitter.SetSprite ("tweetcion");	
		} else {
			Twitter.SetSprite ("twiterlogin");
		}

		IsSecondTime = true;
	}
	

	public void LoginFacebookPlayScreen(){
		IsCalledFromThis = true;
		if (!FB.IsLoggedIn) {
			UIGameManager.Instance.ShowWall ("Connecting...",15);
			FBIntegrate.Instance.Login ();	
		} 
		else {
			fB.SetSprite ("fbicon");
			UIGameManager.Instance.PopUp ("Already Connected...");
		}

		//TwitterBinding.CheckNetwork ();
	}
	
	void HandleIsNetworkon (string obj){
		if (obj.Equals ("1")) {
			if (!FB.IsLoggedIn) {
				UIGameManager.Instance.ShowWall ("Connecting...",15);
				FBIntegrate.Instance.Login ();	
			} 
			else {
				UIGameManager.Instance.PopUp ("Already Connected...");
			}
		}
		else {
			UIGameManager.Instance.PopUp ("No Network Found");
		}
	}
	
	void HandleOnFBLoggedIn () {
		print ("HandleOnFBLoggedIn");
		fB.SetSprite ("fbicon");

		//FBIntegrate.Instance.GetAppScore ();
	}

	void HandleOnGetAppScore()	{
		UIGameManager.Instance.ShowWall ("Getting Data...",25);
		//FBIntegrate.Instance.getProfileImage ();
	}
	
	void HandleOnProfilePicsCall(){
		if (UIGameManager.Instance.IsWall) {
			UIGameManager.Instance.HideWall ();
			UIGameManager.Instance.PopUp ("Connected to Facebook");
		}
	}

	void HandleOnTimeExceed (){
		HandleOnFBLoginFailed ("Error");
		if (UIGameManager.Instance.IsWall) {
			UIGameManager.Instance.HideWall ();
			UIGameManager.Instance.PopUp ("Connected to Facebook");
		}
	}


	void HandleOnFBLoginFailed (string error) {
		FB.LogOut ();
		fB.SetSprite ("fblogin");
		print ("HandleOnFBLoginFailed "+error);

		if (!IsCalledFromThis)
		return;

		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
	}

	public void LoginTwitterPlayScreen() {

		if (!TwitterManage.IsLogined) {
			Twitter.SetSprite ("twiterlogin");
			TwitterManage.Instance.Login ();
			UIGameManager.Instance.ShowWall ("Connecting...");
		}
		else {
			Twitter.SetSprite ("tweetcion");
			UIGameManager.Instance.PopUp ("Already Connected...");
		}
	}

	public void PlayLogoAnimation()	{
		//Debug.Log (Resolutions.GetWidth ());
		SuperDash.SetActive (true);
		SuperDash.transform.localPosition = new Vector3 (-Resolutions.GetWidth (), SuperDash.transform.localPosition.y, 0);
		line.SetActive (true);
		line.transform.localPosition = new Vector3 (-Resolutions.GetWidth (), line.transform.localPosition.y, 0);
		punchLine.SetActive (true);
		punchLine.transform.localPosition = new Vector3 (-Resolutions.GetWidth (), punchLine.transform.localPosition.y, 0);
		Run.SetActive (true);
		Run.transform.localPosition = new Vector3 (-Resolutions.GetWidth (), Run.transform.localPosition.y, 0);
		float y = 0;
		float epicY = 0;
	
		if (Resolutions.GetWidth () >= 8 && Resolutions.GetWidth () <= 9.5) {		 
			y = (Resolutions.GetWidth () / 4f);
			epicY = (Resolutions.GetWidth () / 6.2f); 
		}
		if (Resolutions.GetWidth () >= 9.6 && Resolutions.GetWidth () <= 11) {		 
			y = (Resolutions.GetWidth () / 3.8f);
			epicY = (Resolutions.GetWidth () / 6.2f); 
		}
		if (Resolutions.GetWidth () >= 11 && Resolutions.GetWidth () <= 13) {		 
			y = (Resolutions.GetWidth () / 3.3f);
			epicY = (Resolutions.GetWidth () / 7f); 
		}
		iTween.MoveBy(SuperDash, iTween.Hash("x", Resolutions.GetWidth () + (y/2.5f),"time",1, "easeType", "easeInOutExpo", "loopType", "none", "delay", 0));
		iTween.MoveBy(Run, iTween.Hash("x", Resolutions.GetWidth () + y - 0.5f,"time",1, "easeType", "easeInOutExpo", "loopType", "none", "delay", 0.4f));
		//iTween.MoveBy(line, iTween.Hash("x", Resolutions.GetWidth () -lineY +y,"time",1, "easeType", "easeInOutExpo", "loopType", "none", "delay", 0.5f));
		iTween.MoveBy(punchLine, iTween.Hash("x", Resolutions.GetWidth () +y,"time",1, "easeType", "easeInOutExpo", "loopType", "none", "delay", 1f));
		StartCoroutine ("OnComplete");
	}

	IEnumerator OnComplete() {
		yield return new WaitForSeconds (2);
		GetComponent<Animation>().Play ();
	}

	void ActivateOnScreenPlayButton() {
		//StartFromScreen.SetActive (true);
		// init game center here
	}
}
