﻿using UnityEngine;
using System.Collections;

public class Resolutions : MonoBehaviour {
	static float PerPixel;
	static float Unit;
	public AudioSource AudioSrc;

	void Awake() {
		PerPixel = Screen.width/(Screen.height/16.0f);
	}

	
	// Update is called once per frame
	void Update () {
		return;
		if( !MyGUIManager.instance.StartBGMusic) {
			return;
		}

		if(GameManager.instance != null)
		{
			if(GameManager.instance.IsGameRunnnig == false)
			{
				if(SettingStaticData.Instance.getSoundOnOff())
				{
					if(!AudioSrc.isPlaying)
						AudioSrc.Play();

					AudioSrc.volume = (SettingStaticData.Instance.getSoundValue())/2;
				}
				else
				{
					AudioSrc.Stop();
				}
			}
			else
			{
				AudioSrc.Stop();
			}
		}
	}

	public static float GetWidth()
	{
		return  PerPixel;
	}
	

}
