﻿using UnityEngine;
using System.Collections;
using System;
using Facebook.Unity;

public class LoadingManager : MonoBehaviour {
	//public GameObject LoadingScreen;
	public static LoadingManager Instance;
	public static event Action OnLoadComplete;
	public tk2dTextMesh Title;
	public string path = "com.zidapps.superdashrun.MainActivity";
	public GameObject loadingBar;
	bool IsLoaded = false;
	float timer = 0;
	float StopTimer = 7;
	string str = "Fun At Your FingerTips";


	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	void Update() {
	}

	void OnEnable() {
		//AdMobManager.ReceivedAd += HandleReceivedAd;
		//AdMobManager.FailedToReceiveAd +=  HandleFailedToReceiveAd;
		FBIntegrate.OnInitComplete += HandleOnInitComplete;
//		TwitterManager.IsUserlogin += HandleIsUserlogin;
		FBIntegrate.OnGetAppScore += HandleOnGetAppScore;
		FBIntegrate.OnProfilePicsCall += HandleOnProfilePicsCall; 
	}

	// Update is called once per frame
	public void LoadFiles () {
		if (UIGameManager.Instance.IsTesting) {
			//PlayLineAnimation();
			return;		
		}
		ShowLoadingTitleAnimation ();
	}

	public void ShowLoadingTitleAnimation() {
		str = Title.text;
		Title.text = "";
		StartCoroutine ("DelayText");
	}
	
	IEnumerator DelayText() {

		//Debug.LogError("=====>> REMOVE HERE CODE");
		//PlayerPrefs.DeleteAll();


		int barCount = 10;
		float barWidth = 1.0f / (float)barCount;
		// increase loading bar. 10 pieces
		for (int i=0; i < barCount/*str.Length*/; i++) {
			Vector3 scale = loadingBar.transform.localScale;
			scale.x += barWidth;
			loadingBar.transform.localScale = scale;
			//Title.text += str[i];

			yield return new WaitForSeconds(0.09f);
		}
		//GetComponent<Animation>().Play ("HandAnimation");

		FBIntegrate.Instance.InIt ();

//		BankIntializer.Instance.Init ();

//		TwitterGuiManager.Instance.Init ();


        if (Pushup.Instance.GetPush ())
        {
            //Debug.Log("Area730Push: before init()");
            //PushioInit();
        }



		yield return new WaitForSeconds (1);

		OnEndLineAnimation();
	}


	void PushioInit() {
		#if UNITY_ANDROID	
		AndroidJavaClass pluginClass = new AndroidJavaClass (path); //com.finoit.plugin.FinoitPluginJava // com.google.unity.AdMobPlugin
		pluginClass.CallStatic ("Init", new object[0] {});	
		#endif
	}

	void ParsePush() {
#if UNITY_ANDROID
        Debug.Log("Area730Push: Parse Push()");
        AndroidJavaClass pluginClass = new AndroidJavaClass (path);
        //Production keys
        //pluginClass.CallStatic ("InitParsePush", new object[2] {"OO7m89Yo4nk3DPeefEJQfwaXyHYewwokqSFmZaF5","haniNF5Adj9FZc2dAiKvsa3GizPgpJfT0wkXoYvA"});	

        // Dev keys
        pluginClass.CallStatic("InitParsePush", new object[2] { "5hazED5A3pQBxAbJpJVrqKdalqdwpLXruo5Ibwc1", "ejlf4tE3btbkiEjnOnyO2H2fhWCJe03j7K2pOBRJ" });
#endif
    }


	public void PlayLineAnimation() {
		//Debug.Log("Before line anim");
		//GetComponent<Animation>().Play ("LineLoading");
		//Debug.Log("Start fb anim");
		FBIntegrate.Instance.InIt ();

		OnEndLineAnimation();
	}

	public void OnEndLineAnimation() {
		OnFailedToEnd ();
	}

	void HandleOnInitComplete () {
		if (IsLoaded)
			return;

		if (FB.IsLoggedIn) {
			FBIntegrate.Instance.GetAppScore ();
			FBIntegrate.Instance.getProfileImage ();
		}
	}

	void HandleOnGetAppScore()	{
		if (IsLoaded)
			return;
	}
	
	void HandleOnProfilePicsCall(){
		if (IsLoaded)
			return;
	}
	
	void HandleOnbankIntialize () {
		if (IsLoaded)
			return;
	}

	void HandleIsUserlogin (string error) {
		if (IsLoaded)
			return;
	}

	void HandleFailedToReceiveAd (string obj)	{
		if (IsLoaded)
			return;
	}
		
	void HandleReceivedAd () {
		if (IsLoaded)
			return;
	}

	void OnFailedToEnd() {
		UIGameManager.Instance.HideWall ();
		OnLoadComplete ();
		IsLoaded = true;
		this.gameObject.SetActive (false);
	}

}