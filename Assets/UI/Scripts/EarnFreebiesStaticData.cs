﻿using UnityEngine;
using System.Collections;

public enum EarnTypes { Coins , LifeSaver};

public class EarnFreebiesStaticData : MonoBehaviour {
	public static EarnFreebiesStaticData Instance;

	public int OffersCoinsCount;
	public string[] OffersCoinsTitle;
	public string[] OffersCoinsDescription;
	public int[] OffersCoins;
	public string[] OffersCoinsImages;
	public Color[] OffersCoinsColor;

	public string[] CoinsEarnbtnTexts;
	public string[] LifesaversEarnbtnTexts;

	public int OffersLifeSaverItemsCount = 1;
	public string[] OffersLifeSaverTitles;
	public string[] OffersLifeSaverDescription;
	public int[] OffersLifeSavers;
	public string[] OffersLifeSaversImages;
	public Color[] OffersLifeSaversColor;

	void Awake(){
		if (Instance == null || Instance != this)
			Instance = this;
	}

	void Start()
	{
		//ClearAll ();
	}

	public int getOffersCoinsCount(){
		return OffersCoinsCount;
	}
	
	public string getOffersCoinTitle(int Order){
		return OffersCoinsTitle [Order];
	}

	public string getOffersCoinDesciption(int Order){
		return OffersCoinsDescription [Order];
	}
	
	public int getOffersCoins(int Order){
		return OffersCoins [Order];
	}
	
	public int getOffersLifeSaverCount(){
		return OffersLifeSaverItemsCount;
	}
	
	public string getOffersLifeSaverTitle(int Order){
		return OffersLifeSaverTitles [Order];
	}

	public string getOffersLifeSaversDesciption(int Order){
		return OffersLifeSaverDescription [Order];
	}

	public int getOffersLifeSavers(int Order){
		return OffersLifeSavers [Order];
	}

	public string getOffersCoinsEarnText(int Order){
		return CoinsEarnbtnTexts [Order];
	}

	public string getOffersLifesaverEarnText(int Order){
		return LifesaversEarnbtnTexts [Order];
	}

	public void setOfferEarned(EarnTypes earnType, int Order) {
		PlayerPrefs.SetInt (string.Format ("Earn{0}{1}",((int) earnType), Order),1);
	}

	public int getOfferEarned(EarnTypes earnType, int Order) {
		return PlayerPrefs.GetInt(string.Format ("Earn{0}{1}",((int) earnType), Order),0);
	}

	public string getOffersCoinsImage(int Order)
	{
		return OffersCoinsImages[Order];
	}
	
	public Color getOffersCoinsColor(int Order)
	{
		return OffersCoinsColor[Order];
	}
	
	public string getOffersLifeSaverImage(int Order)
	{
		return OffersLifeSaversImages[Order];
	}
	
	public Color getOffersLifeSaverColor(int Order)
	{
		return OffersLifeSaversColor[Order];
	}

	public void ClearAll()
	{
		for (int i=0; i<OffersCoinsCount; i++) {
			PlayerPrefs.SetInt (string.Format ("Earn{0}{1}",(int) EarnTypes.Coins,i ) ,0);	
		}

		for (int i=0; i<OffersLifeSaverItemsCount; i++) {
			PlayerPrefs.SetInt (string.Format ("Earn{0}{1}",(int) EarnTypes.LifeSaver,i ),0);	
		}
	}

}
