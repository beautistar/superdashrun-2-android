﻿using UnityEngine;
using System.Collections;

public class MenuCircle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	

	void OnMouseDown()
	{
		GetComponent<tk2dSprite> ().SetSprite ("playbtn_activel");
	}

	void OnMouseUp()
	{
		GetComponent<tk2dSprite> ().SetSprite ("playbtn_normal");
	}

	 void ToLoadLevel(){

		BgAnimation bgAnim = BgAnimation.instance;
		FatherAnimation fatherAnim = FatherAnimation.instance;
		CharactersAnimation characterAnim = CharactersAnimation.instance;
		CharactersAnimationStay characterAnimStay1 = CharactersAnimationStay.instance;
		bgAnim.OnPlayButton ();
		fatherAnim.OnPlayButton ();
		characterAnimStay1.OnPlayButton ();	
		characterAnim.OnPlayButton ();
		
		StartCoroutine (ToStartGame ());
		
		UIGameManager.Instance.GoogleAnalyticsScreenName ("PlayGame");
		/*BgAnimation bgAnim = BgAnimation.instance;
		FatherAnimation fatherAnim = FatherAnimation.instance;
		CharactersAnimation characterAnim = CharactersAnimation.instance;
		bgAnim.OnPlayButton ();
		fatherAnim.OnPlayButton ();
		characterAnim.OnPlayButton ();
		StartCoroutine (ToStartGame ());*/
		//GameManager.instance.startGame(false);	
	}

	private IEnumerator ToStartGame()
	{
		yield return new WaitForSeconds(0.8f);
		CharactersAnimation characterAnim = CharactersAnimation.instance;
		characterAnim.SetMyLayOut (false);
		yield return new WaitForSeconds(0.1f);
		GameManager.instance.startGame(false);
		//yield return new WaitForSeconds(0.05f);
		MyGUIManager.instance.SetIntroVideo(false);
	}

}
