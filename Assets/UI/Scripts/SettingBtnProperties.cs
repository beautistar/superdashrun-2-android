﻿using UnityEngine;
using System.Collections;

public class SettingBtnProperties : MonoBehaviour {
	public tk2dTextMesh Title;
	public tk2dTextMesh Description;
	public int OrderInList;
	public OnOffProperties OnOff;
	public tk2dSprite sprite;

	// Use this for initialization
	void Start () {
	
	}

	public void OnItemClick()
	{
		SettingManager.Instance.OnItemClick (OrderInList);
	}

	public void OnFaceBookClick()
	{
		SettingManager.Instance.OnFacebookClick ();
	}

	public void OnTwitterClick()
	{
		SettingManager.Instance.OnTwitterClick ();
	}

	public void OnApptentiveClick()
	{
		SettingManager.Instance.OnApptentiveClick ();
	}


}
