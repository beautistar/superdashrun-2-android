﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class LocalNotification : MonoBehaviour {

	public static LocalNotification Instance;
	public string Androidpath = "com.zidapps.superdashrun2";
	static string path = "com.zidapps.superdashrun2";

	#if UNITY_IPHONE
	[DllImport("__Internal")]
	private static extern void _addAlarm( );
	
	[DllImport("__Internal")]
	private static extern void _cancelAlarm( );
	#endif

	// Use this for initialization
	void Awake () {
		Instance = this;
		path = Androidpath;
	}

	void Start() {
		if(DailyChallengeStaticData.Instance.GetTodayVisited ())
			SetAlaram ();
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void SetAlaram() {
		path = Androidpath;
		CancelAlarmNative ();
		AddAlarmNative ();
		DailyChallengeStaticData.Instance.SetTodayVisited (0);                                              
	}

	public void CancelAlarm() {
		if((DailyChallengeStaticData.Instance.GetDailyCurrentChallengeOrder() > 0) && DailyChallengeStaticData.Instance.IsDailyChallengeDayPassed () && !DailyChallengeStaticData.Instance.GetTodayVisited ()){
			DailyChallengeStaticData.Instance.SetTodayVisited (1);
			CancelAlarmNative ();
			return;
		}
		DailyChallengeStaticData.Instance.SetTodayVisited (0);
	}


	public static void AddAlarmNative() {
		#if UNITY_ANDROID
		Debug.Log ("SetAlarm");
		AndroidJavaClass playerClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");	
		AndroidJavaClass pluginClass = new AndroidJavaClass (path); 
		pluginClass.CallStatic ("setAlarm", new object[0] {});	//setAlarm
		#endif
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			_addAlarm( );
		}
		#endif
	}
	
	public static void CancelAlarmNative() {
		return;
		#if UNITY_ANDROID
		Debug.Log ("cancelAlarm");
		AndroidJavaClass playerClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");	
		AndroidJavaClass pluginClass = new AndroidJavaClass (path); 
		pluginClass.CallStatic ("cancelAlarm", new object[0] {});	
		#endif
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			_cancelAlarm( );
		}
		#endif
	}
	public static void TestAlarmNative() {
		#if UNITY_ANDROID
		Debug.Log ("TestsetAlarm");
		AndroidJavaClass playerClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");	
		AndroidJavaClass pluginClass = new AndroidJavaClass (path); 
		pluginClass.CallStatic ("TestsetAlarm", new object[0] {});	
		#endif
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			_cancelAlarm( );
		}
		#endif
	}

}
