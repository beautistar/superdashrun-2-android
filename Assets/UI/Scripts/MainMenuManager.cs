﻿using UnityEngine;
using System.Collections;

public class MainMenuManager : MonoBehaviour {
	public static MainMenuManager Instance;
	public tk2dTextMesh ScreenText;
	public tk2dTextMesh PlayerText;
	public GameObject[] Screens;
	static GameObject Active;
	public tk2dSprite MenuSprite;
	// Use this for initialization
	void Awake () {
		Instance = this;
		for(int i=0;i<Screens.Length;i++) {
			Screens [i].SetActive (false);
		}
		Instance.PlayerText.text = DataManager.instance.getCharacterTitle(DataManager.instance.getSelectedCharacter ());
		Screens [0].SetActive (true);
		Active = Screens [0];
//		GoogleAnalyticsScreenName (0);
	}

	void Start() {
		MenuSprite.SetSprite (StoreStaticData.Instance.getCharacterProfileImage (DataManager.instance.getSelectedCharacter ()));
	}
	
	// Update is called once per frame
	void Update () {

	}

	public static void LoadScreen(string ScreenName) {
		int ScreenNo = System.Int16.Parse (ScreenName.Substring (0,2));
		Instance.ScreenText.text = ScreenName.Substring (2);
		Active.SetActive (false);
		Instance.Screens [ScreenNo].SetActive (true);
		Active = Instance.Screens [ScreenNo];
		GoogleAnalyticsScreenName (ScreenNo);
		//Instance.PlayAnimation (ScreenNo);

		if (ScreenNo == 0) {
			Instance.MenuSprite.SetSprite (StoreStaticData.Instance.getCharacterProfileImage (DataManager.instance.getSelectedCharacter ()));
			Instance.PlayerText.text = DataManager.instance.getCharacterTitle(DataManager.instance.getSelectedCharacter ());
		}
		if(ScreenNo == 9)
			GameManager.instance.showStore(true);
	}

	public void PlayAnimation(int ScreenNo)
	{
		string ScreenName = "";
		switch(ScreenNo)
		{
		case 0:  Screens[0].GetComponent<Animation>().Play ("Mainmenu"); Debug.Log ("Mainmenu"); break;
		case 1: ScreenName = "Assignmnet."+ MissionManager.instance.GetAssignmentLevel();  break;
		case 2: ScreenName = "Bank"; break;
		case 3: ScreenName = "Credits"; break;
		case 4: ScreenName = "Earn Freebies";break;
		case 5: ScreenName = "LeaderBorad"; break;
		case 6: ScreenName = "More Games"; break;
		case 7: ScreenName = "Profile"; break;
		case 8: ScreenName = "Setting";  break;
		case 9: ScreenName = "Store"; break;
		case 10: ScreenName = "Change Player"; break;
		case 11: ScreenName = "Challenger"; break;	
		}
	}

	public static void GoogleAnalyticsScreenName(int ScreenNo)
	{
		string ScreenName = "";
		switch(ScreenNo)
		{
		case 0: ScreenName = "Main Menu";  break;
		case 1: ScreenName = "Assignmnet."+ MissionManager.instance.GetAssignmentLevel();  break;
		case 2: ScreenName = "Bank"; break;
		case 3: ScreenName = "Credits"; break;
		case 4: ScreenName = "Earn Freebies";break;
		case 5: ScreenName = "LeaderBorad"; break;
		case 6: ScreenName = "More Games"; break;
		case 7: ScreenName = "Profile"; break;
		case 8: ScreenName = "Setting";  break;
		case 9: ScreenName = "Store"; break;
		case 10: ScreenName = "Change Player"; break;
		case 11: ScreenName = "Challenger"; break;

		}

		UIGameManager.Instance.GoogleAnalyticsScreenName (ScreenName);
	}

}
