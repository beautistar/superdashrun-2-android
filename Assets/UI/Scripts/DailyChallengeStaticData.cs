﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DailyChallengeStaticData : MonoBehaviour {
	public static DailyChallengeStaticData Instance ;

	public GameObject DailyChallengerBtn;
	public int TotalBtns;
	public Color[] BtnsColor ;
	public string[] SymbolName;
	public int[] Coins;
	public string[] HeaderName;
	public string[] CoinsIcon;
	public string AfterDaysSymbolName;
	public int MinCoinsAfterDay = 10;
	public int MaxCoinsAfterDay = 100;
	// Use this for initialization
	void Awake () {
		Instance = this;
		SetDailyChallValues ();

	}

	void SetDailyChallValues() {
		for (int i=0; i<ChallengesTypesData.TotalDailyChallenges; i++) {
			ChallengesTypesData.DailyChallengesTypesValues[i] = GetDailyChallengesValuesIncremented (i);
		}
	}
	
	public int GetDailyCurrentChallengeOrder() {
		return PlayerPrefs.GetInt ("DailyCurrentChallenge", 0);
	}
	
	public void SetDailyCurrentChallengeOrder(int Order) {
		PlayerPrefs.SetInt ("DailyCurrentChallenge" ,Order);
	}
	



	public int GetDailyCurrentChallengeType() {
		return PlayerPrefs.GetInt ("DailyCurrentChallengeType", 0);
	}

	public void SetDailyCurrentChallengeType(int Order) {
		PlayerPrefs.SetInt ("DailyCurrentChallengeType" ,Order);
	}



	public Color GetBtnsColor(int Order){
		return BtnsColor [Order];
	}

	public string GetSymbolName(int Order){
		if (Order >= TotalBtns)
			return AfterDaysSymbolName;
		return SymbolName [Order];
	}

	public int GetCoins(int Order){
		if (Order < TotalBtns) {
			return Coins [Order];	
		}
		if (GetDailyCurrentChallengeOrder () >= TotalBtns)
			return GetCoinsRandomValue();
		return Coins [Order];
	}

	public int GetCoinsRandomValue() {
		return PlayerPrefs.GetInt ("CoinsRandomValue", 0);
	}

	public void SetCoinsRandomValue() {
		if (GetDailyCurrentChallengeOrder () >= TotalBtns) {
			int Val = UnityEngine.Random.Range (MinCoinsAfterDay, MaxCoinsAfterDay);
			Val = Val * 100;
			PlayerPrefs.SetInt ("CoinsRandomValue", Val);
		}
	}

	public string GetHeaderName(int Order){
		return HeaderName [Order];
	}

	public string GetCoinsIcon(int Order){
		return CoinsIcon [Order];
	}
	



	public bool GetIsDailyChallengeCompleted(int Order) {
		if (PlayerPrefs.GetInt (string.Format("DailyChallengeCompleted{0}", Order), 0) == 1)
			return true;
		return false;
	}
	
	public void SetDailyChallengeCompleted(int Order) {
		if (PlayerPrefs.GetInt (string.Format("DailyChallengeCompleted{0}", Order), 0) == 1)
			PlayerPrefs.SetInt (string.Format("DailyChallengeCompleted{0}", Order), 0);
		else
			PlayerPrefs.SetInt (string.Format("DailyChallengeCompleted{0}", Order), 1);
	}




	public List<int> GetDailyCompletedChallengesTypes() {
		List<int> list = new List<int> ();
		for (int i=0; i<GetDailyCurrentChallengeOrder() ; i++) {
			list.Add (PlayerPrefs.GetInt (string.Format("DailyCompletedChallengesTypes{0}", i),-1));
		}
		return list;
	}

	public void SetDailyCompletedChallengesTypes(int Value) {
		int Total = GetDailyCurrentChallengeOrder ();
		PlayerPrefs.SetInt (string.Format ("DailyCompletedChallengesTypes{0}", Total), Value);
	}




	public string GetDailyChallengeLastDate() {
		return PlayerPrefs.GetString ("DailyChallengeLastDate",System.DateTime.Now.Date.ToString ());
	}

	public void SetDailyChallengeLastDate() {
		PlayerPrefs.SetString ("DailyChallengeLastDate",DateTime.Today.Date.ToString ());
	}




	public int GetDailyChallengeSavedValue() {
		return PlayerPrefs.GetInt ("DailyChallengeSavedValue",0);
	}

	public void SetDailyChallengeSavedValue(int Value) {
		PlayerPrefs.SetInt ("DailyChallengeSavedValue", Value);
	}




	public bool IsTodayDailyChallengeCleared() {
		if (PlayerPrefs.GetInt ("TodayDailyChallengeCleared", 0) == 1)
			return true;
		return false;
	}

	public void SetIsTodayDailyChallengeCleared(bool Completed) {
		if(Completed)
			PlayerPrefs.SetInt ("TodayDailyChallengeCleared", 1);
		else
			PlayerPrefs.SetInt ("TodayDailyChallengeCleared", 0);
	}




	public bool GetShowDailyProgressBar() {
		if (PlayerPrefs.GetInt ("ShowDailyProgressBar", 0) == 1)
			return true;
		return false;	
	}

	public void SetShowDailyProgressBar(bool Show) {
		if(Show)
			PlayerPrefs.SetInt ("ShowDailyProgressBar", 1);
		else
			PlayerPrefs.SetInt ("ShowDailyProgressBar", 0);
	}





	public string GetDailyChallengeName(int Order) {
		string[] str;

		if (Order == 7) {
			str = ChallengesTypesData.DailyChallengesTypesNames[GetDailyCurrentChallengeType()].Split (':');
			if(ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()] > 1 ) {
				return str[0] + " " + ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()] + " " + "Gems";	
			}
			else {
			return str [0] + " " + ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()] + " " + str [1];	
			}
		}

		if (Order == 8) {
			str = ChallengesTypesData.DailyChallengesTypesNames[GetDailyCurrentChallengeType()].Split (':');
			if(ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()] > 1 ) {
				return str [0] + " " + ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()] + " " + "Gems in one run";	
			}
			else {
				return str [0] + " " + ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()] + " " + str [1];	
			}
		}


		str = ChallengesTypesData.DailyChallengesTypesNames[GetDailyCurrentChallengeType()].Split (':');
		return str [0] + " " + ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()] + " " + str [1];
	}

	public int GetDailyChallengeValue(int Order) {
		return  ChallengesTypesData.DailyChallengesTypesValues [GetDailyCurrentChallengeType ()];
	}



	public bool IsDailyChallengeDayPassed() {
		DateTime t1 = DateTime.Parse (GetDailyChallengeLastDate());
		TimeSpan  t = System.DateTime.Today.Subtract (t1);
		
		if (t.Days == 1) {
			return true;
		}
		
		return false;
	}

	public bool IsFirstDailyChallenge() {
		if(GetDailyCurrentChallengeOrder() == 0) {
			return true;
		}
		return false;
	}



	public void AllChallengesCompleted() {
		int[] TempValuesIncrement = new int[ChallengesTypesData.TotalDailyChallenges];

		for (int i=0; i<ChallengesTypesData.TotalDailyChallenges; i++) {
			TempValuesIncrement [i] = NewDailyChallengeValue(i); 
				ChallengesTypesData.DailyChallengesTypesValues [i] = TempValuesIncrement [i];
				PlayerPrefs.SetInt (string.Format ("DailyCompletedChallengesTypes{0}", i), -1);
				SetDailyChallengesValuesIncremented(i, TempValuesIncrement[i]);
		}

		PlayerPrefs.SetInt ("AllChallengesCompleted", PlayerPrefs.GetInt ("AllChallengesCompleted", 0) + 1);
	}

	int NewDailyChallengeValue(int Order) {
		if(  ( ChallengesTypesData.DailyChallengesTypesValues [Order]*2 ) > ChallengesTypesData.DailyChallengesTypesValuesMax [Order])
			return ChallengesTypesData.DailyChallengesTypesValuesMax [Order];
		else
			return ChallengesTypesData.DailyChallengesTypesValues [Order]*2 ;
	}

	int GetDailyChallengesValuesIncremented(int Order) {
		return PlayerPrefs.GetInt (string.Format ("DailyChallengesValuesIncremented{0}", Order), ChallengesTypesData.DailyChallengesTypesValues[Order]);
	}

	void SetDailyChallengesValuesIncremented(int Order, int Value) {
		PlayerPrefs.SetInt (string.Format ("DailyChallengesValuesIncremented{0}", Order), Value);
	}


	public void SetTodayVisited(int Value) {
		PlayerPrefs.SetInt ("SetTodayVisited", Value);
	}

	public bool GetTodayVisited() {
		if(PlayerPrefs.GetInt ("SetTodayVisited",0) == 0 ){
			return false;
		}
		return true;
	}





	public void ResetDailyChallenges() {
		//Debug.Log ("ResetDailyChallenges");
		for (int i=0; i<GetDailyCurrentChallengeOrder() ; i++) {
			PlayerPrefs.SetInt (string.Format("DailyChallengeCompleted{0}", i), 0);
			PlayerPrefs.SetInt (string.Format("DailyCompletedChallengesTypes{0}", i),-1);
		}
		SetDailyCurrentChallengeOrder (0);
		SetDailyCurrentChallengeType (-1);
		SetDailyChallengeSavedValue (0);
		SetTodayVisited (0);
		SetIsTodayDailyChallengeCleared (false);
		SetShowDailyProgressBar (false);
		PlayerPrefs.SetInt ("CoinsRandomValue", 0);
		if (PlayerPrefs.GetInt ("AllChallengesCompleted", 0) > 0) {
			Debug.Log ("AllChallengesCompleted");
			for (int i=0; i<ChallengesTypesData.TotalDailyChallenges; i++) {
				ChallengesTypesData.DailyChallengesTypesValues [i] = ChallengesTypesData.DailyChallengesTypesValuesMin[i];
				SetDailyChallengesValuesIncremented(i,0);
			}
		}
		PlayerPrefs.SetInt ("AllChallengesCompleted", 0);
	}

}
