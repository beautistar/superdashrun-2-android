﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class SettingManager : MonoBehaviour {
	public static SettingManager Instance;
	public SettingBtnProperties[] Items;
	public SettingSoundProperties Music;
	public SettingSoundProperties Sound;
	public tk2dUIScrollbar SliderMusic;
	public tk2dUIScrollbar SliderSound;
	bool FirstTime = false;
	DataManager dataManager;
	SettingStaticData settingStaticData;
	bool FacebookURL = false;
	bool TwitterURL = false;
	bool IsLogin = true;

	void Awake(){
		if (Instance == null || Instance != this)
			Instance = this;
	}

	void OnApplicationPause(bool pauseStatus){
		if (pauseStatus == false && FacebookURL) {
			FacebookURL = false;
		}

		if (pauseStatus == false && TwitterURL) {
			TwitterURL = false;
		}
	}

	void OnEnable(){
		if(FirstTime)
			Init ();

		SliderMusic.OnScroll += HandleOnScroll;
		SliderSound.OnScroll += HandleOnScroll1;
		FBIntegrate.OnFBLoggedIn += HandleOnFBLoggedIn;
		FBIntegrate.OnFBLoginFailed += HandleOnFBLoginFailed;
		FBIntegrate.OnGetAppScore += HandleOnGetAppScore;
		FBIntegrate.OnProfilePicsCall += HandleOnProfilePicsCall;
		FBIntegrate.OnTimeExceed += HandleOnTimeExceed;

		SetTwitterBtn ();
	}

	void OnDisable(){
		SliderMusic.OnScroll -= HandleOnScroll;
		SliderSound.OnScroll -= HandleOnScroll1;
		FBIntegrate.OnFBLoggedIn -= HandleOnFBLoggedIn;
		FBIntegrate.OnFBLoginFailed -= HandleOnFBLoginFailed;
		FBIntegrate.OnGetAppScore -= HandleOnGetAppScore;
		FBIntegrate.OnProfilePicsCall -= HandleOnProfilePicsCall;
		FBIntegrate.OnTimeExceed -= HandleOnTimeExceed;
	}

	void HandleOnScroll (tk2dUIScrollbar obj){
		OnItemClick ((int) SettingOrder.MusicSlider,obj.Value);
	}

	void HandleOnScroll1 (tk2dUIScrollbar obj){
		OnItemClick ((int) SettingOrder.SoundSlider,obj.Value);
	}

	// Use this for initialization
	void Init () {
		settingStaticData = SettingStaticData.Instance;
		SetButtonsItems ();
		SetSoundItems ();
		SetSliderItems ();
	}

	void Start() {
		Init ();
		FirstTime = true;
	}

	public void OnItemClick(int Order) {
		switch(Order) {
		case 0 : settingStaticData.setTutorial (); break;
		case 1 : Pushup.Instance.SetPush (); break;
		case 2 : settingStaticData.setMusicOnOff (); break;
		case 3 : settingStaticData.setSoundOnOff (); break;
		default : settingStaticData.setItems (Order); break;
		}	 
	}

	void OnItemClick(int Order, float Value) {
		switch(Order) {
		case 4 : settingStaticData.setMusicValue (Value); break;
		case 5 : settingStaticData.setSoundValue (Value); break;
		}
	}

	public void OnApptentiveClick()
	{
		//Apptentive.ShowFeedback ();
		Application.OpenURL("http://zidapps.com/contact-page/");
	}

	public void OnFacebookClick(){
		if (!FB.IsLoggedIn) {
			UIGameManager.Instance.ShowWall ("Connecting...");
			FacebookURL = true;
			FBIntegrate.Instance.Login ();	
		}
		else {
			IsLogin = false;
			FB.LogOut ();
			SetFacebookBtn();
			IsLogin = true;
		}
	}

	void HandleOnFBLoggedIn (){
		FacebookURL = false;
	}

	void HandleOnGetAppScore()	{
		UIGameManager.Instance.ShowWall ("Getting Data...",25);
	}

	void HandleOnProfilePicsCall(){
		SetFacebookBtn ();
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Connected to Facebook");
	}

	void HandleOnTimeExceed (){
		HandleOnFBLoginFailed ("Error");
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Time Exeeds");
	}

	void HandleOnFBLoginFailed (string error) {
		IsLogin = false;
		FB.LogOut ();
		IsLogin = true;
		Debug.Log ("HandleOnFBLoginFailed: "+error);
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
		FacebookURL = false;
	}

	public void OnTwitterClick()
	{
		if (!TwitterManage.IsLogined) {
			UIGameManager.Instance.ShowWall ("Connecting...");
			TwitterManage.Instance.Login ();
		}
		else {
			TwitterManage.Instance.Logout ();
		}
		SetTwitterBtn();
	}

	void SetFacebookBtn(){
		if (IsLogin && FB.IsLoggedIn) {
			Items [0].Title.text = "LOGOUT FACEBOOK";
			Items [0].Description.text = "DISCONNECT FACEBOOK ACCOUNT";	
			Items [0].sprite.SetSprite ("fbicon");
		}
		else {
			Items [0].Title.text = settingStaticData.getItemsTitle (0).ToString ();
			Items [0].Description.text = settingStaticData.getItemsDescription (0).ToString ();
			Items [0].sprite.SetSprite ("fblogin");
		}
	}

	void SetTwitterBtn()
	{
		if (TwitterManage.IsLogined) {
			Items [1].Title.text = "LOGOUT TWITTER";
			Items [1].Description.text = "DISCONNECT TWITTER ACCOUNT";	
			Items [1].sprite.SetSprite ("tweetcion");
		}
		else {
			Items [1].Title.text = settingStaticData.getItemsTitle (1).ToString ();
			Items [1].Description.text = settingStaticData.getItemsDescription (1).ToString ();
			Items [1].sprite.SetSprite ("twiterlogin");
		}
	}

	void SetButtonsItems() {

		SetFacebookBtn ();
		SetTwitterBtn ();

		for (int i =2; i< settingStaticData.ItemsCount; i++) {
			Items [i].Title.text = settingStaticData.getItemsTitle (i).ToString ();
			Items [i].Description.text = settingStaticData.getItemsDescription (i).ToString ();
		}


		Items [2].OrderInList = (int) SettingOrder.Tutorials;
		Items [2].OnOff.OrderInList = (int) SettingOrder.Tutorials;
		if (settingStaticData.getTutorial ())
			Items [2].OnOff.OnOn ();
		else
			Items [2].OnOff.OnOff ();


		Items [3].OrderInList = (int) SettingOrder.Notification;
		Items [3].OnOff.OrderInList = (int) SettingOrder.Notification;
		if (settingStaticData.getNotification ())
			Items [3].OnOff.OnOn ();
		else
			Items [3].OnOff.OnOff ();
	}

	void SetSoundItems(){
		Music.OnText = "Music On";
		Music.OffText = "Music Off";
		Music.OrderInList = (int) SettingOrder.Music;
		Sound.OnText = "Sound On";
		Sound.OffText = "Sound Off";
		Sound.OrderInList = (int) SettingOrder.Sound;

		if (settingStaticData.getItems ( Music.OrderInList ) == 0) {
			Music.OnOff ();
		} 
		else {
			Music.OnOn ();	
		}
		if (settingStaticData.getItems ( Sound.OrderInList ) == 0) {
			Sound.OnOff ();
		} 
		else {
			Sound.OnOn ();		
		}
	}

	void SetSliderItems() {
		SliderMusic.SetScrollPercentWithoutEvent ( settingStaticData.getItems ( (int) SettingOrder.MusicSlider ));
		SliderSound.SetScrollPercentWithoutEvent ( settingStaticData.getItems ( (int) SettingOrder.SoundSlider ));
	}

}
