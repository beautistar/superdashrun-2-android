﻿using UnityEngine;
using System.Collections;
using System;

public class TutorialManager : MonoBehaviour {
	public GameObject Arrow;
	public static TutorialManager Instance;
	public bool IsStopAnimation = true; 
	string CurrentAnimation = "";
	// Use this for initialization
	void Awake () {
		Instance = this;
		Arrow.SetActive (false);
	}

	void Update()
	{
		if (IsStopAnimation)
			return;

		if (!GetComponent<Animation>().isPlaying)
			GetComponent<Animation>().Play (CurrentAnimation);
	}
	
	public void ShowUp() {
		//Debug.LogError("Show up");
		CurrentAnimation = "Tutorial";
		IsStopAnimation = false;
		Arrow.SetActive (true);
		GetComponent<Animation>().Play ();
	}

	public void ShowDown(){
		Debug.LogError("Show down");
		IsStopAnimation = false;
		CurrentAnimation = "Down";
		Arrow.SetActive (true);
		GetComponent<Animation>().Play ("Down");
	}

	public void ShowLeft(){
		Debug.LogError("Show left");
		IsStopAnimation = false;
		CurrentAnimation = "Left";
		Arrow.SetActive (true);
		GetComponent<Animation>().Play ("Left");
	}

	public void ShowRight(){
		Debug.LogError("Show right");
		IsStopAnimation = false;
		CurrentAnimation = "Right";
		Arrow.SetActive (true);
		GetComponent<Animation>().Play ("Right");
	}

	public void StopAnimation(){
		//Debug.LogError("Anim stop");
//		IsStopAnimation = true;
//		if(GetComponent<Animation>().isPlaying) {
//			GetComponent<Animation>().Stop ();
//		}
//			
//		Arrow.SetActive (false);

		StartCoroutine(stopAnim());

	}

	IEnumerator stopAnim() {
		yield return new WaitForSeconds(0.3f);


		IsStopAnimation = true;
		if(GetComponent<Animation>().isPlaying) {
			GetComponent<Animation>().Stop ();
		}
		
		Arrow.SetActive (false);
	}

}
