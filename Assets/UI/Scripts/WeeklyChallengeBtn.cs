﻿using UnityEngine;
using System.Collections;

public class WeeklyChallengeBtn : MonoBehaviour {
	public tk2dTextMesh HeaderText;
	public tk2dTextMesh FooterText;
	public tk2dUIProgressBar progressBar;
	public tk2dTextMesh ProgressText;
	public tk2dSprite Symbol;
	public tk2dSprite CoinSymbol;
	public tk2dTextMesh Coins;
	public int Order = 0;
	public int ChallengeOrder = 0;
	public bool IsCompleted;
	public bool IsDiamond;
	public bool IsCoins;
	public GameObject Tick;
	public WeeklyChallengeStaticData weeklyStaticData;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetDetails(int order) {
		weeklyStaticData = WeeklyChallengeStaticData.Instance;
		this.Order = order;
		HeaderText.text = weeklyStaticData.GetWeeklyChallName (Order).Split (' ')[0];
		FooterText.text = weeklyStaticData.GetWeeklyChallValue (Order).ToString () +" " + weeklyStaticData.GetWeeklyChallName (Order).Split (' ')[1];
		Symbol.SetSprite (weeklyStaticData.GetWeeklyChallSymbolName(Order));
		Coins.text = weeklyStaticData.GetWeeklyChallCoins (order).ToString ();
		IsDiamond = weeklyStaticData.GetIsWeeklyChallDiamond (Order);
		IsCoins = weeklyStaticData.GetIsWeeklyChallCoins (Order);
		ProgressText.text = weeklyStaticData.GetCurrentActiveWeeklyChallSavedValue (order).ToString ();
		if(IsDiamond)
			CoinSymbol.SetSprite (weeklyStaticData.GetCoinsSymbolName (false));
		if(IsCoins)
			CoinSymbol.SetSprite (weeklyStaticData.GetCoinsSymbolName (true));
		float savedVal = weeklyStaticData.GetCurrentActiveWeeklyChallSavedValue (order);
		float ChallengeVal = weeklyStaticData.GetWeeklyChallValue (order);
		float Val =  (savedVal/ChallengeVal);
		StartCoroutine (Delay (Val));
		if (weeklyStaticData.GetWeeklyChallCompleted (Order)) 
			ShowTick ();
		else
			ResetBtn();
		
	}

	IEnumerator Delay(float Val) {
		yield return new WaitForEndOfFrame ();
		progressBar.Value = float.Parse ( Val.ToString ());
	}
	
	public void ShowTick() {
		Tick.SetActive (true);
		IsCompleted = true;
	}
	
	public void ResetBtn() {
		Tick.SetActive (false);
		IsCompleted = false;
	}
}
