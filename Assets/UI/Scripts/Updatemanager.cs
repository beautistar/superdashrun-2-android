﻿using UnityEngine;
using System.Collections;

public class Updatemanager : MonoBehaviour {

	public static Updatemanager instance;

	void Awake()
	{
		instance = this;	
	}

	public void OnUpdateCoins()
	{
		if(ScoreBarManager.instance != null)
        {
            // update coins and diamongs
            ScoreBarManager.instance.OnUpdate();
        }
			
		//if(ProfileManager.instance.enabled )
		//	ProfileManager.instance.OnUpdate ();
	}

	public void OnUpdateStore(StoreMenus storeMenu)
	{
        // update label for coins AND diamonds
		OnUpdateCoins ();

		if (StoreManager.Instance != null) {		
			switch (storeMenu) {
			case StoreMenus.Character:	//StoreManager.Instance.SetCharacter (); 
				StoreManager.Instance.OnSuccessFullBuy (); break;
			case StoreMenus.PermanentPurchases: //StoreManager.Instance.SetPermanentPurchases ();	
				StoreManager.Instance.OnSuccessFullBuy (); break;			
			case StoreMenus.Upgrade: //StoreManager.Instance.SetUpgrade (); 
				StoreManager.Instance.OnSuccessFullBuyUpgrade (); break; 
			case StoreMenus.SingleUse: //StoreManager.Instance.SetSingleUse (); 
				StoreManager.Instance.OnSuccessFullBuySingleUse (); break;		
			}
		}

		if(ScrollbarCustom.instance != null)
		{
			ScrollbarCustom.instance.SetPurchased (DataManager.instance.getSelectedCharacter ());
		}
	}

	public void OnUpdateBank( )
	{
		OnUpdateCoins ();
	}

	public void OnUpdateEarn( )
	{
		OnUpdateCoins ();
	}


}
