﻿using UnityEngine;
using System.Collections.Generic;

public class StoreMenuScript : MonoBehaviour {

	public Btn4Properties[] InstanceOfChilds;
	public StoreManager InstanceStoreManager;
	public Btn4Properties CurrentActiveBtn;
	public int OrderInList = 0;
	public int Childs = 0;
	public GameObject TitleSeperator;

	GameObject[] TempInstanceOfChilds;

	// Use this for initialization
	void Awake () {

	}

	public void ActiveCalled(int OrderInList)	{
		if (TempInstanceOfChilds == null) {		
			TempInstanceOfChilds = new GameObject[InstanceOfChilds.Length];
			for (int i=0; i<InstanceOfChilds.Length; i++)
				TempInstanceOfChilds [i] = InstanceOfChilds [i].gameObject;
		}

		if(InstanceStoreManager.IsActive){
			InstanceStoreManager.CurrentActiveStoreMenu.CurrentActiveBtn.OnStretchDown ();
			InstanceStoreManager.IsActive = false;
		}

		StaticFunction.CalledFunct (OrderInList, true,  TempInstanceOfChilds );	

		CurrentActiveBtn = InstanceOfChilds [OrderInList];
		InstanceStoreManager.ActiveCalled (this.OrderInList);
	}

	public void NormalCalled(int OrderInList) {
		StaticFunction.CalledFunct (OrderInList, false, TempInstanceOfChilds );
		InstanceStoreManager.NormalCalled (this.OrderInList);
	}

	public void SetDetails(int Count, GameObject gameObject, float y, int Order) {
		float Gap = y;
		Gap -= 1;
		this.OrderInList = Order;
		if (InstanceOfChilds.Length == 0) {
			TitleSeperator.transform.localPosition = new Vector3 (0,y,-0.3f);
			if(Order == 3 || Order == 2) {
				InstanceOfChilds = new Btn4Properties[Count- 1 ]; 
			}
			else
				InstanceOfChilds = new Btn4Properties[Count];
			InstanceStoreManager = StoreManager.Instance;
		}
		
		if(Order == 3 || Order == 2) {
			Count--;
		}
		
		for(int i=0;i<Count;i++) {
			if(InstanceOfChilds[i] == null ) {
				InstanceOfChilds[i] = ((GameObject)Instantiate (gameObject)).GetComponent<Btn4Properties>();
				InstanceOfChilds [i].gameObject.transform.parent = this.transform;
				InstanceOfChilds[i].transform.localPosition = new Vector3(0,Gap,0);
				InstanceOfChilds[i].SetInstanceStoreMenu(this);
				Gap -= 2;  
			}
			switch(gameObject.name) {
			case "CharacterBtn" : 	InstanceOfChilds[i].SetDetailsCharacter (i);break;
			case "PermanentBtn" :   InstanceOfChilds[i].SetDetailsPermanent (i);break;
			case "UpgradeBtn" : 	InstanceOfChilds[i].SetDetailsUpgrade (i); break;
			case "SingleUseBtn" :	 InstanceOfChilds[i].SetDetailsSingleUse (i);  break;
			}
			
		}
	}

}
