﻿using UnityEngine;
using System.Collections;

public class ChallengeMenuManager : MonoBehaviour {
	public static ChallengeMenuManager Instance;
	public GameObject DailyChallenges;
	public GameObject WeeklyChallenges;

	// Use this for initialization

	void Awake () {
		Instance = this;
	}

	void OnEnable() {
		ShowDailyChallenges ();
	}

	public void ShowDailyChallenges() {
		MainMenuManager.Instance.ScreenText.text = "Daily Challenges";
		DailyChallenges.SetActive (true);
		WeeklyChallenges.SetActive (false);
		//DailyChallengeManager.Instance.SetDailyChallenge ();
	}

	public void ShowWeeklyChallenges() {
		MainMenuManager.Instance.ScreenText.text = "Weekly Challenges";
		DailyChallenges.SetActive (false);
		WeeklyChallenges.SetActive (true);
	}

}
