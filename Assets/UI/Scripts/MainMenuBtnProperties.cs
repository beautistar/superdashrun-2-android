﻿using UnityEngine;
using System.Collections;

public class MainMenuBtnProperties : MonoBehaviour {

	public bool IsNotification = false;
	public string NotificationValue = "0";

	// Use this for initialization
	void Start () {
		BroadcastMessage ("SetNotification", new object[2]{IsNotification,NotificationValue}, SendMessageOptions.DontRequireReceiver);
		//GetComponent<tk2dTextMesh> ().text = NameOfButton;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void LoadScreen() {
		MainMenuManager.LoadScreen (this.name);
	}
	
}
