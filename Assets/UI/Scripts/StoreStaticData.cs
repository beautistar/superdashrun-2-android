﻿using UnityEngine;
using System.Collections;

public class StoreStaticData : MonoBehaviour {
	public static StoreStaticData Instance;
	public string[] CharacterImages;
	public string[] CharacterProfileImages;
	public Color[] CharacterColor;
	public string[] PermanentImages;
	public Color[] PermanentColor;
	public string[] UpgradeImages;
	public Color[] UpgradeColor;
	public string[] SingleUseImages;
	public Color[] SingleUseColor;


	void Awake()
	{
		Instance = this;
	}

	public string getCharacterCoinsImage(int Order)
	{
		return CharacterImages[Order];
	}

	public string getCharacterProfileImage(int Order)
	{
		return CharacterProfileImages[Order];
	}

	public Color getCharacterCoinsColor(int Order)
	{
		return CharacterColor[Order];
	}

	public string getPermanentCoinsImage(int Order)
	{
		return PermanentImages[Order];
	}
	
	public Color getPermanentCoinsColor(int Order)
	{
		return PermanentColor[Order];
	}

	public string getUpgradeCoinsImage(int Order)
	{
		return UpgradeImages[Order];
	}
	
	public Color getUpgradeCoinsColor(int Order)
	{
		return UpgradeColor[Order];
	}

	public string getSingleUseCoinsImage(int Order)
	{
		return SingleUseImages[Order];
	}
	
	public Color getSingleUseCoinsColor(int Order)
	{
		return SingleUseColor[Order];
	}
}
