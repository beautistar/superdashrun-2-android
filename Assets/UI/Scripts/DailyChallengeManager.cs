using UnityEngine;
using System.Collections;
using System;

public class DailyChallengeManager : MonoBehaviour {
	public static DailyChallengeManager Instance;

	public ChallengeBtn[] challengeBtn;
	public GameObject ParentGameObject;
	public Vector2 InitialPos ;
	public float Gap ;
	public tk2dTextMesh HeaderText;
	public tk2dTextMesh TimerText;
	public tk2dTextMesh Footertext;
	public tk2dSprite Icon;
	public tk2dTextMesh IconText;
	public GameObject progressBarObject;
	public tk2dUIProgressBar progressBar;
	bool StartTimer = false;
	bool IsFirstTime = false;
	DailyChallengeStaticData challengeStaticData;
	float IntialHeaderY = 0;

	void Awake() {
		Instance = this;
		IntialHeaderY = HeaderText.gameObject.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if (!StartTimer)
			return;

		CountTimer ();
	}

	void OnEnable() {
		IsFirstTime = false;
		StartTimer = false;
		UpDownHeaderText (false);
		progressBarObject.SetActive (false);
		SetDailyChallenge ();
	}

	public void SetDailyChallenge() {
		challengeStaticData = DailyChallengeStaticData.Instance;
		if (challengeBtn.Length == 0) {
			challengeBtn = new ChallengeBtn[DailyChallengeStaticData.Instance.TotalBtns];
			for (int i =0; i < DailyChallengeStaticData.Instance.TotalBtns; i++) {
				challengeBtn [i] = ((GameObject)Instantiate (DailyChallengeStaticData.Instance.DailyChallengerBtn)).GetComponent<ChallengeBtn> ();
				challengeBtn [i].gameObject.transform.parent = ParentGameObject.transform;
				challengeBtn [i].transform.localPosition = new Vector3 (InitialPos.x + Gap, InitialPos.y, -1);
				InitialPos = new Vector2 (InitialPos.x + Gap, InitialPos.y);
				challengeBtn [i].SetDetails (i, CheckForCompletion (i));
			}

		} else {
			for (int i =0; i < DailyChallengeStaticData.Instance.TotalBtns; i++) {
				challengeBtn [i].SetDetails (i, CheckForCompletion (i));
			}
		}

		Icon.SetSprite (challengeStaticData.GetSymbolName(challengeStaticData.GetDailyCurrentChallengeOrder()));

		if (DailyChallengeStaticData.Instance.GetDailyCurrentChallengeOrder () < DailyChallengeStaticData.Instance.TotalBtns)
			IconText.text = challengeStaticData.GetCoins(challengeStaticData.GetDailyCurrentChallengeOrder()).ToString ();
		else
			IconText.text = "";

		if (challengeStaticData.IsTodayDailyChallengeCleared ()) {
			NextChallenge ();
			return;
		}

		SameDayChallenge ();
	}


	bool CheckForCompletion(int Order) {
		if (Order < challengeStaticData.GetDailyCurrentChallengeOrder ()) {
			return true;
		}
		return false;
	}

	void SameDayChallenge() {
		UpDownHeaderText (true);
		HeaderText.text = challengeStaticData.GetDailyChallengeName(challengeStaticData.GetDailyCurrentChallengeType());
		TimerText.text = "";
		Footertext.text = "";
		if (challengeStaticData.GetShowDailyProgressBar ()) {
			progressBarObject.SetActive (true);
			//Debug.Log ( challengeStaticData.GetDailyChallengeSavedValue () + " " + challengeStaticData.GetDailyChallengeValue ( challengeStaticData.GetDailyCurrentChallengeType()));
			float savedVal = challengeStaticData.GetDailyChallengeSavedValue ();
			float ChallengeVal = challengeStaticData.GetDailyChallengeValue ( challengeStaticData.GetDailyCurrentChallengeType());
			float Val =  savedVal/ChallengeVal;
			//Debug.Log (Val);
			progressBar.Value = Val;
		}
	}

	void UpDownHeaderText(bool Up) {
		if (Up) {
			HeaderText.gameObject.transform.position =  new Vector3 (HeaderText.gameObject.transform.position.x, IntialHeaderY -0.5f,HeaderText.gameObject.transform.position.z);
		} else {
			HeaderText.gameObject.transform.position =  new Vector3 (HeaderText.gameObject.transform.position.x, IntialHeaderY ,HeaderText.gameObject.transform.position.z);
		}
	}

	void NextChallenge() {
		UpDownHeaderText (false);
		HeaderText.text = "Next Challenge In";
		Footertext.text = "Come Back And Win";
		StartTimer = true;
		Icon.SetSprite (challengeStaticData.GetSymbolName(challengeStaticData.GetDailyCurrentChallengeOrder()));

		if (DailyChallengeStaticData.Instance.GetDailyCurrentChallengeOrder () < DailyChallengeStaticData.Instance.TotalBtns)
			IconText.text = challengeStaticData.GetCoins(challengeStaticData.GetDailyCurrentChallengeOrder()).ToString ();
		else
			IconText.text = "";
	}
	
	void CountTimer() {
		DateTime now = DateTime.Now.Date;
		DateTime TodayDate = new DateTime(now.Year, now.Month, now.Day  , DateTime.Now.TimeOfDay.Hours, DateTime.Now.TimeOfDay.Minutes, DateTime.Now.TimeOfDay.Seconds);
		DateTime NextDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1 , 00,00, 00);  
		//Debug.Log (TodayDate);
		//Debug.Log (NextDate);

		TimeSpan timeStamp = NextDate.Subtract (TodayDate);
		TimerText.text =  string.Format("{0:00} : {1:00} : {2:00}",timeStamp.Hours,timeStamp.Minutes, timeStamp.Seconds);
		if (timeStamp.Days > 0) {
			OnTimeUp();
		}
		//Debug.Log (timeStamp.Hours + " " + timeStamp.Minutes + " " + timeStamp.Seconds);
	}

	void OnTimeUp() {
		StartTimer = false;
		ChallengerManager.Instance.IntializeDailyChallenge ();
		SetDailyChallenge();
	}
}
