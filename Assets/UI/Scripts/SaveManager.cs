﻿using UnityEngine;
using System.Collections;

public class SaveManager : MonoBehaviour {
	public static SaveManager Instance;
	public tk2dTextMesh Scoretext;
	public tk2dTextMesh Timertext;
	public tk2dTextMesh DiamondCount;
	public tk2dTextMesh HowMuchDiamond;
	public GameObject NotEnoughDiamond;
	public GameObject SaveMe;
    public GameObject VideoNotAvailableDialog;
    public GameObject SaveMeBtn;
    public GameObject WatchVideoBtn;
    public string HowMuchDiamondString;
	int CurrentSaveMeScore = 1;
	GameOverType gameoverType;
	bool IsURL = false;
	DataManager dataManager ;
	BankStaticData bankStaticData;
	bool StartTimer = false;
	float timer = 4f;
	bool IsBankFromNoEnoghDiamond = false;
	// Use this for initialization
	void Awake(){
		Instance = this;

        //Debug.Log("Save manager name: " + name + " parent " + transform.parent.name);
	}

    void OnDestroy()
    {
        
    }

	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus == true && IsURL) {
			UIGameManager.Instance.HideWall ();
			IsURL = false;
		}
	}

	void Update() {

		if (StartTimer) {
			//Debug.Log("Time: " + timer);
		}

		if (StartTimer && timer < 0) {
			OnCancelClick ();
			StartTimer = false;
			timer = 0;
            //Debug.LogWarning("===>>>TIMER OVER!!!");
	
		} else if (StartTimer) {
			//Timertext.text = ( (int)timer).ToString ();
			timer -= Time.deltaTime;
            //Debug.LogWarning("===>>>TIMER LEFT: " + timer);
        }

        
	}

	void Start() {
		dataManager = DataManager.instance;
		bankStaticData = BankStaticData.Instance;
	}

	public void OnGameOver(GameOverType gameoverType) {
		StartTimer = true;
		timer = 5;
		//StopCoroutine ("IngamePowerProgress.Instance.Loader");
		this.gameoverType = gameoverType;
		SaveMe.SetActive (true);
        //Debug.LogWarning("===>> saveme pos: " + SaveMeBtn.transform.position + ", local: " + SaveMeBtn.transform.localPosition);
		DiamondCount.text = CurrentSaveMeScore.ToString () ;
		if (CurrentSaveMeScore == 1) {
			HowMuchDiamond.text = HowMuchDiamondString.Replace ("DIAMONDS", "DIAMOND");	
		} else {
			HowMuchDiamond.text = HowMuchDiamondString;		
		}
		MyGUIManager.instance.pause.SetActive (false);
		Scoretext.text = (DataManager.instance.getTotalLifeSavers ()).ToString ();

        Vector3 middlePos = new Vector3(0, -1.29f, -0.2f);
        Vector3 sidePos = new Vector3(1.23f, -1.29f, -0.2f);

		SaveMeBtn.transform.localPosition = sidePos;
		WatchVideoBtn.SetActive(true);
	}
	
	public void OnSavemeClick(){

		if ((DataManager.instance.getTotalLifeSavers ()) >= CurrentSaveMeScore) {
			RestartGame();
		}
		else {
            //Debug.Log("==Area730Save: Total: " + DataManager.instance.getTotalLifeSavers() + ", Current: " + CurrentSaveMeScore);
			NotEnoughDiamond.SetActive (true);
			SaveMe.SetActive (false);
			StartTimer = false;
		}
	}

    public void onVideoNotAvailableBack()
    {
        VideoNotAvailableDialog.SetActive(false);
        SaveMe.SetActive(true);
        StartTimer = true;
        timer = 5 - timer;
    }

    public void OnSavemeVideoClick()
    {
		StartTimer = false;

		if (!GoogleMobileAd.IsRewardedVideoReady)
        {
            SaveMe.SetActive(false);
            StartTimer = false;
            VideoNotAvailableDialog.SetActive(true);
			AdsControl.instance.ShowVideo();
        }else{
			AdsControl.instance.ShowVideo();
			RestartAfterVideo ();
        }
    }

    void RestartAfterVideo()
    {
		Debug.Log("SaveManager: Restart after video");
        PlayerController.instance.AfterLifeSaverAsking();
        PowerUpManager.instance.IsActiveFromRestart1();
        PowerUpManager.instance.activatePowerUp(PowerUpTypes.Invincibility);
        MyGUIManager.instance.inGameScreen.SetActive(true);
        SaveMe.SetActive(false);
        //dataManager.setTotalLifeSavers(-1 * CurrentSaveMeScore);
        GameManager.instance.pauseGame(false);
        InGameUIManager.Instance.StartCoutingScore();
        MyGUIManager.GameStarted = true;
        InGameAlertsManager.Instance.Init();
        //MyGUIManager.instance.pause.SetActive (true);
        StartTimer = false;
        //CurrentSaveMeScore *= 2;
        dataManager.SetSaveMeUsedInGame(1);
    }

    void RestartGame() {
		Debug.Log("SaveManager: Restart game");
		PlayerController.instance.AfterLifeSaverAsking ();
		PowerUpManager.instance.IsActiveFromRestart1 ();
		PowerUpManager.instance.activatePowerUp(PowerUpTypes.Invincibility);
		MyGUIManager.instance.inGameScreen.SetActive (true);
		SaveMe.SetActive (false);
		dataManager.setTotalLifeSavers (-1*CurrentSaveMeScore);
		GameManager.instance.pauseGame(false);
		InGameUIManager.Instance.StartCoutingScore();
		MyGUIManager.GameStarted = true;
		InGameAlertsManager.Instance.Init ();
		//MyGUIManager.instance.pause.SetActive (true);
		StartTimer = false;
		CurrentSaveMeScore *= 2;
		dataManager.SetSaveMeUsedInGame (1);
	}
	
	public void OnBuyClick(){
		IsURL = true;
		IsBankFromNoEnoghDiamond = true;
//		OpenIAB.purchaseProduct (BankIntializer.Instance.SKU_spareme);
		BankManager.Instance.CallPurchaseLifesaver(0);
	}

	public void OnCancelClick() {
		CurrentSaveMeScore = 1;
		StartTimer = false;
		SaveMe.SetActive (false);
		NotEnoughDiamond.SetActive (false);
		GameManager.instance.ShowSecrecyBox (gameoverType);

    }

	public void SuccesfulPurchaseBankLifeSavers(int Order) {
		dataManager = DataManager.instance;
		bankStaticData = BankStaticData.Instance;
		dataManager.setTotalLifeSavers (bankStaticData.LifeSavers [Order]);
		NotEnoughDiamond.SetActive (false);

		RestartGame (); 
	}
	
//	private void queryInventorySucceededEvent(Inventory inventory) {
//		if (!IsBankFromNoEnoghDiamond)
//			return;
//		IsBankFromNoEnoghDiamond = false;
//		Debug.Log("queryInventorySucceededEvent: " + inventory);
//	}
//	private void queryInventoryFailedEvent(string error) {
//		if (!IsBankFromNoEnoghDiamond)
//			return;
//		IsBankFromNoEnoghDiamond = false;
//		Debug.Log("queryInventoryFailedEvent: " + error);
//	}
//	private void purchaseSucceededEvent(Purchase purchase) {
//		if (!IsBankFromNoEnoghDiamond)
//			return;
//		IsBankFromNoEnoghDiamond = false;
//		SuccesfulPurchaseBankLifeSavers (0);
//		Debug.Log("purchaseSucceededEvent: " + purchase);
//		UIGameManager.Instance.HideWall ();
//	}
//	private void purchaseFailedEvent(string error) {
//		if (!IsBankFromNoEnoghDiamond)
//			return;
//		IsBankFromNoEnoghDiamond = false;
//		Debug.Log("purchaseFailedEvent: " + error);
//		UIGameManager.Instance.HideWall ();
//		UIGameManager.Instance.PopUp ("Purchase Failed \n Try Again");
//		
//	}
//	private void consumePurchaseSucceededEvent(Purchase purchase) {
//		if (!IsBankFromNoEnoghDiamond)
//			return;
//		IsBankFromNoEnoghDiamond = false;
//		Debug.Log("consumePurchaseSucceededEvent: " + purchase);
//		UIGameManager.Instance.HideWall ();
//	}
//	private void consumePurchaseFailedEvent(string error) {
//		if (!IsBankFromNoEnoghDiamond)
//			return;
//		IsBankFromNoEnoghDiamond = false;
//		Debug.Log("consumePurchaseFailedEvent: " + error);
//		UIGameManager.Instance.HideWall ();
//	}
}
