﻿using UnityEngine;
using System.Collections.Generic;

public class WeeklyChallengeManager : MonoBehaviour {
	public static WeeklyChallengeManager Instance;
	
	public WeeklyChallengeBtn[] weeklyChallengeBtn;
	public GameObject ParentGameObject;
	public Vector2 InitialPos ;
	public float Gap ;
	public List<int> ChallengesList;

	WeeklyChallengeStaticData weeklyChallengeStaticData;

	// Use this for initialization
	void OnEnable () {
		SetWeeklyChallenge ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetWeeklyChallenge() {
		weeklyChallengeStaticData = WeeklyChallengeStaticData.Instance;
		ChallengesList = weeklyChallengeStaticData.GetCurrentActiveWeeklyChall ();
		if (weeklyChallengeBtn.Length == 0) {
			weeklyChallengeBtn = new WeeklyChallengeBtn[weeklyChallengeStaticData.ToShowBtns];

			for (int i =0; i < weeklyChallengeStaticData.ToShowBtns; i++) {
				weeklyChallengeBtn [i] = ((GameObject)Instantiate (weeklyChallengeStaticData.WeeklyChallengeBtn)).GetComponent<WeeklyChallengeBtn> ();
				weeklyChallengeBtn [i].gameObject.transform.parent = ParentGameObject.transform;
				weeklyChallengeBtn [i].transform.localPosition = new Vector3 (InitialPos.x, InitialPos.y + Gap, -1);
				InitialPos = new Vector2 (InitialPos.x , InitialPos.y + Gap);
				weeklyChallengeBtn [i].SetDetails (i);
			}
			
		} else {
			for (int i =0; i < weeklyChallengeStaticData.ToShowBtns; i++) {
				weeklyChallengeBtn [i].SetDetails (i);
			}
		}
		if (weeklyChallengeStaticData.GetIsWeekCompleted ()) {
			ChallengerManager.Instance.CreateNewWeeklyChallenge();
			SetWeeklyChallenge();
		}
	}
}
