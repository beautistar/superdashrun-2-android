﻿using UnityEngine;
using System.Collections;

public class ButtonSlideScript : MonoBehaviour {
	float DownTime = 0.0f;
	public float UpTime = 0.3f;
	public Btn2Properties InstanceBtn2Prop;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ButtonClickedDown(float OffSetY) // -0.07 
	{
		iTween.MoveBy(this.gameObject, iTween.Hash("y", -OffSetY,"time",DownTime, "easeType", "easeInOutExpo", "loopType", "none", "delay", 0));
	}

	public void ButtonClickedUp(float OffSetY) // -0.07
	{
		iTween.MoveBy(this.gameObject, iTween.Hash("y", OffSetY,"time",DownTime, "easeType", "easeInOutExpo", "loopType", "none", "delay", 0));
	}
	
	public void ButtonClickedUp(float OffSetY,bool AsaButton) // -0.07
	{
		iTween.MoveBy(this.gameObject, iTween.Hash("y", OffSetY,"time",DownTime,"oncomplete","oncomplete", "easeType", "easeInOutExpo", "loopType", "none", "delay", 0));
	}

	IEnumerator oncomplete()
	{
		yield return new WaitForSeconds(0.2f);
		InstanceBtn2Prop.OnComplete ();
	}
}
