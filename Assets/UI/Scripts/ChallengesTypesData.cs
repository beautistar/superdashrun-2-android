using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChallengesTypesData  {

	#region Daily Challenges
	public static int TotalDailyChallenges = 12;
	public enum DailyChallengesTypes {  Run , RunOneRun , Score , ScoreOneRun , CollectCoins, CollectCoinsOneRun, CollectPowerUps , CollectGems , CollectGemsOneRun , UseSaveMe, UseBlastOff , CollectSecrecyBox };
	public static string[] DailyChallengesTypesNames  = {"Run:m" ,"Run:m in One Run","Score:Points","Score:Points in one Run","Collect:coins","Collect:coins in one run","Collect:powerups","Collect:gem","Collect:gem in one run","Use:Save me's","Use:Blast off's","Collect:secrecy boxes"  };
	public static int[] DailyChallengesTypesValues    = {  5000 ,       1000 , 			5000,			2000,						1000,				500,					10,					1,				1,						2,			 	2,					2 				};
	public static int[] DailyChallengesTypesValuesMin = {  5000 ,       1000 , 			5000,			2000,						1000,				500,					10,					1,				1,						2,			 	2,					2 				};
	public static int[] DailyChallengesTypesValuesMax = {  25000 ,      10000 , 		50000,			20000,						10000,				5000,					50,					3,				1,						20,			 	10,					20 				};

	#endregion


	#region Weekly Challenges
	public static int TotalWeeklyChallenges = 5;
	public enum WeeklyChallengesTypes {  CollectCoins , CollectGems , CollectPowerUps , Run , Score };
	public static string[] WeeklyChallengesTypesNames =  {"Collect:Coins" ,"Collect:Gems","Collect:Powerups","Run:m","Score:Points"};
	public static int[] WeeklyChallengesTypesValues =    {    10000,       	 5 , 				20,			100000,		500000     };
	public static int[] WeeklyChallengesTypesValuesMin = {    10000,       	 5 , 				10,			100000,		100000     };
	public static int[] WeeklyChallengesTypesValuesMax = {    50000,       	 10 , 				100,		1000000,	5000000    };
	#endregion

}