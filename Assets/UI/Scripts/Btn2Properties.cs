﻿using UnityEngine;
using System.Collections;

public class Btn2Properties : MonoBehaviour {

	public float OffSetTime = 0.05f;
	public GameObject SelfInstance;
	public ButtonSlideScript SlideScript;
	public GameObject Target;
	bool IsDown = false;
	public static Btn2Properties Instance;
	private MyGUIManager myGuiManager;

	void Awake(){
		if (Instance == null || Instance != null)
			Instance = this;
	}
	// Use this for initialization
	void Start () {
		myGuiManager = MyGUIManager.instance;
	}
	// Update is called once per frame
	void Update () {

	}

	public void OnMouseDown() {
		IsDown = true;
		SlideScript.ButtonClickedDown (OffSetTime);
	}

	void OnMouseExit(){
		if(IsDown)
			SlideScript.ButtonClickedUp (OffSetTime);
		IsDown = false;
	}

	public void OnMouseUpAsButton() {
		if(IsDown)
			SlideScript.ButtonClickedUp (OffSetTime,true);
		IsDown = false;
	}


	public void OnComplete() {
	
		switch(SelfInstance.name) {
		case "Exit" : myGuiManager.ShowPlayScreen (); break;
		case "Back" :  MainMenuManager.LoadScreen ("00Menu"); break;
		case "Play" : ToLoadLevel(); break;
		case "Change" : GameManager.instance.showStore(true);  MainMenuManager.LoadScreen ("10Player Selection"); break;
		case "OnOff" : Target.GetComponent<OnOffProperties>().OnClick (); break;
		case "Restart" : ToRestartLevel(); break;
		case "Home" : EndToHomeScreen(); break;
		case "Pause" : GameManager.instance.pauseGame(true); break;
		case "Resume" : GameManager.instance.pauseGame(false); break;
		case "Quit" : InGameUIManager.Instance.gameObject.SetActive (true); InGameUIManager.Instance.StopAllCoroutines (); GameManager.instance.gameQuit(GameOverType.Quit, true); GameManager.instance.CancelingGamePauseFromSecrecyBox(); GameManager.instance.backToMainMenu(true);MainMenuManager.LoadScreen ("00Menu"); break;
		case "PlayerStoreToMenu" : GameManager.instance.showStore(false); /*GameManager.instance.backToMainMenu(false);*/ MainMenuManager.LoadScreen ("00Menu");  break;
		case "PlayerSelectionBtn" :  GameManager.instance.purchaseCharacter(ScrollbarCustom.instance.Count); GameManager.instance.selectCharacter(ScrollbarCustom.instance.Count); break;
		case "SaveMe" :  SaveManager.Instance.OnSavemeClick (); break;
        case "SaveMeVideo": SaveManager.Instance.OnSavemeVideoClick(); break;
        case "BuyDiamond" : SaveManager.Instance.OnBuyClick (); break;
		case "CancelDiamond" : SaveManager.Instance.OnCancelClick (); break;
		case "DailyBtn"  : ChallengeMenuManager.Instance.ShowDailyChallenges(); break;
		case "WeeklyBtn" : ChallengeMenuManager.Instance.ShowWeeklyChallenges(); break;
		case "NextDailyBtn" : ChallegneGameOverManager.Instance.OnClickDailyNext (); break;
		case "NextWeeklyBtn" : ChallegneGameOverManager.Instance.OnClickWeeklyNext (); break;
        case "CancelNoVideo": SaveManager.Instance.onVideoNotAvailableBack(); break;
            default : break;
		}
	}

	public void ToLoadLevel(){

		BgAnimation bgAnim = BgAnimation.instance;
		FatherAnimation fatherAnim = FatherAnimation.instance;
		CharactersAnimation characterAnim = CharactersAnimation.instance;
		CharactersAnimationStay characterAnimStay1 = CharactersAnimationStay.instance;
		bgAnim.OnPlayButton ();
		fatherAnim.OnPlayButton ();
		characterAnimStay1.OnPlayButton ();	
		characterAnim.OnPlayButton ();
		
		StartCoroutine (ToStartGame ());
		
		UIGameManager.Instance.GoogleAnalyticsScreenName ("PlayGame");
	}

	private IEnumerator ToStartGame(){
		yield return new WaitForSeconds(0.8f);
		CharactersAnimation characterAnim = CharactersAnimation.instance;
		characterAnim.SetMyLayOut (false);
		yield return new WaitForSeconds(0.1f);
		GameManager.instance.startGame(false);
		//yield return new WaitForSeconds(0.2f);
		MyGUIManager.instance.SetIntroVideo(false);
	}

	private void ToRestartLevel(){
		InGameUIManager.Instance.gameObject.SetActive (true); 
		InGameUIManager.Instance.StopAllCoroutines ();
		GameManager.instance.restartGame(true);
	}

	private void EndToHomeScreen(){
		GameManager.instance.backToMainMenu(true);
	}
}
