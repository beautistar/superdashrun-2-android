﻿using UnityEngine;
using System.Collections;

public class GooglePlusPopUp : MonoBehaviour {
	public static GooglePlusPopUp Instance;
	public GameObject googlePlus;
	public int TotalDiamonds;
	bool ShowWall = false;

	public void Awake() {
		Instance = this;
	}

	// Use this for initialization
	public void Init () {
		if (!GetGooglePlusPopup ()) {
			ShowGooglePLusPopUp ();
			PopUpManager.Instance.IsPopOn = true;		
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (ShowWall) {
			UIGameManager.Instance.ShowWall ("", true);		
		}
	}

	void ShowGooglePLusPopUp() {
		UIGameManager.Instance.ShowWall ("", true);
		googlePlus.SetActive (true);
		ShowWall = true;
	}

	
	void HideGooglePLusPopUp() {
		UIGameManager.Instance.HideWall ();
		googlePlus.SetActive (false);
		ShowWall = false;
	}

	void OnConnectGooglePlusPopUp() {
		HideGooglePLusPopUp ();
//		GameServicesManager.Instance.showPlusOneDialog ();
		FBIntegrate.Instance.PostStatusOntimeline();
		DataManager.instance.setTotalLifeSavers (1);
	}

	public void SetGooglePlusPopup(int Val) {
		PlayerPrefs.SetInt ("GooglePlusPopup", Val);
	}

	public bool GetGooglePlusPopup() {
		if (PlayerPrefs.GetInt ("GooglePlusPopup", 0) == 0)
			return false;
		else 
			return true;
	}
}
