using UnityEngine;
using System.Collections;

public class SettingSoundProperties : MonoBehaviour {

	public tk2dTextMesh Text;
	public int OrderInList;
	public bool Active  = true;
	public string OnText;
	public string OffText;
	// Use this for initialization
	void Start () {
	
	}

	public void OnMusicClick()
	{
		if (Active) {
			OnOff ();
		} 
		else {
			OnOn ();
		}

		SettingManager.Instance.OnItemClick (OrderInList);
	}

	public void OnSoundClick()
	{
		if (Active) {
			OnOff ();	
		} 
		else {
			OnOn ();
		}
		SettingManager.Instance.OnItemClick (OrderInList);
	}

	public void OnOn(){
		Text.text = OnText;		
		Active = true;
	}
	
	public void OnOff(){
		Text.text = OffText;
		Active = false;
	}

}
