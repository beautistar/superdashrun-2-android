﻿using UnityEngine;
using System.Collections;

public class Btn4Properties : MonoBehaviour {

	public bool IsLevel;
	public int Level = 0;
	public int BuyingCost = 0 ;
	public int OrderInList = 0;
	private string Description;
	public int TotalYouHave = 0;
	public GameObject CornerBtn;
	public string FullDescription = "Testcase1 Testcase1 Testcase1 Testcase1 Testcase1 Testcase1 Testcase1 Testcase1 Testcase1 Testcase1 Testcase1";
	public bool IsYouHave = false;
	public tk2dTextMesh CoinsObject;
	public tk2dTextMesh TitleObject;
	public GameObject InstantGameObject;
	public tk2dTextMesh DescriptionObject;
	public StoreMenuScript InstanceStoreMenu;
	public StoreLevelManager InstanceStoreLevel;
	public StoreDropList InstanceStoreDroplist;
	public tk2dSprite Symbol;
	public tk2dSprite Front;

	GameObject Go;
	int StretchValue ;
	float PixelsPerUnit ;
	bool IsStretched = false;
	Btn4Properties Instance;
	int SingleUseValue = 4;
	tk2dSlicedSprite SelfSprite;
	int DescriptionLength = 15;
	CornerBtnManager Cornermanager;

	
	public void SetInstanceStoreMenu(StoreMenuScript InstanceStoreMenu) {
		this.InstanceStoreMenu = InstanceStoreMenu;
	}

	// Use this for initialization
	void Awake() {
		  Instance = this;
	}

	void SetTextVariables(){
		DescriptionLength = StoreManager.Instance.DescriptionLength;
		SingleUseValue = DataManager.instance.SingleUse;
		
		SelfSprite = GetComponent<tk2dSlicedSprite> ();
		this.StretchValue = InstanceStoreMenu.InstanceStoreManager.StretchValue;
		this.PixelsPerUnit = InstanceStoreMenu.InstanceStoreManager.PixelsPerUnit;

		Cornermanager = CornerBtn.GetComponent<CornerBtnManager> ();
	}

	void up(Vector2 vec) {
		SelfSprite.dimensions = vec;
	}

	public void OnStoreBuyClick(){
		InstanceStoreMenu.InstanceStoreManager.OnStoreBuyClick (InstanceStoreMenu.OrderInList, OrderInList);
	}

	public void OnStretchDown() {
		if (CornerBtn == null)
			return;

		if (!IsStretched) {
			IsStretched = true;
			OnStretchDownFunct (StretchValue);
			if(Go == null) {
				Go = (GameObject) Instantiate (InstantGameObject);
				Go.transform.parent = this.transform;
				Go.transform.localPosition = new Vector3(0,0,-0.1f);
				InstanceStoreDroplist = Go.GetComponent<StoreDropList>();
				InstanceStoreDroplist.TextObject.text = FullDescription;
				InstanceStoreDroplist.SetInstanceParent ();
			}
			Cornermanager.ChangeSprite (true);
			InstanceStoreDroplist.Active ();
			InstanceStoreMenu.ActiveCalled (OrderInList);
		} 
		else {
			Reset ();
		}
	
	}

	public void Reset() {
		IsStretched = false;	
		OnStretchDownFunct (-StretchValue);
		Cornermanager.ChangeSprite (false);
		InstanceStoreMenu.NormalCalled (OrderInList);
		InstanceStoreDroplist.DeActive ();
	}

	void OnStretchDownFunct(int StretchValue){
		//iTween.ValueTo(this.gameObject, iTween.Hash("onupdate","up","time",0.5f,"from", SelfSprite.dimensions,"to",new Vector2 (SelfSprite.dimensions.x, SelfSprite.dimensions.y + (PixelsPerUnit * StretchValue)), "easeType", "easeInOutExpo", "loopType", "none", "delay", 0));
		SelfSprite.dimensions = new Vector2 (SelfSprite.dimensions.x, SelfSprite.dimensions.y + (PixelsPerUnit * StretchValue));
		//float Corner = CornerBtn.transform.position.y - StretchValue;
		//iTween.MoveTo (CornerBtn, iTween.Hash("time",0.5f,"y",Corner, "easeType", "easeInOutExpo", "loopType", "none", "delay", 0));
		CornerBtn.transform.Translate (new Vector3(0,-StretchValue,0));
	}

	public void SetYouHave(int totalYouHave){
		this.TotalYouHave = totalYouHave;
		DescriptionObject.text = "You Have :" + TotalYouHave;
	}

	public void SetDetailsCharacter(int Order){
		SetTextVariables ();
		TitleObject.text = DataManager.instance.getCharacterTitle (Order).ToString ();
		FullDescription = DataManager.instance.getCharacterDescription (Order).ToString ();
		DescriptionObject.text = DataManager.instance.getCharacterDescription (Order).Substring (0,DescriptionLength) + "...";
		CoinsObject.text = DataManager.instance.getCharacterCost (Order) == -1 ? "" : DataManager.instance.getCharacterCost (Order).ToString ();
		OrderInList = Order;
		Symbol.SetSprite (StoreStaticData.Instance.getCharacterCoinsImage (Order));
		Front.color = StoreStaticData.Instance.getCharacterCoinsColor (Order);

	}

	public void SetDetailsPermanent(int Order){
		SetTextVariables ();
		TitleObject.text = DataManager.instance.getPurchaseTitle (Order).ToString ();
		FullDescription = DataManager.instance.getPurchaseDescription (Order).ToString ();
		DescriptionObject.text = DataManager.instance.getPurchaseDescription (Order).Substring (0,DescriptionLength) + "...";
		CoinsObject.text = DataManager.instance.getPurchaseCost (Order) == -1 ? "" : DataManager.instance.getPurchaseCost (Order).ToString ();
		OrderInList = Order;
		Symbol.SetSprite (StoreStaticData.Instance.getPermanentCoinsImage (Order));
		Front.color = StoreStaticData.Instance.getPermanentCoinsColor (Order);

	}

	public void SetDetailsUpgrade(int Order){
		SetTextVariables ();
		TitleObject.text = DataManager.instance.getPowerUpTitle ( (PowerUpTypes) Order + 1 ).ToString ();
		FullDescription = DataManager.instance.getPowerUpDescription ((PowerUpTypes) Order + 1).ToString ();
		CoinsObject.text = DataManager.instance.getPowerUpCost ((PowerUpTypes) Order + 1) == -1 ? "" : DataManager.instance.getPowerUpCost ((PowerUpTypes) Order + 1).ToString ();
		InstanceStoreLevel.SetLevel ((DataManager.instance.getPowerUpLevel ((PowerUpTypes) Order + 1)) - 1); 
		OrderInList = Order;
		Symbol.SetSprite (StoreStaticData.Instance.getUpgradeCoinsImage (Order + 1));
		Front.color = StoreStaticData.Instance.getUpgradeCoinsColor (Order + 1);
	}

	public void SetDetailsSingleUse(int Order){
		SetTextVariables ();
		TitleObject.text = DataManager.instance.getPowerUpTitle ( (PowerUpTypes) Order + SingleUseValue).ToString ();
		FullDescription = DataManager.instance.getPowerUpDescription ((PowerUpTypes) Order + SingleUseValue).ToString ();
		SetYouHave(DataManager.instance.getPowerUpLevel ((PowerUpTypes) Order+SingleUseValue ));
		//InstanceOfChilds[3].InstanceOfChilds[i].DescriptionObject.text = "You Have :" + DataManager.instance.getPowerUpLevel ((PowerUpTypes) i+SingleUseValue );
		CoinsObject.text =  DataManager.instance.getSingleUseCost ((PowerUpTypes) Order + SingleUseValue).ToString ();
		OrderInList = Order;
		Symbol.SetSprite (StoreStaticData.Instance.getSingleUseCoinsImage (Order));
		Front.color = StoreStaticData.Instance.getUpgradeCoinsColor (Order);
	
	}

}
