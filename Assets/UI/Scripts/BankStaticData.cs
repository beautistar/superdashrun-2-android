﻿using UnityEngine;
using System.Collections;

public enum BankItems { NoMoreAds };

public class BankStaticData : MonoBehaviour {

	public int CoinsItemsCount = 9;
	public string[] CoinsTitles;
	public int[] Coins;
	public float[] CoinsAmount;
	public string[] CoinsImages;
	public Color[] CoinsColor;

	public int LifeSaverItemsCount = 1;
	public string[] LifeSaverTitles;
	public int[] LifeSavers;
	public float[] LifeSaverAmount;
	public string[] LifeSaverImages;
	public Color[] LifeSaverColor;

	public static BankStaticData Instance;

	void Awake(){
		Instance = this;
        //setDoubleCoins ();
    }

	public int getCoinsCount(){
		return CoinsItemsCount;
	}

	public string getCoinTitle(int Order){
		return CoinsTitles [Order];
	}

	public int getCoins(int Order){
		return Coins [Order];
	}

	public float getCoinsAmount(int Order){
		return CoinsAmount [Order];
	}

	public int getLifeSaverCount(){
		return LifeSaverItemsCount;
	}

	public string getLifeSaverTitle(int Order){
		return LifeSaverTitles [Order];
	}
	
	public int getLifeSavers(int Order){
		return LifeSavers [Order];
	}
	
	public float getLifeSaverAmount(int Order){
		return LifeSaverAmount [Order];
	}

	public string getCoinsImage(int Order){
		return CoinsImages[Order];
	}

	public Color getCoinsColor(int Order){
		return CoinsColor[Order];
	}

	public string getLifeSaverImage(int Order){
		return LifeSaverImages[Order];
	}
	
	public Color getLifeSaverColor(int Order){
		return LifeSaverColor[Order];
	}

	public void setAds() {
		if (PlayerPrefs.GetInt ("Ads", 0) == 0)
			PlayerPrefs.SetInt ("Ads", 1);
		else
			PlayerPrefs.SetInt ("Ads", 0);

//		AdsControl.instance.ResetRemoveAds ();
	}

	public bool getAds() {
		if (PlayerPrefs.GetInt ("Ads", 0) == 0)
			return true;
		else
			return false;
	}

	public void setDoubleCoins() {
		if (PlayerPrefs.GetInt ("DoubleCoins", 0) == 0)
			PlayerPrefs.SetInt ("DoubleCoins", 1);
		else
			PlayerPrefs.SetInt ("DoubleCoins", 0);
	}
	
	public bool getDoubleCoins() {
		if (PlayerPrefs.GetInt ("DoubleCoins", 0) == 1)
			return true;
		else
			return false;
	}
}
