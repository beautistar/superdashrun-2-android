﻿using UnityEngine;
using System.Collections;

public class StoreDropList : MonoBehaviour {
	public GameObject BuyButton;
	public tk2dTextMesh TextObject;
	Btn4Properties InstanceBtn4Properties;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetInstanceParent()
	{
		InstanceBtn4Properties = transform.parent.GetComponent<Btn4Properties> ();
	}

	void OnStoreBuyClick()
	{
		InstanceBtn4Properties.OnStoreBuyClick ();
	}

	public void Active()
	{
		HideBuy ();

		if(!this.gameObject.activeSelf)
			this.gameObject.SetActive (true);
	}

	public void DeActive()
	{
		this.gameObject.SetActive (false);
	}

	public void HideBuy()
	{
		if(InstanceBtn4Properties.CoinsObject.text.Equals (""))
		{
			BuyButton.SetActive(false);
		}
	}
}
