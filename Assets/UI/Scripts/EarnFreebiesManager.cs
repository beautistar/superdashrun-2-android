﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public  enum EarnFreebiesTypes {Coins, LifeSaver};

public class EarnFreebiesManager : MonoBehaviour {
	public static EarnFreebiesManager Instance;

	public EarnFreebiesBtnProperties[] CoinsItems;
	public EarnFreebiesBtnProperties[] LifeSaverItems;
	public GameObject Earn;
	public GameObject EarnLifeSaver;

	public int InitialY = 3;
	public tk2dUIScrollableArea area;

	int order;
	string currentAppID;
	bool IsURl = false;

	// Use this for initialization
	DataManager dataManager;
	EarnFreebiesStaticData EarnStaticData;
	
	void Awake() {
		if (Instance == null || Instance != this)
			Instance = this;


		area.ContentLength = 10;
	}

	void OnApplicationPause(bool pauseStatus){
		//Debug.Log ("IsURl "+IsURl);
		print ("Order: "+order);
		if (pauseStatus == false && IsURl) {
			IsURl = false;
			switch(order) {
			case 0: 
			case 1: 
				if(CheckForTwitterLogin ()) HandletwitterLogin ();  
				break;
			case 2:
			case 3:
			case 4: UIGameManager.Instance.ShowWall ("Connecting...",15); FBIntegrate.Instance.IsLikedPage (currentAppID); break;
			}
		}
	}
	
	void OnEnable(){
		FBIntegrate.OnLikePageCall += HandleOnLikePageCall;
		FBIntegrate.OnFBLoggedIn += HandleOnFBLoggedIn;
		FBIntegrate.OnPostStatusCall += HandleOnPostStatusCall;
		FBIntegrate.OnFBLoginFailed += HandleOnFBLoginFailed;
		FBIntegrate.OnPostStatusFailedCall += HandleOnPostStatusFailedCall;
		FBIntegrate.OnProfilePicsCall += HandleOnProfilePicsCall;
		FBIntegrate.OnTimeExceed += HandleOnTimeExceed;
		FBIntegrate.OnGetAppScore += HandleOnGetAppScore;
	}

	void OnDisable(){
		FBIntegrate.OnFBLoggedIn -= HandleOnLikePageCall;
		FBIntegrate.OnFBLoggedIn -= HandleOnFBLoggedIn;	
		FBIntegrate.OnPostStatusCall -= HandleOnPostStatusCall;
		FBIntegrate.OnFBLoginFailed -= HandleOnFBLoginFailed;
		FBIntegrate.OnPostStatusFailedCall -= HandleOnPostStatusFailedCall;
		FBIntegrate.OnProfilePicsCall -= HandleOnProfilePicsCall;
		FBIntegrate.OnTimeExceed -= HandleOnTimeExceed;
		FBIntegrate.OnGetAppScore -= HandleOnGetAppScore;
	}
	
	// Use this for initialization
	void Start () {
		EarnStaticData = EarnFreebiesStaticData.Instance;
		dataManager = DataManager.instance;

		CoinsItems = new EarnFreebiesBtnProperties[EarnStaticData.getOffersCoinsCount ()];
		LifeSaverItems = new EarnFreebiesBtnProperties[EarnStaticData.getOffersLifeSaverCount ()];
		SetCoinsItems ();
		SetLifeSaverItems ();
	}


	public void OnEarnClickCoin(int Order) {
		UIGameManager.Instance.ShowWall ("Connecting...",15);
		order = Order;

		switch(Order) {
//		case 0: currentAppID = ScreenNameDevelopmentInnovation; 	IsURl = true;
//			TwitterBinding.twitterFollowIt (currentAppID); 
//			break;
		case 1: if(CheckForTwitterLogin  ()) HandletwitterLogin (); break;
		case 2: if(CheckForTwitterLogin  ()) HandletwitterLogin (); break;
//		case 3:  currentAppID = IDDevelopmentInnovation; if(CheckForFacebookLogin ()) { HandleOnFBLoggedIn(); } break;
//		case 4: currentAppID = IDSuperDashRun;  if(CheckForFacebookLogin ()) { HandleOnFBLoggedIn();  }	  break;
		case 5: if(CheckForFacebookLogin ()) HandleOnFBLoggedIn();  break;
		case 6: if(CheckForFacebookLogin ()) HandleOnFBLoggedIn();  break;
		case 7: if(CheckForTwitterLogin  ()) HandletwitterLogin (); break;
		}
	}

	void PlayVideo() {

	}

	bool CheckForTwitterLogin() {
//		if (!TwitterManage.IsAuthenticated) 
//		{
//			//UIGameManager.Instance.ShowWall ("Connecting Twitter");
//			TwitterManage.Instance.TWConnect();
//			return false;
//		}
			
		return true;
	}

	void HandletwitterLogin (){
		Debug.Log ("HandletwitterLogin" + currentAppID);
		switch(order) {
		case 0: 
		case 1: 
			onSuccessEarnClickCoin ();
			FBIntegrate.Instance.OnVisitTwitter ();
			break;
		case 2: 
			onSuccessEarnClickCoin ();
			TwitterManage.Instance.PostScoreFromMain ();
			break;
		case 7:
			if (!TwitterManage.IsLogined) 
			{
				onSuccessEarnClickCoin ();
				UIGameManager.Instance.HideWall ();
				TwitterManage.Instance.Login ();
			}
			break;
		}
	}

	bool CheckForFacebookLogin(){
		//Debug.Log ("FBlogin " + FB.IsLoggedIn);
		//IsURl = true;
		if (!FB.IsLoggedIn) {
			FBIntegrate.Instance.Login ();
			return false;
		}
		return true;
	}
	
	void HandleOnFBLoggedIn () {
		//Debug.Log ("HandleOnFBLoggedIn");
		FBClickFunct ();
	}

	void HandleOnFBLoginFailed (string error) {
			//Debug.Log ("HandleOnFBLoginFailed " + error);
			UIGameManager.Instance.HideWall ();
			UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
			IsURl = false;
	}

	void FBClickFunct() {
		switch(order) {
		case 3:
		case 4: IsURl = true;  FBIntegrate.Instance.OpenUrl (currentAppID);  break;
		case 5:
			onSuccessEarnClickCoin ();
			FBIntegrate.Instance.OnVisitFB ();   break;
		case 6: UIGameManager.Instance.ShowWall ("Connecting...",15); FBIntegrate.Instance.PostStatusOntimeline (); break;
		}
	}

	void HandleOnGetAppScore () {
		UIGameManager.Instance.ShowWall ("Getting Data...",25);
	}

	void HandleOnTimeExceed () {
		FBClickFunct ();
		UIGameManager.Instance.HideWall ();
	}
	
	void HandleOnProfilePicsCall () {
		FBClickFunct ();
		UIGameManager.Instance.HideWall ();
	}

	void HandleOnLikePageCall () {
		//Debug.Log ("HandleOnLikePageCall ");
		onSuccessEarnClickCoin ();
		UIGameManager.Instance.HideWall ();                          
	}

	void HandleOnPostStatusCall () {
		onSuccessEarnClickCoin ();
		UIGameManager.Instance.HideWall ();
	}

	void HandleOnPostStatusFailedCall () {
		UIGameManager.Instance.HideWall ();
	}

	void onSuccessEarnClickCoin() {
		GoogleAnalyticsScreenName (EarnFreebiesTypes.Coins);
		CoinsItems [order].OnEarned ();
		EarnStaticData.setOfferEarned (EarnTypes.Coins, order);

		//#if UNITY_ANDROID  // Only get coins for Android Version not for iOS Version
		if (order == 5) {
			if (PlayerPrefs.GetInt ("FirstTimeFacebook", 0) == 0) {
				DataManager.instance.adjustTotalCoins (EarnFreebiesStaticData.Instance.getOffersCoins (order));
				DataManager.instance.setTotalCoinsLifetime (EarnFreebiesStaticData.Instance.getOffersCoins (order));
				PlayerPrefs.SetInt ("FirstTimeFacebook", 1);
			}
		} else if (order == 7) {
			if(PlayerPrefs.GetInt ("FirstTimeTwitter",0) == 0) {
				DataManager.instance.adjustTotalCoins (EarnFreebiesStaticData.Instance.getOffersCoins (7));
				DataManager.instance.setTotalCoinsLifetime (EarnFreebiesStaticData.Instance.getOffersCoins (7));
				PlayerPrefs.SetInt ("FirstTimeTwitter", 1);
				//Debug.Log ("2");
				Updatemanager.instance.OnUpdateCoins();
				//Debug.Log ("3");
			}
		} else {
			dataManager.adjustTotalCoins (EarnStaticData.getOffersCoins (order));
			dataManager.setTotalCoinsLifetime (EarnStaticData.getOffersCoins (order));
		}
		
		Updatemanager.instance.OnUpdateEarn ();
		//#endif
	
		#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			if (order == 7) {
				dataManager.adjustTotalCoins (EarnStaticData.getOffersCoins (order));
				dataManager.setTotalCoinsLifetime (EarnStaticData.getOffersCoins (order));	
			}
		}
		#endif

		UIGameManager.Instance.PopUp ("Successfully Done..");
	}
	

	public void OnEarnClickLifeSaver(int Order) {
		this.order = Order;
//		switch(Order)
//		{
//		case 0: Handheld.PlayFullScreenMovie(Path, Color.blue,  FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.Fill); onSuccessEarnLifesaver(); break;
//		}
	}

	void onSuccessEarnLifesaver() {
		GoogleAnalyticsScreenName (EarnFreebiesTypes.LifeSaver);
		LifeSaverItems [order].OnEarned ();
		EarnStaticData.setOfferEarned (EarnTypes.LifeSaver, order);

		#if UNITY_ANDROID
		dataManager.adjustTotalLifeSavers (EarnStaticData.getOffersLifeSavers(order));
		dataManager.setTotalLifeSavers( EarnStaticData.getOffersLifeSavers(order));
		Updatemanager.instance.OnUpdateEarn ();
		#endif

		UIGameManager.Instance.PopUp ("Successfully Done..");
	}

	void UpdateMenu() {

	}
	
	void SetCoinsItems()	{
		for (int i=0; i<EarnStaticData.getOffersCoinsCount (); i++) {
			if(CoinsItems[i] == null && i != 0 /*&& i != 2 */&& i !=3 && i != 4 ) {
				GameObject go = (GameObject) Instantiate (Earn);
				go.transform.parent = this.transform;
				go.transform.localPosition = new Vector3(0,InitialY,0);
				CoinsItems[i] = go.GetComponent<EarnFreebiesBtnProperties>();
				CoinsItems[i].SetDetailsCoin (i);
				InitialY -= 2;
			}
		}
	}
	
	void SetLifeSaverItems() {
		for (int i=0; i<EarnStaticData.getOffersLifeSaverCount (); i++) {
			if(LifeSaverItems[i] == null ) {
				GameObject go = (GameObject) Instantiate (EarnLifeSaver);
				go.transform.parent = this.transform;
				go.transform.localPosition = new Vector3(0,InitialY,0);
				LifeSaverItems[i] = go.GetComponent<EarnFreebiesBtnProperties>();
				LifeSaverItems[i].SetDetailsLifeSaver(i);
				InitialY -= 2;
			}
		}
	} 

	public void GoogleAnalyticsScreenName( EarnFreebiesTypes earnFreeType)
	{
		string ScreenName = "";
		int EarnOrder = (int)(int)earnFreeType;
		switch (EarnOrder) {
		case 0 : ScreenName = "EarnFreebies."+ EarnStaticData.getOffersCoinTitle (order); break;
		case 1 :  ScreenName = "EarnFreebies."+ EarnStaticData.getOffersLifeSaverTitle (order); break;
		}
		
		UIGameManager.Instance.GoogleAnalyticsScreenName (ScreenName);
	}
}
