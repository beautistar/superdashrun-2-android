﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class FacebookManager : MonoBehaviour {
	// Use this for initialization
	public static FacebookManager Instance;
	public GameObject LoginInstance;
	void Awake()
	{
		if (Instance == null || Instance != null)
			Instance = this;

	}
	void Start () {
	}

	void OnEnable() {
		if (FB.IsLoggedIn){
			UIGameManager.Instance.ShowWall ("Connecting...",20);
			HandleOnFBLoggedIn ();
		}
		else {
			LoginInstance.SetActive (true);	
		}

		FBIntegrate.OnFBLoggedIn += HandleOnFBLoggedIn;
		FBIntegrate.OnFBLoginFailed += HandleOnFBLoginFailed;
	    FBIntegrate.OnGetAppScore += HandleOnGetAppScore;
		FBIntegrate.OnProfilePicsCall += HandleOnProfilePicsCall; 
		FBIntegrate.OnScorePostCall += HandleOnScorePostCall;
		FBIntegrate.OnScorePostFailedCall += HandleOnScorePostFailedCall;
		FBIntegrate.OnTimeExceed += HandleOnTimeExceed;
		FBIntegrate.MyScoreSucced += HandleMyScoreSucced;
		FBIntegrate.MyScoreFailed += HandleMyScoreFailed;
	}
	
	void OnDisable(){
		FBIntegrate.OnFBLoggedIn -= HandleOnFBLoggedIn;
		FBIntegrate.OnGetAppScore -= HandleOnGetAppScore;
		FBIntegrate.OnFBLoginFailed -= HandleOnFBLoginFailed;
		FBIntegrate.OnProfilePicsCall -= HandleOnProfilePicsCall;
		FBIntegrate.OnScorePostCall -= HandleOnScorePostCall;
		FBIntegrate.OnScorePostFailedCall -= HandleOnScorePostFailedCall;
		FBIntegrate.OnTimeExceed -= HandleOnTimeExceed;
		FBIntegrate.MyScoreSucced -= HandleMyScoreSucced;
		FBIntegrate.MyScoreFailed -= HandleMyScoreFailed;
	}

	public void Login(){
		if (!FB.IsLoggedIn) {
			FBIntegrate.Instance.LoginLeaderBoard ();	
		} 
		else {
			HandleOnFBLoggedIn ();
		}
		UIGameManager.Instance.ShowWall ("Connecting...", 25);
		UIGameManager.Instance.GoogleAnalyticsScreenName ("LeaderBoard.ConnectToFacebook");
	}

	void HandleOnFBLoggedIn (){
		FBIntegrate.Instance.GetmyScore ();
	}

	void HandleOnFBLoginFailed (string error){
		FB.LogOut ();
		Debug.Log ("HandleOnFBLoginFailed: "+error);
		LoginInstance.SetActive (true);
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
	}

	void HandleMyScoreSucced () {
		if (FBIntegrate.Instance.MyScore < DataManager.instance.getHighScore ())
			FBIntegrate.Instance.PostAppScore (DataManager.instance.getHighScore ().ToString ());
		else
			HandleOnScorePostCall ();
	}

	void HandleMyScoreFailed (string obj) {
		Debug.Log (obj);
		LoginInstance.SetActive (true);
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
	}

	void HandleOnScorePostCall (){
		UIGameManager.Instance.ShowWall ("Connecting...",25);
		FBIntegrate.Instance.GetAppScore ();
	}

	void HandleOnScorePostFailedCall (string error)	{
		Debug.Log ("HandleOnScorePostFailedCall: "+error);
		LoginInstance.SetActive (true);
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Could Not Post Score \n Try Again");
	}

	void HandleOnGetAppScore () {
		UIGameManager.Instance.ShowWall ("Getting Data...",25);
	}

	void GetProfilePicture() {
		//UIGameManager.Instance.ShowWall ("Getting Data...",25);
		FBIntegrate.Instance.getProfileImage ();
	}

	void HandleOnTimeExceed (){
		LoginInstance.SetActive (true);
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Time Exceeds \n Try Again");
	}
	
	void HandleOnProfilePicsCall (){
		LoginInstance.SetActive (false);
		MakeLeaderBoard ();
		UIGameManager.Instance.HideWall ();
	}

	public void MakeLeaderBoard() {
		LeaderBoardManager.Instance.MakeLeaderBoard ();
	}

}
