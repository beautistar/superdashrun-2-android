using UnityEngine;
using System.Collections;

public class AssignmentLevelManager : MonoBehaviour {
	public static AssignmentLevelManager Instance;
	public GameObject AssignmentBtnPreFab;
	public AssignmentBtn[] Btns;
	public int CompletedGoals;
	public tk2dTextMesh LevelText;
	public tk2dUIScrollbar Slider;
	public int InitialY = -2;
	int initialy =0;
	// Use this for initialization

	void Awake()
	{
		if(Instance == null || Instance != this)
			Instance = this;	


		Debug.Log("======>>>>> Ass manager: " + name);
	}

	void Start() {

	}

	public void SetAssignmentsBtn(int Level) {
		if(Btns.Length == 0)
			Btns = new AssignmentBtn[3];
		Slider.Value = 0;
		int k = 0;
		for (int i=0; i<3; i++) {
			if(Btns[i] == null) {
				GameObject go = (GameObject) Instantiate (AssignmentBtnPreFab);
				go.transform.parent = this.transform;
				go.transform.localPosition = new Vector3(0,InitialY,0);

				Btns[i] = go.GetComponent<AssignmentBtn>();
				InitialY -= 2;
			}
				
			Btns[i].Title.text = SetSpace(Level + k);
			Btns[i].Description.text = DataManager.instance.getMissionDescription ((MissionType)Level + k);	
			Btns[i].Goal = DataManager.instance.getMissionGoal ((MissionType)Level + k);
			//Debug.Log ((MissionType)Level + k + " " + DataManager.instance.getMissionGoal ((MissionType)Level + k) + " " + MissionManager.instance.AssignmentStatus(i));

			Btns[i].SetTick ( MissionManager.instance.AssignmentStatus(i));

		    if(MissionManager.instance.AssignmentStatus(i) == 1) {
				Slider.SetScrollPercentWithoutEvent (Slider.Value += 0.33f);
			}

			k += 20;
		}
		LevelText.text = "Level " + (Level+1);
	}

	string SetSpace(int  Value) {
		string str = ((MissionType) Value).ToString ();
		str = str.Replace ("_", " ");
		return str;
	}

	void RemoveBtns() {
		if (Btns.Length == 0)
			return;

		for (int i=0; i < Btns.Length; i++) {
			Destroy (Btns[i].gameObject);	
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
