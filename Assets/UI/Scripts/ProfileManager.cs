﻿using UnityEngine;
using System.Collections;

public class ProfileManager : MonoBehaviour {
	public tk2dUIScrollableArea ScrollArea;
	public tk2dTextMesh[] SingleRun;
	public tk2dTextMesh[] Lifetime;
	public GameObject Extra;
	public tk2dTextMesh[] Extras;
	public static ProfileManager instance;
	public tk2dTextMesh CharacterTitle;
	public tk2dTextMesh CharacterDescription;
	public tk2dTextMesh CharacterSpecialPowers;
	public tk2dSprite ProfilePic;
	public tk2dTextMesh Multiplier;
	float ExtraY = 0;
	// Use this for initialization
	bool FirstTime = false;

	void Awake()
	{
		instance = this;	
	}


	void Start () {
		ExtraY = Extra.transform.localPosition.y;
		OnUpdate ();
		FirstTime = true;

	}

	void OnEnable()
	{
		if(FirstTime)
			OnUpdate ();
	}
	
	// Update is called once per frame
	public void OnUpdate () {
		if (DataManager.instance == null)
			return;

		ProfilePic.SetSprite (StoreStaticData.Instance.getCharacterProfileImage (DataManager.instance.getSelectedCharacter ()));
		CharacterTitle.text = DataManager.instance.getCharacterTitle (DataManager.instance.getSelectedCharacter ());
		CharacterDescription.text = DataManager.instance.getCharacterDescription (DataManager.instance.getSelectedCharacter ()).Substring (0,80) +"...";
   		Multiplier.text = "Multiplier " + DataManager.instance.getMultiplier ().ToString () + "x";
		SingleRun [0].text = DataManager.instance.getHighScore().ToString ();
		SingleRun [1].text =  DataManager.instance.getLongestRun().ToString ();
		SingleRun [2].text = DataManager.instance.getMostCoins().ToString ();
		SingleRun [3].text = DataManager.instance.getMostLifeSavers ().ToString ();
		
		Lifetime [0].text = DataManager.instance.getTotalGame ().ToString ();
		Lifetime [1].text = DataManager.instance.getTotalDistance ().ToString ();
		Lifetime [2].text = DataManager.instance.getTotalCoinsLifetime ().ToString ();
		Lifetime [3].text = DataManager.instance.getTotalLifeSavers ().ToString ();
		Lifetime [4].text = DataManager.instance.getPowerUpLevel (PowerUpTypes.SpeedIncrease).ToString ();
		Lifetime [5].text = DataManager.instance.getPowerUpLevel (PowerUpTypes.Invincibility).ToString ();
		ProfileAchieveManager.Instance.setAchievements ();
		/*Achievements [1].text = DataManager.instance.;
		Achievements [2].text = DataManager.instance.TotalCoins;
		Achievements [3].text = DataManager.instance.TotalLifeSavers;*/
		Extra.transform.localPosition = new Vector3 (0, ExtraY + ProfileAchieveManager.Instance.InitialY, 0);
		Extras [0].text = DataManager.instance.getAppVersion ().ToString ();
		StartCoroutine ("NewContentLength");
	}

	
	IEnumerator NewContentLength() //Fixes the scrollbar length according to no of player records.
	{
		yield return null;
		float f = ScrollArea.MeasureContentLength ();
		ScrollArea.ContentLength = f - 2;
	}
}
