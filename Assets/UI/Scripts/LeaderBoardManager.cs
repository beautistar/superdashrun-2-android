﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class LeaderBoardManager : MonoBehaviour {
	public float Gap = -1.7f;
	public LeaderBoardBtnProperties[] Blocks;
	public GameObject BlockObject;
	public static LeaderBoardManager Instance;
	public tk2dUIScrollableArea ScrollArea;
	public int y = 150;
	float gap;
	// Use this for initialization

	void Awake()
	{
		if (Instance == null || Instance != null)
			Instance = this;
	}

	void Start () {
		gap = Gap;
	}

	bool ShowText = false;

	public void MakeLeaderBoard()
	{
		DeleteAlreadyScore ();
		Blocks = new LeaderBoardBtnProperties[FBIntegrate.Instance.Count];
		Gap = gap;
		for (int i=0; i<FBIntegrate.Instance.Count; i++) {
			GameObject Go =  (GameObject) Instantiate (BlockObject);
			Go.transform.parent = this.gameObject.transform;
			//Go.transform.Translate (new Vector3(0,Gap,0));
			Blocks[i] = Go.GetComponent<LeaderBoardBtnProperties>();
			Blocks[i].transform.localPosition = new Vector3(0,Gap+7,0f);
			Blocks[i].SetData (i);
			Gap = Gap + gap;
		}
		StartCoroutine ("NewContentLength");
	}

	IEnumerator NewContentLength() //Fixes the scrollbar length according to no of player records.
	{
		yield return null;
		float f = ScrollArea.MeasureContentLength ();
		ScrollArea.ContentLength = f - 4;
	}

	void DeleteAlreadyScore(){
		if (Blocks.Length > 0) {
			for(int i=0;i<Blocks.Length;i++)
				Destroy (Blocks[i].gameObject);
		}
	}

	void OnGUI()
	{
		if (FB.IsLoggedIn&& ShowText) { //, ScaleMode.ScaleToFit, true, 10.0f
			y = 200;
			for(int i=0;i<FBIntegrate.Instance.ProfilePics.Count;i++)
			{
				GUI.DrawTexture (new Rect (75, y, 80, 80), FBIntegrate.Instance.ProfilePics[i]);
				y += 90;
			}
		}

	}

}
