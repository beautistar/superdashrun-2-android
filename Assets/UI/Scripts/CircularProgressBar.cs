using UnityEngine;
using System.Collections;

public class CircularProgressBar : MonoBehaviour {
	 float f = 0;
	bool NotStarted = true;
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void OnEnable(){
		NotStarted = false;
		f = 0.1f;

	}

	void OnDisable(){
		NotStarted = true;
	}

    private float _current;
    private float _max;

    void StartTimer(float current, float max)
    {

    }

	void Update () {

		if (NotStarted)
			return;

		//renderer.material.SetFloat("_Cutoff", Mathf.InverseLerp(0, Screen.width, Input.mousePosition.x)); 
		f += Time.deltaTime ;

		GetComponent<Renderer>().material.SetFloat("_Cutoff", Mathf.InverseLerp(6.2f, 0.7f,f)); 
	}
}
