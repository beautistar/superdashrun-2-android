﻿using UnityEngine;
using System.Collections;

public class FbReminder : MonoBehaviour {
    public GameObject window;

    private const int REMIND_EVERY_TIMES = 3;

    private static bool _expired = false;

    public static FbReminder Instance;

    private Animation _anim;

    void Awake()
    {
        Instance = this;

        _anim = window.GetComponent<Animation>();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
    public void Show()
    {
        // if not shown and facebook not activated
        if (!_expired && PlayerPrefs.GetInt("FirstTimeFacebook", 0) == 0)
        {
            _expired = true;

            StartCoroutine(ShowRoutine());
        }
        
    }

    private IEnumerator ShowRoutine()
    {
        _anim.Play("Alert");
        yield return new WaitForSeconds(5.0f);
        _anim.Play("Alertback");
    }
	
}
