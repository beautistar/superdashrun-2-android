﻿using UnityEngine;
using System.Collections;

public class TutorialSign : MonoBehaviour {

    public GameObject sign;
    private tk2dTextMesh textMesh;

    public static TutorialSign Instance;

    void Awake()
    {
        Instance = this;
    }

    void Start ()
    {
        sign.SetActive(true);
        textMesh = sign.GetComponent<tk2dTextMesh>();
        sign.SetActive(false);
    }
	


    public void ShowStartSign()
    {
        sign.SetActive(true);
        Color c = textMesh.color;
        c.a = 1;
        textMesh.color = c;

        StartCoroutine(fadeOut());
    }

    private IEnumerator fadeOut()
    {
        yield return new WaitForSeconds(1);

        int steps = 15;
        float fadeStep = 1.0f / (float)steps;
        Color c = textMesh.color;
        float fadeTime = 1.0f;
        float timeStep = fadeTime / (float)steps;
        WaitForSeconds dt = new WaitForSeconds(timeStep);

        for (int i = 0; i < steps; ++i)
        {
            c.a -= fadeStep;
            textMesh.color = c;
            yield return dt;
        }
    }
}
