﻿using UnityEngine;
using System.Collections;

public class MudSplash : MonoBehaviour 
{
	public Texture2D [] splashTextures = new Texture2D[4];
	// Use this for initialization
	void OnEnable () 
	{
		StartCoroutine ("ChangeSplash");
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	private IEnumerator ChangeSplash()
	{
		int i = 0;
		while (true) 
		{
			GetComponent<Renderer>().material.SetTexture ("_MainTex", splashTextures [i]);
			i++;
			yield return new WaitForSeconds(0.1f);
			if(i == 4)
				i = 0;
		}
	}

	void OnDisable()
	{
		StopCoroutine("ChangeSplash");
	}
}
