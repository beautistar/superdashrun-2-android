﻿using UnityEngine;
using System.Collections;

public class SuperPogoStickVisibility : MonoBehaviour {
	public GameObject pogoStick;
	private AudioSource mySounds;

	void OnEnable()
	{
		mySounds = GetComponent<AudioSource>();
		if(SettingStaticData.Instance.getSoundOnOff())
        {
            mySounds.enabled = true;
            mySounds.volume = SettingStaticData.Instance.getSoundValue();
        }
        else
        {
        	mySounds.enabled = false;
        }
		pogoStick.SetActive(true);
		StartCoroutine(ReSetMyPogoStick());
	}

	// Use this for initialization
	void Start () {
	
	}
	

	IEnumerator ReSetMyPogoStick()
	{
		yield return new WaitForSeconds(0.7f);
		//pogoStick.SetActive(false);
	}
}
