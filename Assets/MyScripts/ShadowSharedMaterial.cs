﻿using UnityEngine;
using System.Collections;

public class ShadowSharedMaterial : MonoBehaviour {
	public Material tempMaterial;
	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().sharedMaterial = tempMaterial;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
