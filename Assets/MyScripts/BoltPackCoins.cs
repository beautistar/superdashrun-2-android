﻿using UnityEngine;
using System.Collections;

public class BoltPackCoins : MonoBehaviour {
	MeshRenderer meshRenderer;
	BoxCollider boxCollider;
	Transform thisTransform;
	public GameObject temp;
	void Awake()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		boxCollider = GetComponent<BoxCollider>();
		thisTransform = transform;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack))
		{
			meshRenderer.enabled = false;
			if(boxCollider != null)
				boxCollider.enabled = false;
		}
		else
		{
			if(temp != null && PowerUpManager.instance != null && PlayerController.instance != null)
			if(Vector3.SqrMagnitude(thisTransform.position - temp.transform.position) < ((PlayerController.instance.PlayerForwardSpeed() * PowerUpManager.instance.BoltPackDuration())-22500))
			{
				meshRenderer.enabled = true;
				if(boxCollider != null)
					boxCollider.enabled = true;
			}
		}
	}
}
