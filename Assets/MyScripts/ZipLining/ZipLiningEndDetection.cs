﻿using UnityEngine;
using System.Collections;

public class ZipLiningEndDetection : MonoBehaviour 
{
	private PlayerController playerController;
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider other)
	{

		playerController = PlayerController.instance;
		if((other.gameObject.tag).Equals("Player") || (other.gameObject.transform.parent.tag).Equals("Player"))
		{
			playerController.ZipLiningEnding();
		}
	}
}
