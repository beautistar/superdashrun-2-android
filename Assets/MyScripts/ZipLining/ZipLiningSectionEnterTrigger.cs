﻿using UnityEngine;
using System.Collections;

public class ZipLiningSectionEnterTrigger : MonoBehaviour 
{
	private PlayerController playerController;
	// Use this for initialization
	void Start () 
	{

	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	IEnumerator OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag.Equals ("Player")) {
						if (PowerUpManager.instance.isPowerUpActive (PowerUpTypes.BoltPack)) {
								playerController = PlayerController.instance;
								StartCoroutine (playerController.GladiatorHeightDecreasing ());
								yield return new WaitForSeconds (2);
								if (PowerUpManager.instance.isPowerUpActive1 (PowerUpTypes.BoltPack)) 
										PowerUpManager.instance.deactivatePowerUp1 ();
								else
										PowerUpManager.instance.deactivatePowerUp ();
								if (playerController != null)
										playerController.SettingMyLayOut ();
						}	
				}
	}
}
