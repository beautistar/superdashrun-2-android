﻿using UnityEngine;
using System.Collections;

public class ZipLiningJump : MonoBehaviour {

	private PlayerController playerController;
	private PowerUpManager powerUpManager;
	private bool collisionDetected;
	//public GameObject explosion;

	void Awake()
	{
		collisionDetected = false;
	}
	// Use this for initialization
	void Start () 
	{
		powerUpManager = PowerUpManager.instance;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if((other.gameObject.tag).Equals("Player"))
		{
			//explosion.SetActive(true);
			//explosion.GetComponent<TimedObjectDestructor>().Invoke("DestroyNow",4);
			collisionDetected = true;
			playerController = PlayerController.instance;
			playerController.ZipLiningJump();
			if(powerUpManager.isPowerUpActive(PowerUpTypes.SpeedIncrease) || powerUpManager.isPowerUpActive(PowerUpTypes.Invincibility) || powerUpManager.isPowerUpActive(PowerUpTypes.BoltPack) || powerUpManager.isPowerUpActive1(PowerUpTypes.SpeedIncrease) || powerUpManager.isPowerUpActive1(PowerUpTypes.Invincibility) || powerUpManager.isPowerUpActive1(PowerUpTypes.BoltPack))
			{

				print ("------ZipLiningJump");
				PlayerController.instance.jump(true);
			}

		}
	}

	/*void OnTriggerStay(Collider other)
	{
		if((other.gameObject.tag).Equals("Player") || (other.gameObject.transform.parent.tag).Equals("Player"))
		{
			if(!collisionDetected)
			{
				collisionDetected = true;
				playerController.ZipLiningJump();
				if(powerUpManager.isPowerUpActive(PowerUpTypes.SpeedIncrease) || powerUpManager.isPowerUpActive(PowerUpTypes.Invincibility) || powerUpManager.isPowerUpActive(PowerUpTypes.BoltPack) || powerUpManager.isPowerUpActive1(PowerUpTypes.SpeedIncrease) || powerUpManager.isPowerUpActive1(PowerUpTypes.Invincibility) || powerUpManager.isPowerUpActive1(PowerUpTypes.BoltPack))
				{
					playerController.jump(true);
				}
			}
		}
	}*/
}
