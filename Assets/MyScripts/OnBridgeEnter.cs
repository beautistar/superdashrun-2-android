﻿using UnityEngine;
using System.Collections;

public class OnBridgeEnter : MonoBehaviour 
{
private CameraController cameraController;
	// Use this for initialization
	void Start () 
	{
		cameraController = CameraController.instance;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider colli)
	{
		if((colli.gameObject.tag).Equals("Player"))
		{
			cameraController.CameraUnderBridge(true);
		}
	}
}
