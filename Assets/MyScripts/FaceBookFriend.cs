﻿using UnityEngine;
using System.Collections;

public class FaceBookFriend : MonoBehaviour {
	public GameObject friendRenderer;
	public GameObject backCollider;
	BoxCollider boxCollider;
	bool myslefSpawned;
	// Use this for initialization
	void OnEnable () 
	{
		transform.localScale = Vector3.one * 1.2f;
		myslefSpawned = false;
		boxCollider = GetComponent<BoxCollider> ();
		MySetting (false);
	}

	void OnDisable()
	{
		MySetting (false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (myslefSpawned == false) {
						if (Vector3.Distance (PlayerController.instance.GetComponent<Transform> ().position, transform.position) < 90) {
								if (PlayerController.instance.FacebookFriendStatusGetter ()) {
										MySetting (true);
										myslefSpawned = true;
										PlayerController.instance.FacebookFriendSetter (false);
								} else {
										MySetting (false);
								}
						} else {
								MySetting (false);
						}
				}
	}

	void MySetting(bool temp)
	{
		friendRenderer.SetActive (temp);
		backCollider.SetActive (temp);
		boxCollider.enabled = temp;
	}
}
