﻿using UnityEngine;
using System.Collections;

public class Left : MonoBehaviour {

int mySelfTrigger;
protected int playerLayer;
	// Use this for initialization
	void Start () {
		mySelfTrigger = 0;
		playerLayer = LayerMask.NameToLayer("Player");
	}
	
	

	void OnTriggerEnter(Collider other) 
	{
        if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
        	if (!PowerUpManager.instance.isPowerUpActive(PowerUpTypes.SpeedIncrease) && !PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack) && !PowerUpManager.instance.isPowerUpActive(PowerUpTypes.Invincibility) && !PowerUpManager.instance.isPowerUpActive(PowerUpTypes.SuperPogoStick)) {
            mySelfTrigger = 2;
            transform.parent.GetComponent<ObstacleObject>().AfterCollision(2,other);
        	}
		}

	}

	public int MySelfTriggeringGetter()
	{
		return mySelfTrigger;
	}

	public void MySelfTriggeringSetter()
	{
		mySelfTrigger = 0;
	}
}
