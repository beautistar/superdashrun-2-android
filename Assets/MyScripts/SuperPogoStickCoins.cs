﻿using UnityEngine;
using System.Collections;

public class SuperPogoStickCoins : MonoBehaviour {
	MeshRenderer meshRenderer;
	BoxCollider boxCollider;

	void Awake()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		boxCollider = GetComponent<BoxCollider>();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!PowerUpManager.instance.isPowerUpActive(PowerUpTypes.SuperPogoStick))
		{
			meshRenderer.enabled = false;
			if(boxCollider != null)
				boxCollider.enabled = false;
		}
		else
		{
			meshRenderer.enabled = true;
			if(boxCollider != null)
				boxCollider.enabled = true;
		}
	}
}
