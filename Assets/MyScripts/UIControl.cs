﻿using UnityEngine;
using System.Collections;

public class UIControl : MonoBehaviour 
{
	static public UIControl instance;
	public void Awake()
	{
		instance = this;	
	}
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void SetMyState(bool temp)
	{
		gameObject.SetActive(temp);
	}
}
