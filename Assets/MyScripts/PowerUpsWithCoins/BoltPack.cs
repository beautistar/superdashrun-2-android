﻿using UnityEngine;
using System.Collections;

public class BoltPack : MonoBehaviour {

	private PowerUpTypes powerUpType;
	private GameManager gameManager;
	private DataManager dataManager;
	private int playerLayer;
	void Awake()
	{
		playerLayer = LayerMask.NameToLayer("Player");
		gameManager = GameManager.instance;
		dataManager = DataManager.instance;
		powerUpType = PowerUpTypes.BoltPack;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.layer == playerLayer) {
			if(PlayerController.instance.IsSaveMeOnGetter())
			{
				return;
			}
            gameManager.activatePowerUp(powerUpType, true);
            dataManager = DataManager.instance;
            dataManager.SetLevelPowerUpCount(powerUpType);
		}
	}
}
