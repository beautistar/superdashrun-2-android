﻿using UnityEngine;
using System.Collections;

public class MudSlideTutorialEnter : MonoBehaviour {

	private bool _isIn = false;

	void OnTriggerEnter(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player") && !_isIn) {
			_isIn = true;
			if(PlayerPrefs.GetInt("MudSlidingTutorial", 0) == 0)
				TutorialManager.Instance.ShowDown();
			//GetComponent<GUIText>().text = "Swipe Up To Jump";
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			_isIn = false;
		}
	}

}
