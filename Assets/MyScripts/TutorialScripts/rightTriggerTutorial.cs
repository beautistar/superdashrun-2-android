using UnityEngine;
using System.Collections;

public class rightTriggerTutorial : MonoBehaviour {

	private bool _isIn = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame

	void OnTriggerEnter(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player") && !_isIn) {
			_isIn = true;
			TutorialManager.Instance.ShowRight();
			//print("Swipe Right");
			//GetComponent<GUIText>().text = "Swipe Right";
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			_isIn = false;
			TutorialManager.Instance.StopAnimation();
			SettingStaticData.Instance.setTutorial();

			UpdatedTutorial.Instance.ShowTutorialSuccessMsg();

			//print("Swipe Right");
			//GetComponent<GUIText>().text = "";
		}
	}
}
