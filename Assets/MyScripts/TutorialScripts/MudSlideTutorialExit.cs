﻿using UnityEngine;
using System.Collections;

public class MudSlideTutorialExit : MonoBehaviour {

	void OnTriggerExit(Collider other){
		
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			PlayerPrefs.SetInt("MudSlidingTutorial", 1);
			TutorialManager.Instance.StopAnimation();
			//GetComponent<GUIText>().text = "";
		}
	}
}
