﻿using UnityEngine;
using System.Collections;

public class ZipLiningTutorialTriggerExit : MonoBehaviour {

	void OnTriggerExit(Collider other){
		
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			PlayerPrefs.SetInt("ZipLiningTutorial", 1);
			TutorialManager.Instance.StopAnimation();
			//GetComponent<GUIText>().text = "";
		}
	}
}
