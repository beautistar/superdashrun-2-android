using UnityEngine;
using System.Collections;

public class MudSplashTricklingTrail : MonoBehaviour {
	public Texture2D []BigPatchTrail = new Texture2D[4];
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnEnable1()
	{
		if(gameObject.name.Contains ("1"))
			GetComponent<Renderer>().material.mainTexture = BigPatchTrail[0];
		if(gameObject.name.Contains ("2"))
			GetComponent<Renderer>().material.mainTexture = BigPatchTrail[2];
		transform.localScale = new Vector3 (1, 1, 0.1f);
		StartCoroutine (ScaleUp ());
	}

	IEnumerator ScaleUp()
	{
		while (transform.localScale.z < 1.5f) 
		{
			yield return new WaitForSeconds(0.05f);
			transform.localScale += new Vector3(0,0,0.1f);
		}
	}
}
