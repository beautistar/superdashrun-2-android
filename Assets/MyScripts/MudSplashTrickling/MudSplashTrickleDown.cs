﻿using UnityEngine;
using System.Collections;

public class MudSplashTrickleDown : MonoBehaviour {
	public Texture2D []ImageSequence = new Texture2D[8];
	Vector3 myLocalPosition;
	public Vector3 min;
	public Vector3 max;
	public GameObject myChild;
	public Texture2D []BigPatch = new Texture2D[3];
	// Use this for initialization
	public void OnEnable1()
	{
		GetComponent<MeshRenderer> ().enabled = true;
		transform.localPosition = myLocalPosition;
		//renderer.material.SetTexture ("_MainTex", ImageSequence [0]);
		if (gameObject.name.Contains ("Big")) {
			if(gameObject.name.Contains ("1"))
				GetComponent<Renderer>().material.mainTexture = BigPatch[0];
			if(gameObject.name.Contains ("2"))
				GetComponent<Renderer>().material.mainTexture = BigPatch[2];
						StartCoroutine (ImageSequenceChanging ());
						if (myChild != null) {
								myChild.GetComponent<MudSplashTricklingTrail> ().OnEnable1 ();		
						}
				} else {
			StartCoroutine("MudSplashTricklingSmall");
		}
		//transform.localScale = min;
		//StartCoroutine (ScaleUp ());
	}

	void Start () 
	{
		GetComponent<MeshRenderer> ().enabled = false;
		myLocalPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/*IEnumerator MudSplashTricklingDown()
	{

	}*/

	IEnumerator ImageSequenceChanging()
	{
		/*for (int i = 0; i < 8; i++) 
		{
			yield return new WaitForSeconds(0.1f);
			renderer.material.SetTexture ("_MainTex", ImageSequence [i]);
		}*/
		while(transform.position.y > -14f)
		{
			yield return new WaitForSeconds(0.05f);
			transform.localPosition = transform.localPosition + new Vector3(0,-0.1f,0);
		}
	}

	IEnumerator ScaleUp()
	{
		for (float i = min.x, j = min.y,k = min.z; (i <= max.x || j <= max.y || k <= max.z);) 
		{
			if(i < max.x)
				i = i+0.05f;
			if(j < max.y)
				j = j+0.05f;
			if(k < max.z)
				k = k+0.05f;
			transform.localScale = new Vector3(i,j,k);
			yield return new WaitForSeconds(0.3f);
		}
	}

	private IEnumerator MudSplashTricklingSmall()
	{
		Color c = GetComponent<Renderer>().material.GetColor("_TintColor");
		c.a = 0.5f;
		GetComponent<Renderer>().material.SetColor("_TintColor",c); 
		while (GetComponent<Renderer>().material.GetColor("_TintColor").a > 0.2f) 
		{
			yield return new WaitForSeconds (0.5f);
			c.a = GetComponent<Renderer>().material.GetColor("_TintColor").a - 0.1f;
			GetComponent<Renderer>().material.SetColor("_TintColor",c); 
		}
		c.a = 0;
		GetComponent<Renderer>().material.SetColor("_TintColor",c); 
	}
}
