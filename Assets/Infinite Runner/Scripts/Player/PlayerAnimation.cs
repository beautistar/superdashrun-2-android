using UnityEngine;
using System.Collections;

/*
 * Basic class to manage the different animation states
 */
public class PlayerAnimation : MonoBehaviour
{
    private int jumpNumber;
    private int rollNumber;
#if !UNITY_3_5
    public bool useMecanim = false;
#endif

    // the amount of time it takes to transition from the previous animation to a run
    public float runTransitionTime;

    // animation names
    public string runAnimationName = "Run";
    public string runJumpAnimationName = "RunJump";
    public string runJumpAnimationRollName = "RunJumpRoll";
    public string runJumpAnimation1Name = "RunJump1";
    public string runSlideAnimationName = "RunSlide";
    public string runSlideAnimationRollName = "RunSlide";
    public string runRightStrafeAnimationName = "RunRightStrafe";
    public string runLeftStrafeAnimationName = "RunLeftStrafe";
    public string attackAnimationName = "Attack";
    public string backwardDeathAnimationName = "BackwardDeath";
    public string forwardDeathAnimationName = "ForwardDeath";
    public string slidingDeathAnimationName = "SlidingDeath";
    public string idleAnimationName = "Idle";
    public string runScooterAnimationName = "ScooterTransform";
	public string runZipLiningAnimationName = "ZipLiningTransform";
    public string runPogoStickAnimationName = "PogoStickTransform";
    public string runGladiatorAnimationName = "GladiatorTransform";
    public string restartAnimationName = "Restart";
    public string headTurnAnimationName = "HeadTurn";
    public string gliderNameAnimationName = "GliderLand";
    public string pogoStickLandAnimationName = "PogoLand";
    public Avatar [] myAvatar = new Avatar[3];
    // the speed of the run animation when the player is running
    public float slowRunSpeed = 1.0f;
    public float fastRunSpeed = 1.0f;

    private Animation thisAnimation;
#if !UNITY_3_5
    private Animator thisAnimator;
#endif

    public void init()
    {

        jumpNumber = 0;

#if !UNITY_3_5
        if (useMecanim) {
            thisAnimator = GetComponent<Animator>();
        } else {
#endif
            thisAnimation = GetComponent<Animation>();

            thisAnimation[runAnimationName].wrapMode = WrapMode.Loop;
            thisAnimation[runAnimationName].layer = 0;
            thisAnimation[runRightStrafeAnimationName].wrapMode = WrapMode.Once;
            thisAnimation[runRightStrafeAnimationName].layer = 1;
            thisAnimation[runLeftStrafeAnimationName].wrapMode = WrapMode.Once;
            thisAnimation[runLeftStrafeAnimationName].layer = 1;
            if (!runJumpAnimationName.Equals("")) {
                thisAnimation[runJumpAnimationName].wrapMode = WrapMode.ClampForever;
                thisAnimation[runJumpAnimationName].layer = 1;
            }
            if (!runSlideAnimationName.Equals("")) {
                thisAnimation[runSlideAnimationName].wrapMode = WrapMode.ClampForever;
                thisAnimation[runSlideAnimationName].layer = 1;
            }
            if (!attackAnimationName.Equals("")) {
                thisAnimation[attackAnimationName].wrapMode = WrapMode.Once;
                thisAnimation[attackAnimationName].layer = 1;
            }
            if (!backwardDeathAnimationName.Equals("")) {
                thisAnimation[backwardDeathAnimationName].wrapMode = WrapMode.ClampForever;
                thisAnimation[backwardDeathAnimationName].layer = 2;
            }
            if (!forwardDeathAnimationName.Equals("")) {
                thisAnimation[forwardDeathAnimationName].wrapMode = WrapMode.ClampForever;
                thisAnimation[forwardDeathAnimationName].layer = 2;
            }
            thisAnimation[idleAnimationName].wrapMode = WrapMode.Loop;
            thisAnimation[idleAnimationName].layer = 3;
#if !UNITY_3_5
        }
#endif
    thisAnimator.avatar = myAvatar[0];

    }

    public void OnEnable()
    {
        GameManager.instance.onPauseGame += onPauseGame;
    }

    public void OnDisable()
    {
        GameManager.instance.onPauseGame -= onPauseGame;
    }

    public void run()
    {
#if !UNITY_3_5
        if (useMecanim) {;
			//Debug.Log ("run");
            thisAnimator.CrossFade(runAnimationName,0.0f,0,0.0f);
            }else{;
#endif
            //thisAnimation.CrossFade(runAnimationName, runTransitionTime, PlayMode.StopAll);
#if !UNITY_3_5
        }
#endif
    }

    public void setRunSpeed(float speed, float t)
    {
#if !UNITY_3_5
        if (useMecanim) {
            thisAnimator.SetFloat("Speed", speed);
        } else {
#endif
            thisAnimation[runAnimationName].speed = Mathf.Lerp(slowRunSpeed, fastRunSpeed, t);
#if !UNITY_3_5
        }
#endif
    }

    public void strafe(bool right)
    {
#if !UNITY_3_5
        if (useMecanim) {
            if (right) {
                //thisAnimator.CrossFade(runRightStrafeAnimationName,0.0f,0,0.0f);
                StartCoroutine(playOnce(runRightStrafeAnimationName));
            } else {
                //thisAnimator.CrossFade(runLeftStrafeAnimationName,0.0f,0,0.0f);
                StartCoroutine(playOnce(runLeftStrafeAnimationName));
            }
        } else {
#endif
            if (right) {
                thisAnimation.CrossFade(runRightStrafeAnimationName, 0.05f);
            } else {
                thisAnimation.CrossFade(runLeftStrafeAnimationName, 0.05f);
            }
#if !UNITY_3_5
        }
#endif
    }

    public void jump(string temp)
    {
#if !UNITY_3_5
        if (useMecanim) {
			if(temp.Contains("Platform_23_Straight") || temp.Contains("Platform_16_Straight")|| temp.Contains("Platform_17_Straight")||temp.Contains("Platform_20_Straight"))
				jumpNumber = 0;
            if(jumpNumber == 0)
            {
                //thisAnimator.CrossFade(runJumpAnimationName,0.0f,0,0.0f);
                StartCoroutine(playOnce(runJumpAnimationName));
                jumpNumber = 1;
            }
            else if(jumpNumber == 1)
            {
                //thisAnimator.CrossFade(runJumpAnimationRollName,0.0f,0,0.0f);
                StartCoroutine(playOnce(runJumpAnimationRollName));
                jumpNumber = 2;
            }
            else if(jumpNumber == 2)
            {
                //thisAnimator.CrossFade(runJumpAnimationRollName,0.0f,0,0.0f);
                StartCoroutine(playOnce(runJumpAnimation1Name));
                jumpNumber = 0;
            }
                    
        } else {
#endif
            thisAnimation.CrossFade(runJumpAnimationName, 0.1f);
#if !UNITY_3_5
        }
#endif
    }

    public void slide()
    {
#if !UNITY_3_5
        if (useMecanim) {
            if(rollNumber == 0)
            {
                //thisAnimator.CrossFade(runSlideAnimationName,0.0f,0,0.0f);
                StartCoroutine(playOnce(runSlideAnimationName));
            //    rollNumber = 1;
            }
            else
            {
                thisAnimator.CrossFade(runSlideAnimationName,0.0f,0,0.0f);
                StartCoroutine(playOnce(runSlideAnimationRollName));
                rollNumber = 0;
            }
        } else {
#endif
            thisAnimation.CrossFade(runSlideAnimationName);
#if !UNITY_3_5
        }
#endif
    }

    public void attack()
    {
#if !UNITY_3_5
        if (useMecanim) {
            StartCoroutine(playOnce(attackAnimationName));
        } else {
#endif
            thisAnimation.CrossFade(attackAnimationName, 0.1f);
#if !UNITY_3_5
        }
#endif
    }

    public void idle()
    {
#if !UNITY_3_5
        if (!useMecanim) {
#endif
            if (thisAnimation == null) {
                thisAnimation = GetComponent<Animation>();
                thisAnimation[idleAnimationName].wrapMode = WrapMode.Loop;
            }
            thisAnimation.Play(idleAnimationName);
#if !UNITY_3_5
        }
#endif
    }

    public void gameOver(GameOverType gameOverType)
    {
#if !UNITY_3_5
        if (!useMecanim) {
#endif
        thisAnimation.Stop(runAnimationName);
#if !UNITY_3_5
        }
#endif

        if (gameOverType != GameOverType.Quit) {
            if (gameOverType == GameOverType.Pit) {
#if !UNITY_3_5
                if (useMecanim) {
                    //thisAnimator.CrossFade(slidingDeathAnimationName,0.0f,0,0.0f);
                    StartCoroutine(playOnce(slidingDeathAnimationName));
                    //print("ForwardDeath");
                } else {
#endif
                    thisAnimation.Play(slidingDeathAnimationName);
#if !UNITY_3_5
                }
#endif
            }
            else if (gameOverType == GameOverType.JumpObstacle) {
#if !UNITY_3_5
                if (useMecanim) {
                    //thisAnimator.CrossFade(forwardDeathAnimationName,0.0f,0,0.0f);

                    StartCoroutine(playOnce(backwardDeathAnimationName));
                    //print("ForwardDeath");
                } else {
#endif
                    thisAnimation.Play(backwardDeathAnimationName);
#if !UNITY_3_5
                }
#endif
            } else {
#if !UNITY_3_5
                if (useMecanim) {
                    //thisAnimator.CrossFade(backwardDeathAnimationName,0.0f,0,0.0f);
                    StartCoroutine(playOnce(backwardDeathAnimationName));
                    //print("BackDeath");
                } else {
#endif
                    thisAnimation.Play(backwardDeathAnimationName);
#if !UNITY_3_5
                }
#endif
            }
        }
    }

    public void reset()
    {
#if !UNITY_3_5
        if (useMecanim) {
            thisAnimator.SetFloat("Speed", 0);
        } else {
#endif
            thisAnimation.Play(idleAnimationName);
#if !UNITY_3_5
        }
#endif
    }

    public void onPauseGame(bool paused)
    {
        float speed = (paused ? 0 : 1);
#if !UNITY_3_5
        if (useMecanim) {
            thisAnimator.speed = speed;
        } else {
#endif
            foreach (AnimationState state in thisAnimation) {
                state.speed = speed;
            }
#if !UNITY_3_5
        }
#endif
    }

    public void runScooterAnimation(bool state)
    {
#if !UNITY_3_5
        if (useMecanim) {
             StartCoroutine(playOnceBool(runScooterAnimationName,state));
        } else {
#endif
            thisAnimation.Play(runScooterAnimationName);
#if !UNITY_3_5
        }
#endif
    }

	public void runZipLiningAnimation(bool state)
	{
		#if !UNITY_3_5
		if (useMecanim) {
			StartCoroutine(playOnceBool(runZipLiningAnimationName,state));
		} else {
			#endif
			thisAnimation.Play(runZipLiningAnimationName);
			#if !UNITY_3_5
		}
		#endif
	}
	
	public void runPogoStickAnimation(bool state)
    {
#if !UNITY_3_5
        if (useMecanim) {
			//Debug.Log ("runPogoStickAnimation");
             StartCoroutine(playOnceBool(runPogoStickAnimationName,state));
        } else {
#endif
            thisAnimation.Play(runPogoStickAnimationName);
#if !UNITY_3_5
        }
#endif
    }

    public void runGladiatorAnimation(bool state)
    {
#if !UNITY_3_5
        if (useMecanim) {
             StartCoroutine(playOnceBool(runGladiatorAnimationName,state));
        } else {
#endif
            thisAnimation.Play(runGladiatorAnimationName);
#if !UNITY_3_5
        }
#endif
    }

    public void gliderLandAnimation(bool state)
    {
#if !UNITY_3_5
        if (useMecanim) {
             StartCoroutine(playOnceBool(gliderNameAnimationName,state));
        } else {
#endif
            thisAnimation.Play(runGladiatorAnimationName);
#if !UNITY_3_5
        }
#endif
    }

    public void pogoStickLandAnimation(bool state)
    {
	
#if !UNITY_3_5
        if (useMecanim) {
             StartCoroutine(playOnceBool(pogoStickLandAnimationName,state));
        } else {
#endif
            thisAnimation.Play(pogoStickLandAnimationName);
#if !UNITY_3_5
        }
#endif
    }

    public void restartAnimation(bool state)
    {
#if !UNITY_3_5
        if (useMecanim) {
             StartCoroutine(playOnce(restartAnimationName));
        } else {
#endif
            thisAnimation.Play(runScooterAnimationName);
#if !UNITY_3_5
        }
#endif
    }

     public void headTurnAnimation(bool state)
    {
#if !UNITY_3_5
        if (useMecanim) {
             StartCoroutine(playOnce(headTurnAnimationName));
        } else {
#endif
            thisAnimation.Play(headTurnAnimationName);
#if !UNITY_3_5
        }
#endif
    }

#if !UNITY_3_5
    public IEnumerator playOnce(string eventName)
    {
        /*if(eventName.Equals("BackwardDeath"))
            thisAnimator.avatar = myAvatar[4];
        else
            thisAnimator.avatar = myAvatar[0];*/    
        //thisAnimator.SetBool(eventName, true);
        yield return null;
        //thisAnimator.SetBool(eventName, false);

		//Debug.Log("====>>>>Trigger: " + eventName);

        thisAnimator.SetTrigger(eventName);
    }

    public IEnumerator playOnceBool(string eventName,bool state)
    {
        yield return null;
        if(state)
        {
            if(eventName.Equals("GladiatorTransform"))
                thisAnimator.avatar = myAvatar[2];
            else if(eventName.Equals("ScooterTransform"))    
                    thisAnimator.avatar = myAvatar[1];
                    else if(eventName.Equals("PogoStickTransform"))    
                        thisAnimator.avatar = myAvatar[3];
						else if(eventName.Equals("ZipLiningTransform"))    
							thisAnimator.avatar = myAvatar[0];
			
		}
		else    
            thisAnimator.avatar = myAvatar[0];

        thisAnimator.SetBool(eventName, state);
        //thisAnimator.SetTrigger(eventName);
    }
#endif
}



