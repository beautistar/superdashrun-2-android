using UnityEngine;
using System.Collections;

/*
 * Basic camera script which doesn't really do anything besides hold variables so it knows where to reset
 */
public class CameraController : MonoBehaviour
{
	static public CameraController instance;
	
	// the target position and rotation of the camera
	public Vector3 inGamePosition;
	public Vector3 inGameRotation;
	
	public float moveSpeed;
	public float rotationSpeed;
	public bool steadyHeight;
	
	private bool isCameraUnderBridge;
	private bool isJumping;

	public Vector3 underRopePosition;
	public Vector3 underRopeRotation;
	private Quaternion underRopeQuaternionRotation;

	public Vector3 underBridgePosition;
	public Vector3 gliderPosition;
	public Vector3 positionDuringjump;
	// How long should the camera shake for if the player collided with an obstacle
	public float shakeDuration;
	public float shakeIntensity;
	private bool isShaking;
	private float shakeStartTime;
	private float shakeDelta;
	private float currentShakeIntensity;
	
	private Vector3 targetPosition;
	private Quaternion targetRotation;
	
	
	private Vector3 startPosition;
	private Quaternion startRotation;
	
	private int platformLayer;
	private float startPlatformDistance;
	
	private Transform thisTransform;
	private PlayerController playerController;
	private Transform playerTransform;
	private bool isGlider;
	private bool isRopeOn;
	private bool isFromGameOver;
	// for pausing:
	private float pauseTime;
	
	public void Awake()
	{
		instance = this;
		
		startPosition = transform.position;
		startRotation = transform.rotation;
	}
	
	public void Start()
	{
		thisTransform = transform;
		isCameraUnderBridge = false;
		isGlider = false;
		isRopeOn = false;
		isJumping = false;
		isFromGameOver = false;
		targetPosition.x = 0;
		// cast a ray against both the platform and platform jump layer
		platformLayer = (1 << LayerMask.NameToLayer("Platform")) | (1 << LayerMask.NameToLayer("PlatformJump")) | (1 << LayerMask.NameToLayer("Floor"));
		
		enabled = false;
	}
	
	// set the camera as a child of the parent. On restart set the camera to the correct position/rotation right away
	public void setPlayerAsParent(bool fromRestart)
	{
		playerController = PlayerController.instance;
		thisTransform.parent = playerController.transform;
		if (fromRestart) {
			thisTransform.localPosition = inGamePosition;
			thisTransform.localEulerAngles = inGameRotation;
		} else {
			targetPosition = inGamePosition;
			
			if (steadyHeight) {
				playerTransform = playerController.transform;
				
				// get a starting value of the platform distance so we know when the player is jumping
				RaycastHit hit;
				if (Physics.Raycast(playerTransform.position + Vector3.up, -playerTransform.up, out hit, Mathf.Infinity, platformLayer)) {
					startPlatformDistance = hit.distance;
				}
			}
		}
		underRopeQuaternionRotation.eulerAngles = underRopeRotation;
		targetRotation.eulerAngles = inGameRotation;
		//print("Here1");
		enabled = true;
		GameManager.instance.onPauseGame += gamePaused;
		isShaking = false;
	}
	
	public void gameOver(GameOverType gameOverType)
	{
		thisTransform.parent = null;
		if (isShaking && gameOverType != GameOverType.Quit) {// let the shaking finish 
			targetRotation = thisTransform.rotation;
		} else {
			enabled = false;
		}
		/*if (gameOverType == GameOverType.Pit) 
		{
			transform.localPosition = new Vector3(transform.localPosition.x,transform.localPosition.y,transform.localPosition.z + 3);	
		}*/
		GameManager.instance.onPauseGame -= gamePaused;	
		isGlider = false;
		isRopeOn = false;
		isJumping = false;
	}

	public IEnumerator CameraTranslationAfterPit()
	{

		while (transform.localPosition.z < -2) 
		{
			transform.localPosition = transform.localPosition + new Vector3 (0, 0, 0.2f);
			yield return null;
		}
		isRopeOn = false;
	}

	public void CameraTranslationAfterUsingSaveMeInPit()
	{
		if(transform.localPosition.z > -3)
			transform.localPosition = inGamePosition;
	}
	
	
	public void LateUpdate()
	{
		bool continueUpdate = false;
		if (isShaking) {
						continueUpdate = true;
						Vector3 rotation = targetRotation.eulerAngles;
						rotation.Set (rotation.x + Random.Range (-currentShakeIntensity, currentShakeIntensity),
			             rotation.y + Random.Range (-currentShakeIntensity, currentShakeIntensity),
			             rotation.z + Random.Range (-currentShakeIntensity, currentShakeIntensity));
						currentShakeIntensity -= shakeDelta * Time.deltaTime; // slowly wind down
						thisTransform.localEulerAngles = rotation;
						if (Time.time - shakeStartTime > shakeDuration) {
								isShaking = false; // will end on the next update loop
								if (thisTransform.parent == null) { // the game has ended while the camera was shaking. disable the camera now
										enabled = false;
										continueUpdate = false;
								}
						}
				} else if (isJumping) {
						if ((thisTransform.localPosition - positionDuringjump).sqrMagnitude > 0.01f) {
								continueUpdate = true;
								thisTransform.localPosition = Vector3.MoveTowards (thisTransform.localPosition, positionDuringjump, 15f * Time.deltaTime);
						}
				} else  if (isGlider) {
						if ((thisTransform.localPosition - gliderPosition).sqrMagnitude > 0.01f) {
								continueUpdate = true;
								thisTransform.localPosition = Vector3.MoveTowards (thisTransform.localPosition, gliderPosition, 5 * Time.deltaTime);
						}
				} else  if (isCameraUnderBridge) {
						if ((thisTransform.localPosition - underBridgePosition).sqrMagnitude > 0.01f) {
								continueUpdate = true;
								thisTransform.localPosition = Vector3.MoveTowards (thisTransform.localPosition, underBridgePosition, 5 * Time.deltaTime);
						}
				} else if (isRopeOn) {
					if ((thisTransform.localPosition - underRopePosition).sqrMagnitude > 0.01f) {
						continueUpdate = true;
						thisTransform.localPosition = Vector3.MoveTowards(thisTransform.localPosition, underRopePosition, 15 * Time.deltaTime);
					}
					if (Quaternion.Angle(thisTransform.localRotation, underRopeQuaternionRotation) > 0.01f) {
						continueUpdate = true;
						thisTransform.localRotation = Quaternion.RotateTowards(thisTransform.localRotation, underRopeQuaternionRotation, 50 * Time.deltaTime);
					}
				}
				else {
					if ((thisTransform.localPosition - targetPosition).sqrMagnitude > 0.01f) {
						continueUpdate = true;
						thisTransform.localPosition = Vector3.MoveTowards(thisTransform.localPosition, targetPosition, moveSpeed * Time.deltaTime);
					}
					if (Quaternion.Angle(thisTransform.localRotation, targetRotation) > 0.01f) {
						continueUpdate = true;
						thisTransform.localRotation = Quaternion.RotateTowards(thisTransform.localRotation, targetRotation, rotationSpeed * Time.deltaTime);
					}
		}
		
		if (steadyHeight && !continueUpdate) {
			if (playerController.inAir()) {
				RaycastHit hit;
				if (Physics.Raycast(playerTransform.position + Vector3.up, -playerTransform.up, out hit, Mathf.Infinity, platformLayer)) {
					targetPosition.y = inGamePosition.y - (hit.distance - startPlatformDistance);
					thisTransform.localPosition = targetPosition;
				}
			}
		} else {
			/*if(!continueUpdate)
            {
                MyGUIManager.instance.SetIntroVideo(false);
            }*/
			enabled = continueUpdate;
			
		}
	}
	
	public void shake()
	{
		if (isShaking || shakeIntensity == 0 || shakeDuration == 0)
			return;
		
		if (!enabled) {
			//print("Here1");
			enabled = true;
		}
		isShaking = true;
		//shakeStartTime = Time.time;
		//currentShakeIntensity = shakeIntensity;
		//shakeDelta = shakeIntensity / shakeDuration;
	}
	
	public void reset()
	{
		thisTransform.position = startPosition;
		thisTransform.rotation = startRotation;
		isCameraUnderBridge = false;
		targetPosition.x = 0;
		isGlider = false;
		isJumping = false;
	}
	
	public void gamePaused(bool paused)
	{
		if (paused) {
			//print("mayank");
			pauseTime = Time.time;
			enabled = false;
		} else {
			//print("mayank");
			if (isShaking) {
				shakeStartTime += (Time.time - pauseTime);
			}
			
			if(isFromGameOver == false)
			{
				enabled = true;
				//print("Here1");
			}
		}
	}
	
	public void CameraUnderBridge(bool temp)
	{
		//print("Here1");
		underBridgePosition.x = targetPosition.x;
		isCameraUnderBridge = false;
		enabled = true;
	}
	
	public void IsGliderOn(bool temp)
	{
		//print("Here1");
		gliderPosition.x = targetPosition.x;
		isGlider = temp;
		//isCameraUnderBridge = false;
		enabled = true;
	}

	public void IsRopeOn(bool temp)
	{
		underRopePosition.x = targetPosition.x;
		isRopeOn = temp;
		//isCameraUnderBridge = false;
		enabled = true;
	}
	
	public void PlayerJumping(bool temp)
	{
		//print("Here1");
		isJumping = temp;
		positionDuringjump.x = targetPosition.x; 
		enabled = true;
	}
	
	public void IsFromGameOver(bool temp)
	{
		isFromGameOver = temp;
	}
	
	public void UpdateTargetPosition(SlotPosition currentSlotPosition)
	{
		//print("Here1");
		enabled = true;
		moveSpeed = 2;
		if(currentSlotPosition == SlotPosition.Left)
		{
			targetPosition.x = 1.2f;
			gliderPosition.x = targetPosition.x;
			positionDuringjump.x = targetPosition.x;
			underBridgePosition.x = targetPosition.x;
		}
		if(currentSlotPosition == SlotPosition.Right)
		{
			targetPosition.x = -1.2f;
			gliderPosition.x = targetPosition.x;
			positionDuringjump.x = targetPosition.x;
			underBridgePosition.x = targetPosition.x;
		}
		if(currentSlotPosition == SlotPosition.Center)    
		{
			targetPosition.x = 0f;
			gliderPosition.x = targetPosition.x;
			positionDuringjump.x = targetPosition.x;
			underBridgePosition.x = targetPosition.x;
		}
	}
}