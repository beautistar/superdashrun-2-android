using UnityEngine;
using System.Collections;

/*
 * The power up manager is a singleton which manages the state of the power ups. 
 */
//public enum PowerUpTypes {  CoinMagnet , DoubleCoin , SpeedIncrease , Invincibility , SuperPogoStick , SixMultiplier , BoltPack, SecrecyBox,  None };
public enum PowerUpTypes {  SuperPogoStick , BoltPack , CoinMagnet , DoubleCoin , Invincibility , SixMultiplier , SpeedIncrease, SecrecyBox,  None };

//public enum PowerUpTypes {  Scooter, CoinMagnet, Car, SuperBike , DoubleCoin , SpeedIncrease , Invincibility , None };
public class PowerUpManager : MonoBehaviour {
	
	static public PowerUpManager instance;
	
	private PowerUpTypes activePowerUp;
	private CoroutineData activePowerUpData;
	private PowerUpTypes activePowerUp1;
	private CoroutineData activePowerUpData1;
	
	private GameManager gameManager;
	private DataManager dataManager;
	private bool isActiveFromRestart;
	
	private PlayerController playerController;
	
	public void Awake(){
		instance = this;
	}

	public void Start(){
		isActiveFromRestart = false;
		gameManager = GameManager.instance;
		dataManager = DataManager.instance;
		gameManager.onPauseGame += gamePaused;	
		
		
		activePowerUp = PowerUpTypes.None;
		activePowerUpData = new CoroutineData();
		activePowerUp1 = PowerUpTypes.None;
		activePowerUpData1 = new CoroutineData();
	}
	
	public void reset(){
		if (activePowerUp != PowerUpTypes.None) {
			
			StopCoroutine("runPowerUp");
			deactivatePowerUp();	
		}
		if (activePowerUp1 != PowerUpTypes.None) {
			StopCoroutine("runPowerUp1");
			deactivatePowerUp1();	
		}
		IsContinueFromResume1 = false;
		IsContinueFromResume2 = false;
		isActiveFromRestart = false;
	}
	
	public bool isPowerUpActive(PowerUpTypes powerUpType)
	{
		//print("--------------activePowerUp"+activePowerUp);
		return ((activePowerUp == powerUpType) || isPowerUpActive1(powerUpType));
	}

	public PowerUpTypes getActivePowerUp() {
		return activePowerUp;
	}

	public PowerUpTypes[] getActivePowerUps() {
		return new PowerUpTypes[]{activePowerUp,activePowerUp1};
	}
	
	public void activatePowerUp(PowerUpTypes powerUpType){
		//print("--------------activePowerUp"+powerUpType);
		activePowerUp = powerUpType;
		activePowerUpData.duration = dataManager.getPowerUpLength(powerUpType);
		//Debug.Log ("2");
		StartCoroutine("runPowerUp");
	}
	
	private IEnumerator runPowerUp(){
		activePowerUpData.startTime = Time.time;
		if (activePowerUp == PowerUpTypes.BoltPack) {
			InfiniteObjectGenerator.instance.removeHorizon = -276;
			if (!IsContinueFromResume1)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData.duration, (int)PowerUpTypes.BoltPack);//glidar
			else {
				IsContinueFromResume1 = false;
				IngamePowerProgress.Instance.ResumeLoader1 (activePowerUpData.duration, dataManager.getPowerUpLength (PowerUpTypes.BoltPack));
			}
			
			yield return new WaitForSeconds ((activePowerUpData.duration) - 2f);
			playerController = PlayerController.instance;
			StartCoroutine (playerController.GladiatorHeightDecreasing ());
			InfiniteObjectGenerator.instance.removeHorizon = -96;
			
			yield return new WaitForSeconds (2f);
		} else if (activePowerUp == PowerUpTypes.SuperPogoStick) {
			yield return new WaitForSeconds ((activePowerUpData.duration) - 0.2f);
			playerController = PlayerController.instance;
			playerController.PogoStickLandAnimation ();
			yield return new WaitForSeconds (0.2f);
			//yield return new WaitForSeconds (2f);
		} else if (activePowerUp == PowerUpTypes.SpeedIncrease) {
			
			if (!IsContinueFromResume1)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData.duration, (int)PowerUpTypes.SpeedIncrease);  //Speed Increaser
			else {
				IsContinueFromResume1 = false;
				IngamePowerProgress.Instance.ResumeLoader1 (activePowerUpData.duration, dataManager.getPowerUpLength (PowerUpTypes.SpeedIncrease));
			}
			
			
			//print("ActivatePowerUp---"+activePowerUpData.duration);
			//print("Calculate--"+activePowerUpData.calcuateNewDuration());
			//IngamePowerProgress.Instance.StartLoader (activePowerUpData.duration,(int) PowerUpTypes.SpeedIncrease);  //Speed Increaser
			
			
			yield return new WaitForSeconds ((activePowerUpData.duration) - 2f);
			playerController = PlayerController.instance;
			playerController.SpeedDecreasing ();
			
			yield return new WaitForSeconds (2f);
		} else if (activePowerUp == PowerUpTypes.Invincibility) {
			if (isActiveFromRestart == false) {					
				if (!IsContinueFromResume1) 
					IngamePowerProgress.Instance.StartLoader (activePowerUpData.duration, (int)PowerUpTypes.Invincibility);  //Scooter
				else {
					IsContinueFromResume1 = false;
					IngamePowerProgress.Instance.ResumeLoader1 (activePowerUpData.duration, dataManager.getPowerUpLength (PowerUpTypes.Invincibility));
				}
				
				yield return new WaitForSeconds ((activePowerUpData.duration) - 2f);
				playerController = PlayerController.instance;
				playerController.SettingMyLayOut ();
				yield return new WaitForSeconds (2f);
			} else {
				playerController = PlayerController.instance;
				playerController.SettingMyLayOut ();
				yield return new WaitForSeconds (5f);
				isActiveFromRestart = false;
			}
			
		} else if (activePowerUp == PowerUpTypes.CoinMagnet) {
			if (!IsContinueFromResume1)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData.duration, (int)PowerUpTypes.CoinMagnet);
			else {
				IsContinueFromResume1 = false;
				IngamePowerProgress.Instance.ResumeLoader1 (activePowerUpData.duration, dataManager.getPowerUpLength (PowerUpTypes.CoinMagnet));
			}
			
			yield return new WaitForSeconds (activePowerUpData.duration);
		}
		else if(activePowerUp == PowerUpTypes.DoubleCoin ) {
			if(!IsContinueFromResume1)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData.duration,(int) PowerUpTypes.DoubleCoin);
			else {
				IsContinueFromResume1 = false;
				IngamePowerProgress.Instance.ResumeLoader1 (activePowerUpData.duration,dataManager.getPowerUpLength(PowerUpTypes.DoubleCoin));
			}
			
			yield return new WaitForSeconds(activePowerUpData.duration);
		}
		
		IsContinueFromResume1 = false;
		deactivatePowerUp();
	}
	
	public void deactivatePowerUp(){
		if (activePowerUp == PowerUpTypes.None)
			return;
		if (activePowerUp == PowerUpTypes.Invincibility || activePowerUp == PowerUpTypes.BoltPack || activePowerUp == PowerUpTypes.SuperPogoStick)
		{
			//Debug.Log ("deactivatePowerUp");
			playerController = PlayerController.instance;
			if(playerController != null)
				playerController.SettingMyLayOut();
		}    
		// Be sure the coroutine is stopped, deactivate may be called before runPowerUp is finished
		StopCoroutine("runPowerUp");
		gameManager.activatePowerUp(activePowerUp, false);
		activePowerUp = PowerUpTypes.None;
		activePowerUpData.duration = 0;
	}
	
	public bool isPowerUpActive1(PowerUpTypes powerUpType){
		//print("--------------activePowerUp"+activePowerUp);
		return activePowerUp1 == powerUpType;
	}
	
	public PowerUpTypes getActivePowerUp1() {
		return activePowerUp1;
	}
	
	public void activatePowerUp1(PowerUpTypes powerUpType)
	{
		//print("--------------activePowerUp"+powerUpType);
		activePowerUp1 = powerUpType;
		activePowerUpData1.duration = dataManager.getPowerUpLength(powerUpType);
		StartCoroutine("runPowerUp1");
	}
	
	private IEnumerator runPowerUp1()
	{
		activePowerUpData1.startTime = Time.time;
		if(activePowerUp1 == PowerUpTypes.BoltPack)
		{
			InfiniteObjectGenerator.instance.removeHorizon = -276;
			Debug.Log (IsContinueFromResume2);
			if(!IsContinueFromResume2)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData1.duration,(int) PowerUpTypes.BoltPack); //glidar
			else {
				IsContinueFromResume2 = false;
				IngamePowerProgress.Instance.ResumeLoader2 (activePowerUpData1.duration,dataManager.getPowerUpLength(PowerUpTypes.BoltPack));
			}
			
			yield return new WaitForSeconds((activePowerUpData1.duration)-2f);
			playerController = PlayerController.instance;
			StartCoroutine(playerController.GladiatorHeightDecreasing());
			InfiniteObjectGenerator.instance.removeHorizon = -96;
			
			yield return new WaitForSeconds(2f);
		}
		else if(activePowerUp1 == PowerUpTypes.SuperPogoStick)
		{
			yield return new WaitForSeconds((activePowerUpData1.duration)-0.2f);
			playerController = PlayerController.instance;
			playerController.PogoStickLandAnimation();
			yield return new WaitForSeconds(0.2f);
			//yield return new WaitForSeconds (2f);
		}
		else if(activePowerUp1 == PowerUpTypes.SpeedIncrease)
		{
			
			if(!IsContinueFromResume2)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData1.duration,(int) PowerUpTypes.SpeedIncrease);  //Speed Increaser
			else {
				IsContinueFromResume2 = false;
				IngamePowerProgress.Instance.ResumeLoader2 (activePowerUpData1.duration,dataManager.getPowerUpLength(PowerUpTypes.SpeedIncrease));
			}
			
			//IngamePowerProgress.Instance.StartLoader (activePowerUpData1.duration,(int) PowerUpTypes.SpeedIncrease);  //Speed Increaser
			
			yield return new WaitForSeconds((activePowerUpData1.duration)-2f);
			playerController = PlayerController.instance;
			playerController.SpeedDecreasing();
			
			yield return new WaitForSeconds(2f);
		}		
		else if(activePowerUp1 == PowerUpTypes.Invincibility)
		{
			if(isActiveFromRestart == false)
			{
				if(!IsContinueFromResume2)
					IngamePowerProgress.Instance.StartLoader (activePowerUpData1.duration,(int) PowerUpTypes.Invincibility);  //Scooter
				else {
					IsContinueFromResume2 = false;
					IngamePowerProgress.Instance.ResumeLoader2 (activePowerUpData1.duration,dataManager.getPowerUpLength(PowerUpTypes.Invincibility));
				}
				//IngamePowerProgress.Instance.StartLoader (activePowerUpData.duration,(int) PowerUpTypes.Invincibility);  //Scooter						
				yield return new WaitForSeconds((activePowerUpData1.duration)-2f);
				playerController = PlayerController.instance;
				playerController.SettingMyLayOut();
				yield return new WaitForSeconds(2f);
			}
			else
			{
				/*if(!IsContinueFromResume)
							IngamePowerProgress.Instance.StartLoader (activePowerUpData1.duration,(int) PowerUpTypes.Invincibility);  //Scooter
						else {
							IsContinueFromResume = false;
					IngamePowerProgress.Instance.ResumeLoader2 (activePowerUpData.duration,dataManager.getPowerUpLength(PowerUpTypes.Invincibility));
							}*/		
				playerController = PlayerController.instance;
				playerController.SettingMyLayOut();
				yield return new WaitForSeconds(5f);
				isActiveFromRestart = false;
			}
		}
		else if (activePowerUp1 == PowerUpTypes.CoinMagnet) {
			if (!IsContinueFromResume2)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData1.duration, (int)PowerUpTypes.CoinMagnet);
			else {
				IsContinueFromResume2 = false;
				IngamePowerProgress.Instance.ResumeLoader2 (activePowerUpData1.duration, dataManager.getPowerUpLength (PowerUpTypes.CoinMagnet));
			}
			
			yield return new WaitForSeconds (activePowerUpData1.duration);
		}
		else if(activePowerUp1 == PowerUpTypes.DoubleCoin ) {
			if(!IsContinueFromResume2)
				IngamePowerProgress.Instance.StartLoader (activePowerUpData1.duration,(int) PowerUpTypes.DoubleCoin);
			else {
				IsContinueFromResume2 = false;
				IngamePowerProgress.Instance.ResumeLoader2 (activePowerUpData1.duration,dataManager.getPowerUpLength(PowerUpTypes.DoubleCoin));
			}
			
			yield return new WaitForSeconds(activePowerUpData1.duration);
		}
		
		deactivatePowerUp1();
		IsContinueFromResume2 = false;
	}
	
	public void deactivatePowerUp1()
	{
		if (activePowerUp1 == PowerUpTypes.None)
			return;
		if (activePowerUp1 == PowerUpTypes.Invincibility || activePowerUp1 == PowerUpTypes.BoltPack || activePowerUp1 == PowerUpTypes.SuperPogoStick)
		{
			playerController = PlayerController.instance;
			if(playerController != null)
				playerController.SettingMyLayOut();
		}    
		// Be sure the coroutine is stopped, deactivate may be called before runPowerUp is finished
		StopCoroutine("runPowerUp1");
		gameManager.activatePowerUp(activePowerUp1, false);
		activePowerUp1 = PowerUpTypes.None;
		activePowerUpData1.duration = 0;
	}
	bool IsContinueFromResume1 = false;
	bool IsContinueFromResume2 = false;
	
	private void gamePaused(bool paused)
	{
		if (activePowerUp != PowerUpTypes.None) {
			if (paused) {
				StopCoroutine("runPowerUp");
				IsContinueFromResume1 = true;
				activePowerUpData.calcuateNewDuration();
			} else {
				if(GameManager.instance.IsGameRunnnig)
					StartCoroutine("runPowerUp");
				//StartCoroutine("runPowerUp1");
			}
		}
		if (activePowerUp1 != PowerUpTypes.None) {
			if (paused) {
				//StopCoroutine("runPowerUp");
				StopCoroutine("runPowerUp1");
				IsContinueFromResume2 = true;
				activePowerUpData1.calcuateNewDuration();
			} else {
				//StartCoroutine("runPowerUp");
				if(GameManager.instance.IsGameRunnnig)
					StartCoroutine("runPowerUp1");
			}
		}
	}
	
	public void IsActiveFromRestart1()
	{
		isActiveFromRestart = true;
	}

	public bool GetIsActiveFromRestart1()
	{
		return isActiveFromRestart;
	}

	
	public float BoltPackDuration()
	{
		if(activePowerUp == PowerUpTypes.BoltPack)
			return (activePowerUpData.duration * activePowerUpData.duration);
		else
			return  (activePowerUpData1.duration * activePowerUpData1.duration);
	}
	
	public bool numberOfPowerUpActive()
	{
		if(activePowerUp1 != PowerUpTypes.None && activePowerUp != PowerUpTypes.None)
			return true;
		else 
			return false;	
	}
}
