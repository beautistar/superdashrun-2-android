using UnityEngine;
using System.Collections;

/**
 * After each run gameOver() will be called and the MissionManager will check the active missions to determine if they have been satisifed. If they are satisfied, the scoreMultiplier
 * is incremented by scoreMultiplierIncrement and that value is multiplied by the points to give you your final score.
 * 
 * ID                       Description
 * NoviceRunner             run for 500 points
 * CompetentRunner          run for 1500 points
 * ExpertRunner             run for 5000 points
 * RunnerComplete           running complete
 * NoviceCoinCollector      collect 50 coins
 * CompetentCoinCollector   collect 150 coins
 * ExpertCoinCollector      collect 500 coins
 * CoinCollectorComplete    coin collector complete
 * NovicePlayCount          play 5 games
 * CompetentPlayCount       play 15 games
 * ExpertPlayCount          play 50 games
 * PlayCountComplete        play count complete
 **/
public enum MissionType { Got_Away, Out_Run, No_Funds, Save_A_Life,Feeling_Breezy,Bank_Deposit,Bag_Of_Coins,Half_Million_VIP, Coin_Stash, Olympic_Runner, VIP_Club, VIP_owner_Club, Ahead_of_the_Game, Gold_Rush, Out_of_Sight, Magnet_Grabber, Fly_Away, Got_The_Hops, No_Secrets,Heap_of_Coins,  
	Loose_Change, Savings_Account, I_See_U, Pay_Day,Coin_Allergies, Quarter_Million_VIP, LOL_I_got_Away, Blast_Off,Stop_Tripping, Ready_Set_Go, Running_Rich, Five_Million_Club, Royalty_Crown_Club, Retirement_Awaits, Awesome_Run, Scooter_Cruiser, Eagle_Soaring, No_Gravity, Jumper, Why_So_Serious,  
	Not_So_Bad, Escaper, Missing_Out, Revive, Running_Free, Life_Savers,Dont_Trip, Double_Life_Support, Cat_Living, Bank_Roll, IRS_Collector, Living_Well, Elusive_Running, Cannot_Be_Caught, Tax_Collector, Cruising_The_Streets, Hip_Hop, Bunny_Rabbit,Flying_High, None }
public class MissionManager : MonoBehaviour {
	
	static public MissionManager instance;
	
	// callback for any class that is interested when a mission is complete (such as the social manager)
	public delegate void MissionCompleteHandler(MissionType missionType);
	public event MissionCompleteHandler onMissionComplete;
	
	// The amount the score should be multiplied by each time a challenge is complete
	
	public float scoreMultiplierIncrement;
	
	private MissionType[] activeMissions;
	private int[] activeMissionsStatus;
	private float scoreMultiplier;
	private int assignmentLevel;
	private int levelHeapOfCoin;
	private DataManager dataManager;
	
	public void Awake()
	{
		Debug.Log("Mission manager: " + name);
		instance = this;
	}
	
	public void Start()
	{
		dataManager = DataManager.instance;
		
		levelHeapOfCoin = 0;
		activeMissions = new MissionType[3]; // 3 active missions at a time
		activeMissionsStatus = new int[3];
		scoreMultiplier = 1;
		for (int i = 0; i < activeMissions.Length; ++i) {
			activeMissions[i] = (MissionType)PlayerPrefs.GetInt(string.Format("Mission{0}", i), -1);
			activeMissionsStatus[i] = (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", i), -1);
			//Debug.Log ("MissionStatus :"+ PlayerPrefs.GetInt(string.Format("MissionStatus{0}", i), -1));
			// there are no active missions if the game hasn't been started yet
			if ((int)activeMissions[i] == -1) {
				activeMissions[i] = (MissionType)(i * 20); // 4 missions per set
			}
			//scoreMultiplier += activeMissions[i]
			//scoreMultiplier += ((int)activeMissions[i] % 20) * scoreMultiplierIncrement;
		}
		assignmentLevel = (int)activeMissions[0];
		//scoreMultiplier += ((int)activeMissions[0]*activeMissions.Length)*scoreMultiplierIncrement;
		scoreMultiplier = assignmentLevel;
	}
	
	public void gameOver()
	{
		//checkForCompletedMissions();
	}

	void Update() {

		if (!MyGUIManager.GameStarted)
			return;

		checkForCompletedMissions ();
	}
	
	// loop through the active missions and determine if the previous run satisfied the mission requirements
	private void checkForCompletedMissions()
	{
		//Debug.Log("===>> Check completed missions...");
		for (int i = 0; i < activeMissions.Length; ++i) {
			switch (activeMissions[i]) {
			case MissionType.Got_Away:  
			case MissionType.Out_Run:
			case MissionType.Running_Free:
			case MissionType.LOL_I_got_Away:
			case MissionType.Ready_Set_Go:
			case MissionType.Awesome_Run:
				if (dataManager.getLongestRunFromGame() >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1) {
						missionComplete(activeMissions[i]);
					} 
				}
				break;
				
			case MissionType.Not_So_Bad:
			case MissionType.Escaper:
			case MissionType.I_See_U:
			case MissionType.Quarter_Million_VIP:
			case MissionType.Half_Million_VIP:
			case MissionType.VIP_Club:
			case MissionType.VIP_owner_Club:
			case MissionType.Five_Million_Club:
			case MissionType.Royalty_Crown_Club:
				if (dataManager.getScore(false) >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1) {
						missionComplete(activeMissions[i]);
					}
				}
				break;
				
			case MissionType.Loose_Change:
			case MissionType.Savings_Account:
			case MissionType.Pay_Day:
			case MissionType.Bank_Deposit:
			case MissionType.Bank_Roll:
			case MissionType.Bag_Of_Coins:
			case MissionType.Coin_Stash:
			case MissionType.IRS_Collector:
			case MissionType.Tax_Collector:
				if (dataManager.getLevelCoins() >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1) {
						missionComplete(activeMissions[i]);
					}
				}
				break;
				
				
			case MissionType.No_Funds:
			case MissionType.Missing_Out: 
			case MissionType.Coin_Allergies:
				if (dataManager.getLongestRunWithoutCoinFromGame() >= dataManager.getMissionGoal(activeMissions[i])) {
					//print("LooseChange");
					if(activeMissionsStatus[i] != 1) {
						missionComplete(activeMissions[i]);
					}
				}
				break;   

			case MissionType.Olympic_Runner:
			case MissionType.Elusive_Running:
			case MissionType.Cannot_Be_Caught:
			case MissionType.Out_of_Sight:
				if (dataManager.getTotalDistance() >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)  {
						missionComplete(activeMissions[i]);
					}
				}
				break;
				
			case MissionType.Running_Rich:
			case MissionType.Gold_Rush:
			case MissionType.Retirement_Awaits:   
				if (dataManager.getTotalCoinsLifetime() >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1) {
						missionComplete(activeMissions[i]);
					}
				}
				break;
				
			case MissionType.Magnet_Grabber:
				if (dataManager.GetLevelPowerUpCount(PowerUpTypes.CoinMagnet) >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1) {
						missionComplete(activeMissions[i]);
					}
				}
				break;
				
			case MissionType.Hip_Hop: 
			case MissionType.Got_The_Hops:
			case MissionType.Bunny_Rabbit:
				if (dataManager.GetLevelPowerUpCount(PowerUpTypes.SuperPogoStick) >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break; 
			case MissionType.Fly_Away: 
			case MissionType.No_Gravity:
			case MissionType.Flying_High:
				if (dataManager.GetLevelPowerUpCount(PowerUpTypes.BoltPack) >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break; 
			case MissionType.Scooter_Cruiser: 
			case MissionType.Cruising_The_Streets:
				if (dataManager.GetLevelPowerUpCount(PowerUpTypes.Invincibility) >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break; 
			case MissionType.Blast_Off: 
			case MissionType.Ahead_of_the_Game:           
				if (dataManager.GetLevelPowerUpCount(PowerUpTypes.SpeedIncrease) >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break;
			case MissionType.Save_A_Life: 
			case MissionType.Life_Savers:
			case MissionType.Living_Well:                
				if (dataManager.getLifeSaversFromGame() >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break;
			case MissionType.Jumper:                
				if (dataManager.GetNumberOfJumps() >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break;
			case MissionType.Why_So_Serious:                
				if (dataManager.getCharacterCost(1) == -1) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break;  
			case MissionType.Feeling_Breezy:                
				if (dataManager.getCharacterCost(5) == -1) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break;
			case MissionType.Heap_of_Coins:                //52
				if (levelHeapOfCoin == 1) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break;
			case MissionType.Revive: 
			case MissionType.Double_Life_Support:
			case MissionType.Cat_Living:                
				if (dataManager.GetNumberOfLifeSaverUsed() >= dataManager.getMissionGoal(activeMissions[i])) {
					if(activeMissionsStatus[i] != 1)
						missionComplete(activeMissions[i]);
				}
				break;
			case MissionType.Dont_Trip: 
                case MissionType.Stop_Tripping:               //57
                    if (dataManager.getLongestRunFromGameWithoutTrip() >= dataManager.getMissionGoal(activeMissions[i])) {
                        if(activeMissionsStatus[i] != 1)
                            missionComplete(activeMissions[i]);
                    }
                    break;	                                                  
				/*case MissionType.NotSoBad:
                case MissionType.Escaper:
                case MissionType.ISeeU:
                    // play count doesn't get incremented until after checkForCompletedMissions is called 
                    if (dataManager.getPlayCount() + 1 >= dataManager.getMissionGoal(activeMissions[i])) {
                        missionComplete(activeMissions[i]);
                    }
                    break;*/
			}   
		}
	}
	
	private void missionComplete(MissionType missionType)
	{
//		GameService.instance.UnlockAchievement (missionType);
		if(assignmentLevel <= 19 )
		{
			if((int)missionType < 20)
			{
				PlayerPrefs.SetInt(string.Format("Mission{0}", 0), (int)missionType);
				PlayerPrefs.SetInt(string.Format("MissionStatus{0}", 0), 1);
				activeMissions[0] = (MissionType)PlayerPrefs.GetInt(string.Format("Mission{0}", 0), -1);
				activeMissionsStatus[0] = (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", 0), -1);
				InGameAlertsManager.Instance.ShowAlert (SetSpace((int)missionType), DataManager.instance.getMissionDescription (missionType),true);
			}
			else if((int)missionType < 40)
			{
				PlayerPrefs.SetInt(string.Format("Mission{0}", 1), (int)missionType);
				PlayerPrefs.SetInt(string.Format("MissionStatus{0}", 1), 1);
				activeMissions[1] = (MissionType)PlayerPrefs.GetInt(string.Format("Mission{0}", 1), -1);
				activeMissionsStatus[1] = (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", 1), -1);
				InGameAlertsManager.Instance.ShowAlert (SetSpace((int)missionType), DataManager.instance.getMissionDescription (missionType),true);
			}
			else if((int)missionType < 60)
			{
				PlayerPrefs.SetInt(string.Format("Mission{0}", 2), (int)missionType);
				PlayerPrefs.SetInt(string.Format("MissionStatus{0}", 2), 1);
				activeMissions[2] = (MissionType)PlayerPrefs.GetInt(string.Format("Mission{0}", 2), -1);
				activeMissionsStatus[2] = (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", 2), -1);
				InGameAlertsManager.Instance.ShowAlert (SetSpace((int)missionType), DataManager.instance.getMissionDescription (missionType),true);
			}



			int temp =  (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", 0), -1) + (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", 1), -1) + (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", 2), -1);

            Debug.LogWarning("=======>>Temp: " + temp);
            if (temp == activeMissions.Length)
			{
				assignmentLevel++;
				if(assignmentLevel<=18)
				{
					//assignmentLevel++;
					for(int i=0;i<activeMissions.Length;i++)
					{
						PlayerPrefs.SetInt(string.Format("Mission{0}", i), ((assignmentLevel)+(20*i)));
						PlayerPrefs.SetInt(string.Format("MissionStatus{0}", i), 0);
						activeMissions[i] = (MissionType)PlayerPrefs.GetInt(string.Format("Mission{0}", i), -1);
						activeMissionsStatus[i] = (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", i), -1);
					}
				}
			}              
		}                
		/*int missionSet = (int)missionType / 20;
        if ((int)missionType < 20 && ((int)missionType - 3) % 20 != 0) { // don't increment if the player has already reached the max
            activeMissions[missionSet] = missionType + 1;
            scoreMultiplier += scoreMultiplierIncrement;
            PlayerPrefs.SetInt(string.Format("Mission{0}", missionSet), (int)activeMissions[missionSet]);
        }*/
		
		if (onMissionComplete != null) {
			onMissionComplete(missionType);
		}
	}

	string SetSpace(int Value) {
		string str = ((MissionType) Value).ToString ();
		str = str.Replace ("_", " ");
		return str;
	}
	
	public float getScoreMultiplier()
	{
		return (scoreMultiplier+1);
	}
	
	public MissionType getMission(int mission)
	{
		return activeMissions[mission];
	}
	
	public int GetAssignmentLevel()
	{
		return assignmentLevel;
	}
	
	public int AssignmentStatus(int MissionNo) {

		return (int)PlayerPrefs.GetInt(string.Format("MissionStatus{0}", MissionNo), -1);
	}
	
	public void HeapofCoinsPurchased()
	{
		levelHeapOfCoin = 1;
		checkForCompletedMissions();
		levelHeapOfCoin = 0;
	}
}