using UnityEngine;
using System.Collections;

/*
 * Static data is a singleton class which holds any data which is not directly game related, such as the power up length and cost
 */
public class StaticData : MonoBehaviour {

	static public StaticData instance;

    public int totalPowerUpLevels;
    public string[] powerUpTitle;
    public string[] powerUpDescription;
    public GameObject[] powerUpPrefab;
	public float[] powerUpLength;
    public int[] powerUpCost;

    public int PermanentPurchasesCount;
    public string[] PermanentPurchasesTitle;
    public string[] PermanentPurchasesDescription;
    public int[] PermanentPurchasesCost;

    public int SingleUseCount;
    public int[] SingleUseCost;
    public string[] SingleUseTitle;
    public string SingleUseDescription;
    public string[] SingleUseFullDescription;

    public int characterCount;
    public string[] characterTitle;
    public string[] characterDescription;
    public int[] characterCost;
    public GameObject[] characterPrefab;

    public string[] missionDescription;
    public int[] missionGoal;

    public GameObject chaseObjectPrefab;

    ChaseCharacterPrefab chaseCharacterPrefab;
	
	public void Awake()
	{
		instance = this;	
	}

    public string getPowerUpTitle(PowerUpTypes powerUpType)
    {
        return powerUpTitle[(int)powerUpType];
    }

    public string getPowerUpDescription(PowerUpTypes powerUpType)
    {
        return powerUpDescription[(int)powerUpType];
    }

    public GameObject getPowerUpPrefab(PowerUpTypes powerUpType)
    {
        return powerUpPrefab[(int)powerUpType];
    }
	
	public float getPowerUpLength(PowerUpTypes powerUpType, int level)
	{
        if((int)powerUpType >= 4)
        {
           return powerUpLength[((int)powerUpType * (totalPowerUpLevels+1)) + 1];
        }
        else    
            return powerUpLength[((int)powerUpType * (totalPowerUpLevels + 1)) + level];
	}
	
	public int getPowerUpCost(PowerUpTypes powerUpType, int level)
	{
		return powerUpCost[((int)powerUpType * totalPowerUpLevels) + level];
	}

	public int getPowerUpCost(PowerUpTypes powerUpType, int level, bool SingleUse)
	{
		return powerUpCost[((int)powerUpType * totalPowerUpLevels)];
	}
	
	public int getTotalPowerUpLevels()
	{
		return totalPowerUpLevels;
	}

    public string getCharacterTitle(int character)
    {
        return characterTitle[character];
    }

    public string getCharacterDescription(int character)
    {
        return characterDescription[character];
    }

    public int getCharacterCost(int character)
    {
        return characterCost[character];
    }

    public GameObject getCharacterPrefab(int character)
    {
        return characterPrefab[character];
    }

    public string getMissionDescription(MissionType missionType)
    {
        return missionDescription[(int)missionType];
    }

    public int getMissionGoal(MissionType missionType)
    {
        return missionGoal[(int)missionType];
    }

    public GameObject getChaseObjectPrefab()
    {
        chaseCharacterPrefab = GetComponent<ChaseCharacterPrefab>();
        chaseObjectPrefab = chaseCharacterPrefab.ChasingCharacter();
        return chaseObjectPrefab;
    }

    //--------------------------------------------------------------------------------------------------------------------------

    public string getPurchaseTitle(int Order)
    {
        return PermanentPurchasesTitle[Order];
    }
    
    public string getPurchaseDescription(int Order)
    {
        return PermanentPurchasesDescription[Order];
    }
    
    public int getPurchaseUseCost(int Order)
    {
        return PermanentPurchasesCost[Order];
    }

    public string getSingleUseTitle(int Order)
    {
        return SingleUseTitle[Order];
    }
    
    public string getSingleUseFullDescription(int Order)
    {
        return SingleUseFullDescription[Order];
    }

    public string getSingleUseDescription()
    {
        return SingleUseDescription;
    }
    
	public int getSingleUseCost(PowerUpTypes powerUpType)
    {
		return powerUpCost[(int)powerUpType * totalPowerUpLevels ];
        //return SingleUseCost[Order];
    }

    //-----------------------------------------------------------------------------------
}
