﻿using UnityEngine;
using System.Collections;

public class BankInit : MonoBehaviour{
	void Start()
	{
		BankManager.Instance.SetCoinsItems ();
		BankManager.Instance.SetLifeSaverItems ();
	}
}
