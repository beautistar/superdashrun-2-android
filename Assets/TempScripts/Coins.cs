﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour 
{
private DataManager dataManager;
GUIText gui;
	// Use this for initialization
	void Start () 
	{
		dataManager = DataManager.instance;
		gui = GetComponent<GUIText>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnGUI()
	{
		gui.text = "Score "+ dataManager.getScore().ToString() + "\n Coins"+dataManager.getLevelCoins().ToString();
	}
}
