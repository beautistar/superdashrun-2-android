﻿using UnityEngine;
using System.Collections;

public class DeletePrfs : MonoBehaviour {

	// Use this for initialization
	void Start () {
		PlayerPrefs.DeleteAll ();
		PlayerPrefs.SetInt("Coins", 1000000000);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
