﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using SA.IOSNative.StoreKit;
using System.Collections.Generic;

public class BankManager : MonoBehaviour {

	public static BankManager Instance;
	public BankbtnProperties[] CoinsItems;
	public BankbtnProperties[] LifeSaverItems;
	public GameObject Buy;
	public GameObject BuyRestore;
	public GameObject BuyLifeSaver;
	public Transform ParentTransform;

	public int InitialY = 3;
	DataManager dataManager;
	BankStaticData bankStaticData;

	int CurrentOrder = 0;
	int CoinLifesaverOrder = 0;

	bool IsURL = false;


	public const string SKU_Nopopad    = "No Pop Up Ads";
	public const string SKU_doublecoin = "Double Coins";
	public const string SKU_heapofcoin = "Heap of Coins";
	public const string SKU_binofcoin = "Bin of Coins";
	public const string SKU_lumpofcoin = "Lump of Coins";
	public const string SKU_pileofcoin = "Pile of Coins";
	public const string SKU_sackofcoin = "Sack of Coins";
	public const string SKU_chestofcoin 	 = "Chest of Coins";
	public const string SKU_vaultofcoin 	 = "Vault of Coins";

	public const string SKU_spareme 	 = "Spare Me";
	public const string SKU_pileoflifesavers = "Pile of Diamonds";
	public const string SKU_stackoflifesavers 	 = "Stack of Diamonds";
	public const string SKU_sackoflifesavers	 = "Sack of Diamonds";

	bool bInit = false;

	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus == true && IsURL) {
			UIGameManager.Instance.HideWall ();
			IsURL = false;
		}
	}

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {

		if (!bInit) {
			Init ();
		}
	}

	public void Init()
	{
		PaymentManagerExample.init ();
		Updatemanager.instance.OnUpdateBank ();
		bankStaticData = BankStaticData.Instance;
		dataManager = DataManager.instance;
		CoinsItems = new BankbtnProperties[bankStaticData.getCoinsCount ()];
		LifeSaverItems = new BankbtnProperties[bankStaticData.getLifeSaverCount ()];

		bInit = true;
	}

	public void OnBuyClickCoin(int Order)
	{
		Debug.Log ("OnBuyClickCoin");

		#if UNITY_EDITOR
		PaymentComplete1(Order);			
		#else
			CoinLifesaverOrder = 0; 
			CurrentOrder = Order;
			CallPurchaseCoin (Order);
		#endif

		Debug.Log ("OnBuyClickCoin " + Order);
	}

	void CallPurchaseCoin(int Order) {

		switch(Order) {
		case 0 : PaymentManagerExample.BuyItem (SKU_Nopopad); break;
		case 1 : PaymentManagerExample.BuyItem (SKU_doublecoin); break;
		case 2 : PaymentManagerExample.BuyItem (SKU_heapofcoin); break;
		case 3 : PaymentManagerExample.BuyItem (SKU_lumpofcoin); break;
		case 4 : PaymentManagerExample.BuyItem (SKU_pileofcoin); break;
		case 5 : PaymentManagerExample.BuyItem (SKU_sackofcoin); break;
		case 6 : PaymentManagerExample.BuyItem (SKU_binofcoin); break;
		case 7 : PaymentManagerExample.BuyItem (SKU_chestofcoin); break;
		case 8 : PaymentManagerExample.BuyItem (SKU_vaultofcoin); break;
		}
	}

	public void OnBuyClickLifeSaver(int Order) {

		Debug.Log ("OnBuyClickLifeSaver");

		CoinLifesaverOrder = 1;
		CurrentOrder = Order;

		#if UNITY_EDITOR
		PaymentComplete2(Order);			
		#else
		CallPurchaseLifesaver (Order);
		#endif

		Debug.Log ("OnBuyClickLifeSaver " + Order);

	}

	public void CallPurchaseLifesaver(int Order) {
		switch(Order)
		{
		case 0 : PaymentManagerExample.BuyItem (SKU_spareme); break;
		case 1 : PaymentManagerExample.BuyItem (SKU_stackoflifesavers); break;
		case 2 : PaymentManagerExample.BuyItem (SKU_pileoflifesavers); break;
		case 3 : PaymentManagerExample.BuyItem (SKU_sackoflifesavers); break;
		}
	}

	public void SuccesfulPurchaseBankCoins(int Order) {
		if (Order == 0) {
			bankStaticData.setAds ();	
			CoinsItems[0].HideBuy ();
		}

		if (Order == 1) {
			bankStaticData.setDoubleCoins ();	
			CoinsItems[1].HideBuy ();
		}

		dataManager.adjustTotalCoins (bankStaticData.Coins [Order]);
		dataManager.setTotalCoinsLifetime (bankStaticData.Coins [Order]);
		Updatemanager.instance.OnUpdateBank ();

		if(Order == 2)    // Only For Heap Of Coin
			MissionManager.instance.HeapofCoinsPurchased();
	}


	public void SuccesfulPurchaseBankLifeSavers(int Order) {
		//dataManager.adjustTotalLifeSavers (bankStaticData.LifeSavers [Order]);
		dataManager.setTotalLifeSavers( bankStaticData.LifeSavers [Order]);
		Updatemanager.instance.OnUpdateBank ();
	}

	public void SetCoinsItems() {
		//		int index = UIGameManager.Instance.isAdsBuilt == true ? 0 : 1;
		for (int i=0; i<bankStaticData.getCoinsCount (); i++) {
			if(CoinsItems[i] == null ) {
				GameObject go;
				if(i == 0 || i == 1)
					go = (GameObject) Instantiate (BuyRestore);
				else
					go = (GameObject) Instantiate (Buy);
				go.transform.parent = ParentTransform;//this.transform;
				go.transform.localPosition = new Vector3(0,InitialY,0);
				CoinsItems[i] = go.GetComponent<BankbtnProperties>();

				if(i == 0 || i == 1) {
					CoinsItems[i].SetAdsRestore (i);
				}
				else  {
					CoinsItems[i].SetDetailsCoin (i);
				}

				InitialY -= 2;
			}
		}
	}

	public void SetLifeSaverItems() {
		for (int i=0; i<bankStaticData.getLifeSaverCount(); i++) {
			if(LifeSaverItems[i] == null ) {
				GameObject go = (GameObject) Instantiate (BuyLifeSaver);
				go.transform.parent = ParentTransform;//this.transform;
				go.transform.localPosition = new Vector3(0,InitialY,0);
				LifeSaverItems[i] = go.GetComponent<BankbtnProperties>();
				LifeSaverItems[i].SetDetailsLifeSaver (i);
				InitialY -= 2;
			}
		}
	}

	public void OnRestoreClick(){		
		UIGameManager.Instance.ShowWall ("Restoring InApps");
		PaymentManagerExample.OnRstorePurchase ();	
	}

	void PaymentComplete(string stProductID){
		switch (stProductID) {
		case SKU_Nopopad: SuccesfulPurchaseBankCoins (0);	break;
		case SKU_doublecoin: SuccesfulPurchaseBankCoins (1);	break;
		case SKU_heapofcoin: SuccesfulPurchaseBankCoins (2);	break;
		case SKU_lumpofcoin: SuccesfulPurchaseBankCoins (3);	break;
		case SKU_pileofcoin: SuccesfulPurchaseBankCoins (4);	break;
		case SKU_sackofcoin: SuccesfulPurchaseBankCoins (5);	break;
		case SKU_binofcoin: SuccesfulPurchaseBankCoins (6);	break;
		case SKU_chestofcoin: SuccesfulPurchaseBankCoins (7);	break;
		case SKU_vaultofcoin: SuccesfulPurchaseBankCoins (8);	break;

		case SKU_spareme: SuccesfulPurchaseBankLifeSavers (0);	break;
		case SKU_stackoflifesavers: SuccesfulPurchaseBankLifeSavers (1);	break;
		case SKU_pileoflifesavers: SuccesfulPurchaseBankLifeSavers (2);	break;
		case SKU_sackoflifesavers: SuccesfulPurchaseBankLifeSavers (3);	break;
		}
	}

	//Test
	void PaymentComplete1(int Order){
		switch(Order) {
		case 0 : PaymentComplete (SKU_Nopopad); break;
		case 1 : PaymentComplete (SKU_doublecoin); break;
		case 2 : PaymentComplete (SKU_heapofcoin); break;
		case 3 : PaymentComplete (SKU_lumpofcoin); break;
		case 4 : PaymentComplete (SKU_pileofcoin); break;
		case 5 : PaymentComplete (SKU_sackofcoin); break;
		case 6 : PaymentComplete (SKU_binofcoin); break;
		case 7 : PaymentComplete (SKU_chestofcoin); break;
		case 8 : PaymentComplete (SKU_vaultofcoin); break;
		}
	}

	//Test
	void PaymentComplete2(int Order){
		switch(Order) {
		case 0 : PaymentComplete (SKU_spareme); break;
		case 1 : PaymentComplete (SKU_stackoflifesavers); break;
		case 2 : PaymentComplete (SKU_pileoflifesavers); break;
		case 3 : PaymentComplete (SKU_sackoflifesavers); break;
		}
	}

	public void PaymentFailed(string error) {
		Debug.Log ("purchaseFailedEvent " + error);
		CurrentOrder = 0;
		UIGameManager.Instance.HideWall ();
	}

	public void UnlockProduct(string productIdentifier){
		PaymentComplete (productIdentifier);
	}
}


public class PaymentManagerExample{

	private static bool IsInitialized = false;

	public static void init(){
		if (!IsInitialized) {
			//if you already set in setting guid windows->stand_asset-Ios Native-> edit settings->billing Method
			// You don't have to add products

			//Event Use Example
			UM_InAppPurchaseManager.Instance.OnPurchaseFinished += OnUMPurchaseComplete;
			UM_InAppPurchaseManager.Instance.OnRestoreFinished += OnUMRestoreComplete;
			UM_InAppPurchaseManager.Instance.OnServiceConnected += OnServiceConnected;
			UM_InAppPurchaseManager.Instance.Connect ();

			IsInitialized = true;

			//PaymentManager.Instance.LoadStore();
		}
	}

	static void HandleOnVerificationComplete(VerificationResponse response){

		IOSNativePopUpManager.showMessage ("Verification", "Transaction verification status:" + response.Status.ToString ());

		ISN_Logger.Log ("ORIGINAL JSON" + response.OriginalJSON);
	}

	static void OnStoreKitInitComplete(SA.Common.Models.Result result){

		if(result.IsSucceeded) {

			int avaliableProductsCount = 0;
			foreach(Product tpl in  PaymentManager.Instance.Products) {
				if(tpl.IsAvailable) {
					avaliableProductsCount++;
				}
			}

			IOSNativePopUpManager.showMessage("StoreKit Init Succeeded", "Available products count: " + avaliableProductsCount);
			ISN_Logger.Log("StoreKit Init Succeeded Available products count: " + avaliableProductsCount);
		} else {
			IOSNativePopUpManager.showMessage("StoreKit Init Failed",  "Error code: " + result.Error.Code + "\n" + "Error description:" + result.Error.Message);
		}
	}

	static void OnRestoreComplete(RestoreResult result){
		if (result.IsSucceeded) {
			IOSNativePopUpManager.showMessage ("Success", "Restore Completed");
		} else {
			IOSNativePopUpManager.showMessage ("Error :" + result.Error.Code, result.Error.Message );
		}
	}

	static void OnTransactionComplete(PurchaseResult result){

		ISN_Logger.Log ("OnTransactionComplete: " + result.ProductIdentifier);
		ISN_Logger.Log ("OnTransactionComplete: state " + result.State);

		switch (result.State) {
		case PurchaseState.Purchased:
		case PurchaseState.Restored:

//			UnlockProducts (result.ProductIdentifier);
//			UnlockProducts (result);

			break;
		case PurchaseState.Deferred:
			break;
		case PurchaseState.Failed:

			ISN_Logger.Log ("Transaction failed with error, code: " + result.Error.Code);
			ISN_Logger.Log ("Transaction failed with error, description: " + result.Error.Message);
			break;
		}

		if (result.State == PurchaseState.Failed) {
			IOSNativePopUpManager.showMessage ("Transaction Failed", "Error code: " + result.Error.Code + "\n" + "Error description: " + result.Error.Message);
		} else {
			IOSNativePopUpManager.showMessage ("StoreKit Response", "product" + result.ProductIdentifier + " state: " + result.State.ToString ());
		}
	}

	static void OnUMPurchaseComplete(UM_PurchaseResult result){
		if (result.isSuccess) {
			UnlockProducts (result.product.id);
//			UM_ExampleStatusBar.text = "Product " + result.product.id + " purchase Success";
		} else {
			BankManager.Instance.PaymentFailed (result.product.id);
//			UM_ExampleStatusBar.text = "Product " + result.product.id + " purchase Fail";
		}
	}

	static void OnUMRestoreComplete(UM_BaseResult result){
		if (result.IsSucceeded) {
//			UM_ExampleStatusBar.text = "Restore Purchase Success";
		} else {
//			UM_ExampleStatusBar.text = "Restore Purchase Failed" + result.ToString ();;
		}
	}

	static void OnServiceConnected(UM_BillingConnectionResult result){
		if (result.isSuccess) {
//			UM_ExampleStatusBar.text = "Billing Init Success";
		} else {
//			UM_ExampleStatusBar.text = "Billing Init Failed" + result.message;
		}
	}

	public static void BuyItem(string productID){
		UM_InAppPurchaseManager.Instance.Purchase (productID);
	}

	static void UnlockProducts(string productIdentifier){
		BankManager.Instance.UnlockProduct (productIdentifier);
	}

	public static void OnRstorePurchase(){
		UM_InAppPurchaseManager.Instance.RestorePurchases ();
	}
}
