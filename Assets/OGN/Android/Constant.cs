﻿using UnityEngine;
using System.Collections;

namespace ognsdk{

	public class Constant {

		public const string REFLECTION_JAVA_LONG = "java.lang.Long";
		public const string REFLECTION_JAVA_INTEGER = "java.lang.Integer";
		public const string REFLECTION_JAVA_BOOLEAN ="java.lang.Boolean";
		public const string REFLECTION_JAVA_ENUM = "java.lang.Enum";
		public const string REFLECTION_OGNSDK_SCORE ="com.opera.ognsdk.common.Score";
		public const string REFLECTION_OGNSDK_EVENT ="com.opera.ognsdk.common.Event";
		public const string REFLECTION_OGNSDK_OGN ="com.opera.ognsdk.OGN";
		public const string REFLECTION_OGNSDK_CSUTILS = "com.opera.ognsdk.util.CsUtils";
		public const string REFLECTION_UNITY_BASE = "com.unity3d.player.UnityPlayer";
		public const string METHOD_OGNSDK_OGN_POSTSCORE ="postScore";
		public const string METHOD_ANDROID_GETINSTANCE ="getInstance";
		public const string METHOD_UNITY_CURRENTACTIVITY ="currentActivity";
		public const string METHOD_OGNSDK_SENDEVENT ="sendEvent";
	}

}
