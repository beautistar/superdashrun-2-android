﻿using UnityEngine;
using System.Collections;

namespace ognsdk{
	public enum Event  {
		GAME_LAUNCHED, GAME_EXITED, PLAY_BEGAN, PLAY_ENDED
	}
}
