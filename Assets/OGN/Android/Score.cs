﻿using UnityEngine;
using System.Collections;

namespace ognsdk{

	public class Score{

		private long score;
		private string label;
		
		public Score(){
			
		}
		
		public Score(long score, string label){
			this.score = score;
			this.label = label;
		}
		
		public long getScore() {
			return score;
		}
		public void setScore(long score) {
			this.score = score;
		}
		public string getLabel() {
			return label;
		}
		public void setLabel(string label) {
			this.label = label;
		}

	}
}
